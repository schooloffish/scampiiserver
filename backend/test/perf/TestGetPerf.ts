import request = require("request");

export async function testDeviceLocations() {
    const url = 'http://www.scampii.in:/devices/location';
    const options: request.Options = {
        headers: {
            'Authorization': 'Bearer dc6a5fba-73d3-48aa-82f8-76a376126354'
        },
        method : 'GET',
        uri : url,
    };
    const res: any = await  request(options);
    console.log(res);
}

testDeviceLocations().then(()=>console.log('Done'));