/**
 * Created by rohit on 05/04/19.
 */
var admin = require('firebase-admin');
var registrationToken = 'd3uD-LLfNxc:APA91bHJHDwl4ojpLu45ytsvWTXLXpUQD1jqzhwjwlfPt_Yzhpfylpj_gNWwlru2oJHKzJRiEyOrnPE2-Sz-hfYMU8wT4xzixU9aTQ1A0-iP1-g1vgE9rHm3VoyGnmT9Dx7nysGjYxHp';

var serviceAccount = require('./test2.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://softgcm-1120.firebaseio.com'
});

var message = {
    data: {
        score: '850',
        time: '2:45'
    },
    token: registrationToken
};

// Send a message to the device corresponding to the provided
// registration token.
admin.messaging().send(message)
    .then((response) => {
        // Response is a message ID string.
        console.log('Successfully sent message:', response);
    })
    .catch((error) => {
        console.log('Error sending message:', error);
    });