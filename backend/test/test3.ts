import * as XLS from 'node-xlsx';
import * as FileWrite from 'fs';

export class Test3 {

    public static async saveToExcel() {
        const data = [
          [1,2,3],
          [4,5,6],
        ];
        const buffer = XLS.build([{name: 'demo', data: data}]);
        FileWrite.writeFileSync('test.xlsx',buffer);
    }
}

Test3.saveToExcel().then(()=>console.log('done'));