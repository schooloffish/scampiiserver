import {
  Security,
  GET,
  POST,
  Path,
  PUT,
  PathParam,
  QueryParam,
} from "typescript-rest";
import { ScampiiController } from "./ScampiiController";
import { UserModel } from "../models/User";
// import { DeviceModel } from "../models/Device";
// import { SharedDeviceModel } from "../models/SharedDevice";
// import { WarrantyModel } from "../models/Warrenty";
// import ServiceActivity from "../models/ServiceActivity";
import { ProductTypesModel } from "../models/ProductTypes";
import AlertTemplate, { NotificationModel } from "../models/Notification";
import { NotificationHistoryModel } from "../models/NotificationHistory";
import { ManufacturerModel } from "../models/Manufacturer";
import { NotFoundError } from "typescript-rest/dist/server/model/errors";
import { Types } from "mongoose";
import * as Notification from "../models/Notification";
import { NotificationTemplateService } from "../utils/NotificationTemplateService";
import { NotificationTargetAudienceService } from "../utils/NotificationTargetAudienceService";
// import * as moment from "moment";
import * as admin from "firebase-admin";
import ObjectId = Types.ObjectId;

var serviceAccount = require("./test2.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://scampii-5fa25.firebaseio.com",
});

module.exports.admin = admin;

@Path("/notification")
@Security()
export class NotificationController extends ScampiiController {
  //Based on User Profile
  @GET
  @Path("get/countries")
  public async getCountries(): Promise<any> {
    try {
      const countries = await UserModel.find(
        {},
        {
          _id: 0,
          countryName: 1,
        }
      ).sort({ createdAt: "desc" });
      if (countries == null) {
        throw new NotFoundError(`Country with id ${countries} not found.`);
      }
      return countries;
    } catch (e) {
      throw e;
    }
  }

  @GET
  @Path("get/countryBasedCities")
  public async getCities(
    @QueryParam("countryName") countryName: string
  ): Promise<any> {
    try {
      var countryName1 = countryName.split(",");
      const cities = await UserModel.find(
        {
          countryName: { $in: countryName1 },
        },
        {
          _id: 0,
          city: 1,
        }
      ).sort({ createdAt: "desc" });
      return cities;
    } catch (e) {
      throw e;
    }
  }

  @GET
  @Path("get/cities")
  public async getAllCities(): Promise<any> {
    try {
      const allcities = UserModel.find({}, "-_id city").sort({
        createdAt: "desc",
      });
      var cityField = (await allcities).filter(
        (value, index, array) =>
          array.findIndex((t) => t.city === value.city) === index
      );
      return cityField;
    } catch (e) {
      throw e;
    }
  }

  @GET
  @Path("get/prod_categories")
  public async getProductsCategory(): Promise<any> {
    try {
      const product_categories = ProductTypesModel.find({}, "-_id ").sort({
        createdAt: "desc",
      });
      var productfield = (await product_categories).filter(
        (value, index, array) =>
          array.findIndex((t) => t.type === value.type) === index
      );
      return productfield;
    } catch (e) {
      throw e;
    }
  }

  @GET
  @Path("get/manu_list")
  public async getmanufactureList(): Promise<any> {
    try {
      const manufacturer = ManufacturerModel.find({}, "-_id name").sort({
        createdAt: "desc",
      });
      var manufield = (await manufacturer).filter(
        (value, index, array) =>
          array.findIndex((t) => t.name === value.name) === index
      );
      return manufield;
    } catch (e) {
      throw e;
    }
  }

  public async sendNotification(message: any) {
    console.log("sendNotification message", message);
    admin
      .messaging()
      .sendMulticast(message)
      .then(function (response) {
        console.log("sendNotification success", response);
        return message;
      })
      .catch((error) => {
        console.log("sendNotification error", error);
      });
  }

  @POST
  @Path("post/notifications")
  public async createService(newRecord: NotificationModel) {
    try {
      const activity: any = await AlertTemplate.create(newRecord);

      if (newRecord.alertType.toString() == "OneTime") {
        await this.sendOneTimeNotification(newRecord);
      }

      return activity;
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  @PUT
  @Path("update/notifications/:templateId")
  public async updateService(
    newRecord: NotificationModel,
    @PathParam("templateId") templateId: any
  ) {
    try {
      let objectId;
      objectId = new ObjectId(templateId);
      const templateValue = await AlertTemplate.findOne({ _id: objectId });
      // console.log(
      //   "Update Notification Template API templateValue:",
      //   templateValue
      // );

      let activity = await AlertTemplate.updateOne(
        { _id: templateValue._id },
        newRecord
      );

      if (newRecord.alertType.toString() == "OneTime") {
        await this.sendOneTimeNotification(newRecord);
      }

      return activity;
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  private async sendOneTimeNotification(alertTemplate: NotificationModel) {
    // console.log("sendOneTimeNotification alertTemplate:", alertTemplate);
    const { itemHave } = alertTemplate;
    let notificationHistory = [];

    const targetAudienceItems: Array<any> =
      await NotificationTargetAudienceService.filterAlertTargetAudience(
        alertTemplate
      );
    // console.log(
    //   "sendOneTimeNotification targetAudienceItems:",
    //   targetAudienceItems
    // );

    for (const itemDetails of targetAudienceItems) {
      const { deviceId, deviceName, manufacturer, category, receipient } =
        itemDetails;
      // console.log("itemDetails:", itemDetails);

      const oneTimeNotificationBody = itemHave
        ? await NotificationTemplateService.checkOneTimeAlertTemplate(
            deviceName,
            category,
            manufacturer
          )
        : await NotificationTemplateService.checkOneTimeAlertTemplateForNoItems();

      for (const receipientDetails of receipient) {
        const { _id, fcm_token, mobile, email, city, countryName } =
          receipientDetails;

        const message = {
          tokens: [fcm_token],
          data: {
            screenName: itemHave ? "ItemDetails" : "",
            itemInfo: itemHave ? deviceId.toString() : "",
          },
          notification: {
            title: "One Time Alert",
            body: oneTimeNotificationBody,
          },
        };
        // console.log("itemDetails receipientDetails fcm message:", message);

        const sendNotificationStatus = await this.sendNotification(message);
        console.log(
          "itemDetails receipientDetails fcm sendNotificationStatus:",
          sendNotificationStatus
        );

        let notificationObj = {
          mobile: mobile ? mobile : "",
          email: email ? email : "",
          user_Id: _id.toString(),
          city: city ? city : "",
          countryName: countryName,
          userDeviceId: itemHave ? deviceId.toString() : "",
          messageBody: message.notification,
          sendAt: Date.now(),
        };

        notificationHistory.push(notificationObj); // push notification obj in notification history
      }
    }

    // bulk notification histoy
    await NotificationHistoryModel.insertMany(notificationHistory);
  }

  @GET
  @Path("get/notifications")
  public async getAllServices() {
    try {
      var activity = await AlertTemplate.find();
      return activity;
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  @GET
  @Path("get/notifications/:id")
  public async getServicebyID(@PathParam("id") id: String) {
    try {
      var activity = await AlertTemplate.findOne({ _id: id });
      return activity;
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  @GET
  @Path("get/notificationHistory/:user_Id")
  public async getnotificationHistory(@PathParam("user_Id") user_Id: String) {
    try {
      var history = await NotificationHistoryModel.find({ user_Id }).sort({
        sendAt: -1,
      });
      if (history == null) {
        throw new NotFoundError(`Object with id ${user_Id} not found.`);
      }
      return Promise.resolve(history);
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  @POST
  @Path("enableDisable/notifications/:id")
  public async enableDisable(
    @PathParam("id") id: string,
    enable: NotificationModel
  ) {
    try {
      let objectId = new ObjectId(id);
      const condition = { _id: objectId };
      const getAlertTemplate = await AlertTemplate.findOne(condition);
      if (!getAlertTemplate) {
        return Promise.reject(`Device with id ${id} not found.`);
      } else {
        let enableDisable = await AlertTemplate.updateOne(
          { _id: getAlertTemplate._id },
          enable
        );
        return Promise.resolve(enableDisable);
      }
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  // Get FCM Token based on deviceID
  @Path("fcm_token/:deviceId")
  @GET
  public async getFcmToken(
    @PathParam("deviceId") deviceId: string
  ): Promise<any> {
    try {
      return Notification.getFcmToken(this.getObjectId(deviceId));
    } catch (e) {
      throw e;
    }
  }

  // Get FCM Token based on mobile
  @Path("gettoken/:mobileOrEmail")
  @GET
  public async getUserFcmToken(
    @PathParam("mobileOrEmail") mobileOrEmail: string
  ): Promise<any> {
    try {
      return Notification.getUserFcmToken(mobileOrEmail);
    } catch (e) {
      throw e;
    }
  }

  // Get Alert Template based on mobile
  @Path("getAlertTemplate/:mobile")
  @GET
  public async getAlertTemplate(
    @PathParam("mobile") mobile: string
  ): Promise<any> {
    try {
      return Notification.getAlertTemplate(mobile);
    } catch (e) {
      throw e;
    }
  }

  @GET
  @Path("searchAlertTemplate/:templateName")
  public async searchAlertTemplate(
    @PathParam("templateName") templateName: string
  ): Promise<any> {
    try {
      const regex = ".*" + templateName + ".*";
      // console.log(regex);
      const searchStr = {
        template: { $regex: regex, $options: "i" },
      };
      const templates: Array<any> = await AlertTemplate.find(searchStr);
      // console.log(templates);
      const devs: Array<any> = new Array<any>();
      for (const template of templates) {
        const dev: any = template;
        devs.push(dev);
      }
      return Promise.resolve(templates);
    } catch (e) {
      throw e;
    }
  }

  @POST
  @Path("checkTargetAudience")
  public async checkTargetAudience(newRecord: NotificationModel): Promise<any> {
    // console.log("checkTargetAudience API Body:", newRecord);

    try {
      const targetAudienceData =
        await NotificationTargetAudienceService.filterAlertTargetAudience(
          newRecord
        );
      // console.log(
      //   "checkTargetAudience API targetAudienceData:",
      //   targetAudienceData
      // );

      return Promise.resolve(targetAudienceData);
    } catch (error) {
      throw error;
    }
  }

  @PUT
  @Path("updateWith91")
  public async updateMobileWith91(): Promise<any> {
    try {
      let usersMobile: any = await UserModel.find(
        { mobile: { $exists: true } },
        { _id: 1, mobile: 1 }
      );
      let newUser: any = [];
      for (let i = 0; i <= usersMobile.length; i++) {
        let user = usersMobile[i];
        if (user.mobile && user.mobile.length == 10) {
          let mobileNo = `91${user.mobile}`;
          let updatedUser = await UserModel.findOneAndUpdate(
            { _id: user._id },
            { $set: { mobile: mobileNo } },
            { new: true }
          ).lean();
          newUser.push(updatedUser);
        }
      }
      return newUser;
    } catch (err) {
      console.log(err);
    }
  }

  private getObjectId(id: string): ObjectId {
    try {
      return new ObjectId(id);
    } catch (err) {
      console.log(err);
      throw new NotFoundError(`Object with id ${id} not found.`);
    }
  }
}
