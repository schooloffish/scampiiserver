import { Types } from 'mongoose';
import * as ObjectHash from 'object-hash';
import { DELETE, GET, Path, PathParam, POST, Security } from 'typescript-rest';
import ObjectId = Types.ObjectId;
import { WarrantyTemplates, WarrantyTemplatesModel } from '../models/WarrantyTemplates';
import { ScampiiController } from './ScampiiController';


/**
 * Created by rohit on 22/08/19.
 */

@Path('/warrantyTemplates')
@Security()
export class WarrantyTemplateContrl extends ScampiiController {

    @GET
    public async getTemplates(): Promise<Array<WarrantyTemplates>> {
        return WarrantyTemplatesModel.find();
    }

    @POST
    public async addTemplates(template: WarrantyTemplates) : Promise<WarrantyTemplates> {
        const hashObj = {covers: template.covers, duration: template.durations};
        template.hash = ObjectHash(hashObj);
        return new WarrantyTemplatesModel(template).save();
    }

    @POST
    @Path('/hash')
    public async templateHash(template: WarrantyTemplates) : Promise<{hash: string;}> {
        const hashObj = {covers: template.covers, duration: template.durations};
        return Promise.resolve({hash: ObjectHash(hashObj)});
    }

    @DELETE
    @Path('/:id')
    public async deleteTemplate(@PathParam('id') id: string): Promise<any> {
        return WarrantyTemplatesModel.deleteOne({_id: new ObjectId(id)});
    }
}

// async function test() {
//     const res = await new ProductController().productList();
//     console.log(res);
// }
//
// test();
