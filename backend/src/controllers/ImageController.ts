import { GET, Path, PathParam, POST, PUT, Security } from 'typescript-rest';
import { NotFoundError } from 'typescript-rest/dist/server/model/errors';
import { ImageModel, ScampiiImage, ScampiiImageOps } from '../models/ScampiiImageIndex';
import { ScampiiController } from './ScampiiController';
//import { UserOps } from '../models/ops/UserOps';
import { UserModel } from '../models/User';
/**
 * Created by rohit on 24/08/19.
 */
  
@Path('/images')
@Security()
export class ImageController extends ScampiiController {

    @POST
    public async uploadImage(imageDetails: any) : Promise<any> {
        const fileURL = await ScampiiImageOps.saveImage(imageDetails);
        return Promise.resolve({url:fileURL});
    }

    @PUT
    @Path('/user/:mobile')
    public async uploadUserImage(@PathParam('mobile') mobile: string, imageDetails: any) : Promise<any> {
        const userFileURL = await ScampiiImageOps.saveUserImage(imageDetails);
        const image_url={image:userFileURL};
        const condition = { mobile: mobile };
        return UserModel.findOneAndUpdate(condition, image_url,{ "new": true}); 
    }

    @GET
    @Path('/search/:searchStr')
    public async searchImage(@PathParam('searchStr') search: string): Promise<Array<ScampiiImage>> {
        const regex = '.*'+search+'.*';
        const str = {$or: [ { name: { $regex: regex } }, { cat: { $regex: regex } }]};
        return ImageModel.find(str);
    }

    @PUT
    @Path(':url')
    public async updateImageDetails(@PathParam('url') url: string, imageDetails: {name?: string;cat?: string;}): Promise<ScampiiImage> {
        const image = await ImageModel.findOne({url:url});
        if(!image) {
            return Promise.reject(new NotFoundError(`Image with the url ${url} not found.`));
        }
        image.name = imageDetails.name;
        image.cat = imageDetails.cat;
        await ImageModel.updateOne({url:url},image);
        return ImageModel.findOne({url:url});
    }
}
