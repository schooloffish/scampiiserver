import { ValidationResult } from "joi";
import * as EmailValidator from "email-validator";
import { Types } from "mongoose";
import {
  DELETE,
  GET,
  PATCH,
  Path,
  PathParam,
  POST,
  PUT,
  Security,
} from "typescript-rest";
import {
  BadRequestError,
  NotFoundError,
} from "typescript-rest/dist/server/model/errors";
import { ScampiiMongoose } from "../db/ScampiiMongoose";
import * as Device from "../models/Device";
import {
  deviceCat,
  DeviceModel,
  IDevice,
  processDevice,
} from "../models/Device";
import { ManufacturerModel } from "../models/Manufacturer";
import { Product, ProductModel } from "../models/Product";
import { User, UserModel } from "../models/User";
import { UserLocationModel } from "../models/UserLocation";
import {
  IWarranty,
  validateDeviceWarranty,
  Warranty,
  WarrantyModel,
} from "../models/Warrenty";
import { ConfigModel } from "../models/Config";

import { SMSService } from "../utils/SMSService";
import { ShareDeviceEmailService } from "../utils/ShareDeviceEmailService";
import { ScampiiController } from "./ScampiiController";
import ObjectId = Types.ObjectId;
import { ScampiiConstants } from "../utils/ScampiiConstants";
import { SharedDevice, SharedDeviceModel } from "../models/SharedDevice";
import ServiceActivity from "../models/ServiceActivity";
var moment = require("moment");
/**
 * Created by rohit on 19/06/19.
 */

@Path("/devices")
@Security()
export class DeviceController extends ScampiiController {
  public static downloadURL: string = "http://google.com";

  constructor() {
    super();
    ScampiiMongoose.initDB();
  }

  @GET
  @Path("/category")
  public async userDevicesByCategory(): Promise<any> {
    const responseArr = new Array();
    try {
      const user: User = this.getUserDetails();
      const condition = {
        $or: [{ owner: user._id }, { sharedUser: user._id }],
      };
      const userDevices: Array<Device.Device> = await DeviceModel.find(
        condition
      );
      const response: any = {};
      userDevices.forEach((device: Device.Device) => {
        response[device.category] = "";
      });

      for (const key in response) {
        if (response.hasOwnProperty(key) && key && key !== "undefined") {
          const devs: Array<Device.Device> = userDevices.filter(
            (device) => device.category === key
          );
          devs.forEach((device) => processDevice(device));
          responseArr.push({ type: key, total: devs.length, devices: devs });
        }
      }
    } catch (e) {
      console.log(e);
      throw e;
    }
    return Promise.resolve(responseArr);
  }

  @GET
  @Path("/location")
  public async userDevicesByLocatoin(): Promise<any> {
    const user: User = this.getUserDetails();
    const condition = { $or: [{ owner: new ObjectId(user._id) }] };
    const devices: Array<Device.Device> = await Device.DeviceModel.find(
      condition
    );
    const locs = {} as any;
    const devWithoutLoc = new Array();
    for (const device of devices) {
      console.log(`checking device ${device._id}`);
      if (device.location) {
        const loct: any = device.location;
        locs[loct._id.toString()] = "";
        console.log(`Location ${loct} and ${loct._id} and ${locs}`);
      } else {
        console.log(`Adding to null location`);
        devWithoutLoc.push(await processDevice(device));
      }
    }

    // shared devices for the user

    const resp = new Array();
    resp.push({ location: { name: "NA" }, devices: devWithoutLoc });
    for (const key in locs) {
      if (locs.hasOwnProperty(key) && key && key !== "undefined") {
        const devArr1 = new Array<IDevice>();
        for (const device of devices) {
          const location: any = device.location;
          if (!device.location) {
            continue;
          }
          if (device.location && location._id.toString() === key) {
            devArr1.push(await processDevice(device));
          }
        }
        const loct = await UserLocationModel.findOne({ _id: key });
        const value = {
          location: { name: loct.name, addressCity: loct.addressCity },
          devices: devArr1,
        };
        resp.push(value);
      }
    }

    // need to optimise this piece of code
    const sharedDevices: Array<SharedDevice> = await SharedDeviceModel.find({
      sharedWith: this.context.request.user._id,
    });
    const devArr = new Array<IDevice>();
    for (const sharedDevice of sharedDevices) {
      const device = await DeviceModel.findOne({ _id: sharedDevice.device });
      if (device) {
        const devi = await processDevice(device);
        devi.owner.sharedStatus = sharedDevice.status;
        devArr.push(devi);
      }
    }
    resp.push({ location: { name: "sharedWithMe" }, devices: devArr });
    console.log("\n\n\\n\n\n\n\n" + JSON.stringify(resp).length);
    return Promise.resolve(resp);
  }

  @POST
  public async addDevice(device: Device.Device): Promise<Device.Device> {
    // add the transaction support while updating multiple entities
    const user: User = this.context.request.user;
    try {
      const masterProductId = device.masterProductId;
      if (masterProductId) {
        const product: Product = await ProductModel.findOne({
          _id: masterProductId,
        });
        if (product == null) {
          throw new NotFoundError(
            `Master product with id ${masterProductId} not found.`
          );
        }
        console.log("Create Device with Master Product1: ", device);

        device.manuName = product.manuName;
        device.manufacturer = product.manufacturer;
        device.category = product.type;
        device.productModel = product.productModel;
        // device.countryCode = product.countryCode;
        device.modelDesc = product.modelDesc;
        device.modelName = product.modelName;
        device.warranties = new Array<Warranty>();
        device.product = product;

        console.log("Create Device with Master Product2: ", device);
        if (device.warrantyStartDate) {
          product.warranty.warrantyStartDate = device.warrantyStartDate;
          const warranty: Warranty = await new WarrantyModel(
            product.warranty
          ).save();
          device.warranties.push(warranty);
        }
        device.product = product._id.toString();
      }
      device.owner = user._id;
      device = await new Device.DeviceModel(device).save();
      if (!user.devices) {
        user.devices = new Array<Device.Device>();
      }
      user.devices.push(device);
      await user.save();
      const dev: any = await Device.processDevice(device);
      return Promise.resolve(dev);
    } catch (err) {
      console.error(`Error adding device ${err}`);
      return Promise.reject(err);
    }
  }

  @DELETE
  @Path("/:deviceId")
  public async deleteDevice(
    @PathParam("deviceId") deviceId: string
  ): Promise<any> {
    return Device.deleteDevice(this.getObjectId(deviceId));
  }

  @DELETE
  @Path("/image/:deviceId")
  public async deleteDeviceImage(
    @PathParam("deviceId") deviceId: string,
    value: any
  ): Promise<any> {
    const images: Array<string> = value.images;
    const url = "http://13.127.39.213/devclient/scampii/images/";
    var docImageString = images.toString();
    var docName = docImageString.split(",");
    let docNamearray = docName.map((i) => url + i);
    // var deleteDocImage=DeviceModel.updateMany(
    //         { _id: deviceId },
    //         { $pullAll: { docImages: docNamearray } },
    //         {new: true}
    //       );
    //    return  deleteDocImage;
    const deleteDocImage = await DeviceModel.updateMany(
      { _id: deviceId },
      { $pullAll: { docImages: docNamearray } },
      { new: true }
    ).populate("docImages");
    console.log(deleteDocImage);
    const device: Device.Device = await DeviceModel.findOne({ _id: deviceId });
    if (!device) {
      return Promise.reject(
        new NotFoundError(`Object with id ${deviceId} not found.`)
      );
    }
    const dev: any = await Device.processDevice(device);
    return Promise.resolve(dev);
  }

  @GET
  @Path("/:deviceId")
  public async deviceDetails(
    @PathParam("deviceId") deviceId: string
  ): Promise<IDevice> {
    const device: Device.Device = await DeviceModel.findOne({ _id: deviceId });
    if (!device) {
      return Promise.reject(
        new NotFoundError(`Object with id ${deviceId} not found.`)
      );
    }
    const dev: any = await Device.processDevice(device);
    return Promise.resolve(dev);
  }

  @PUT
  @Path("/warranty/:deviceId")
  public async addwarranty(
    @PathParam("deviceId") deviceId: string,
    warranty: IWarranty
  ): Promise<Device.Device> {
    const device: Device.Device = await DeviceModel.findOne({ _id: deviceId });
    if (!device) {
      throw new NotFoundError(`Object with id ${deviceId} not found.`);
    }
    const result: ValidationResult<IWarranty> =
      validateDeviceWarranty(warranty);
    if (result && result.error) {
      {
        throw Error(
          `Warranty details are not correct to create ${result.error}`
        );
      }
    }
    if (!device.warranties) {
      device.warranties = new Array<IWarranty>();
    }
    const startDate = warranty.warrantyStartDate;
    const momentstartDate = moment(startDate);
    let frequency = warranty.serviceFrequency;
    console.log("frequency-->>" + frequency);
    let duration = warranty.warrantyTypeDuration;
    console.log("duration-->>" + duration);
    let serviceVisit = warranty.serviceVisit;
    console.log("serviceVisit-->>>" + serviceVisit);
    const startmoment = moment(startDate);
    console.log("startmoment-->>" + startmoment);
    //   let  endmoment = moment(startmoment).add(duration, 'months')
    //    let  endmoment = moment(startmoment).add(duration-1, 'months').add(1, 'months');
    let endDatemoment = moment(startmoment).add(duration - 1, "months");
    console.log("endDatemoment-->>" + endDatemoment);
    let endmoment = endDatemoment.add(1, "months");
    console.log("endmoment-->>>" + endmoment);

    if (serviceVisit == true) {
      for (
        var m = moment(startmoment);
        m.isBefore(endmoment);
        m.add(frequency, "months")
      ) {
        const startDateWithTime = momentstartDate.format(
          "YYYY-MM-DD[T]HH:mm:ss[Z]"
        );
        var dateformat = m.format("YYYY-MM-DD[T]HH:mm:ss[Z]");
        console.log("startDateWithTime--->>>" + startDateWithTime);
        console.log("dateformat-->>>" + dateformat);
        if (startDateWithTime === dateformat) {
          const res = await ServiceActivity.create({
            serviceStartDate: dateformat,
            status: warranty.status,
            deviceId: deviceId,
          });
          console.log(res);
        } else {
          const res = await ServiceActivity.create({
            serviceStartDate: dateformat,
            status: "Scheduled",
            deviceId: deviceId,
          });
          console.log(res);
        }
      }
    }

    const warranty1 = await WarrantyModel.create(warranty);
    device.warranties.push(warranty1);
    await DeviceModel.updateOne({ _id: device._id }, device);
    const dev: any = await Device.processDevice(device);
    return Promise.resolve(dev);
  }

  @PATCH
  @Path("/share/:deviceId/:userMobile")
  public async shareDevice(
    @PathParam("deviceId") deviceId: string,
    @PathParam("userMobile") userMobile: string
  ): Promise<any> {
    const devId: ObjectId = this.getObjectId(deviceId);
    const device: Device.Device = await DeviceModel.findOne({ _id: devId });
    if (!device) {
      throw new NotFoundError(`Device with ${deviceId} not found.`);
    }

    if (this.context.request.user.mobile === userMobile) {
      throw new BadRequestError(`Device cannot be shared with one self.`);
    }

    if (userMobile.length !== 12) {
      throw new BadRequestError(`Not a valid mobile number.`);
    }
    if (userMobile.toUpperCase() !== userMobile.toLowerCase()) {
      throw new BadRequestError(`Not a valid mobile number.`);
    }

    const { name, mobile, countryName, email } = this.context.request.user;
    const deviceOwner = name
      ? name
      : countryName === "India"
      ? mobile.toString().slice(2)
      : email;

    let user: User = await UserModel.findOne({ mobile: userMobile });
    if (!user) {
      user = await UserModel.create({ mobile: userMobile });
      await SMSService.sendShareDeviceSMS(deviceOwner, userMobile);
    } else {
      const devi = await SharedDeviceModel.findOne({
        device: deviceId,
        sharedWith: user._id,
      });
      if (devi) {
        console.log(
          `Device with ${deviceId} is already shared with the user ${user.mobile}`
        );
      }
    }
    // User exists
    const dev = {} as SharedDevice;
    dev.owner = this.context.request.user;
    dev.sharedWith = user;
    dev.status = ScampiiConstants.sharedDeviceStatusShared;
    dev.device = device;
    await SharedDeviceModel.create(dev);
    await SMSService.sendShareDeviceSMS(deviceOwner, userMobile);

    return Promise.resolve({
      message: "Shared device with the user successfully",
    });
  }

  @PATCH
  @Path("/shareDeviceUsingEmail/:deviceId/:userEmail")
  public async shareDeviceUsingEmail(
    @PathParam("deviceId") deviceId: string,
    @PathParam("userEmail") userEmail: string
  ): Promise<any> {
    const validEmail = EmailValidator.validate(userEmail);
    if (!validEmail) {
      throw new BadRequestError(`Not a valid EMail.`);
    }
    try {
      const devId: ObjectId = this.getObjectId(deviceId);
      const device: Device.Device = await DeviceModel.findOne({ _id: devId });
      if (!device) {
        throw new NotFoundError(`Device with ${deviceId} not found.`);
      }

      if (this.context.request.user.email === userEmail) {
        throw new BadRequestError(`Device cannot be shared with one self.`);
      }

      const { name, email, mobile, countryName } = this.context.request.user;
      const deviceOwner = name
        ? name
        : countryName === "India"
        ? mobile.toString().slice(2)
        : email;
      const configs = await ConfigModel.find();
      let smsTemplate =
        "A device has been shared with you by {#var#}. Check SCAMPII app for details. Download now from http://onelink.to/scampii";
      // tslint:disable-next-line:defa
      for (const config of configs) {
        const conf: any = config;
        if (conf["key"] === "shareDeviceSMS") {
          smsTemplate = conf["value"];
        }
      }
      smsTemplate = smsTemplate.replace("{#var#}", deviceOwner);
      // console.log("Shared Device on Email smsTemplate:", smsTemplate);

      let user: User = await UserModel.findOne({ email: userEmail });
      if (!user) {
        user = await UserModel.create({ email: userEmail });
        await ShareDeviceEmailService.sendShareDeviceEmail(
          smsTemplate,
          userEmail
        );
      } else {
        const devi = await SharedDeviceModel.findOne({
          device: deviceId,
          sharedWith: user._id,
        });
        if (devi) {
          console.log(`Device  is already shared with the user ${user.email}`);
        }
      }
      // User exists
      const dev = {} as SharedDevice;
      dev.owner = this.context.request.user;
      dev.sharedWith = user;
      dev.status = ScampiiConstants.sharedDeviceStatusShared;
      dev.device = device;
      await SharedDeviceModel.create(dev);

      await ShareDeviceEmailService.sendShareDeviceEmail(
        smsTemplate,
        userEmail
      );

      return Promise.resolve({
        message: "Shared device with the user successfully",
      });
    } catch (error) {
      throw error;
    }
  }

  @PATCH
  @Path("/:deviceId/:manuId")
  public async updateManufacturer(
    @PathParam("deviceId") deviceId: string,
    @PathParam("manuId") manuId: string
  ): Promise<any> {
    try {
      const device: Device.Device = await Device.DeviceModel.findOne({
        _id: new ObjectId(deviceId),
      });
      if (!device) {
        return Promise.reject(`Device with id ${deviceId} not found.`);
      }
      const manu = await ManufacturerModel.findOne({
        _id: new ObjectId(manuId),
      });
      if (!manu) {
        return Promise.reject(`Manu with id ${manuId} not found.`);
      } else {
        device.manufacturer = manu;
        device.manuName = manu.name;
        device.save();
        return Promise.resolve(device);
      }
    } catch (error) {
      if (error.name === "CastError") {
        return Promise.reject(error);
      } else {
        return Promise.reject(error);
      }
    }
  }

  @PATCH
  @Path("location/:deviceId/:locationId")
  public async updateDeviceLocation(
    @PathParam("deviceId") deviceId: string,
    @PathParam("locationId") locationId: string
  ): Promise<any> {
    try {
      const device: Device.Device = await Device.DeviceModel.findOne({
        _id: new ObjectId(deviceId),
      });
      if (!device) {
        return Promise.reject(`Device with id ${deviceId} not found.`);
      }
      const location = await UserLocationModel.findOne({
        _id: new ObjectId(locationId),
      });
      if (!location) {
        return Promise.reject(`Location with id ${locationId} not found.`);
      } else {
        device.location = location;
        await device.save();
        if (!location.devices) {
          location.devices = new Array();
        }
        location.devices.push(device);
        await location.save();
        return Promise.resolve(
          await Device.DeviceModel.findOne({ _id: new ObjectId(deviceId) })
        );
      }
    } catch (error) {
      if (error.name === "CastError") {
        return Promise.reject(error);
      } else {
        return Promise.reject(error);
      }
    }
  }

  @GET
  public async getUserDevices(): Promise<Array<IDevice>> {
    const user: User = this.getUserDetails();
    const condition = { $or: [{ owner: user._id }, { sharedUser: user._id }] };
    const devices: Array<Device.Device> = await Device.DeviceModel.find(
      condition
    );
    const devs: Array<IDevice> = new Array<IDevice>();
    for (const device of devices) {
      const dev: any = await Device.processDevice(device);
      devs.push(dev);
    }
    return Promise.resolve(devs);
  }

  @GET
  @Path("/all")
  public async all(): Promise<Array<Device.Device>> {
    const devices: Array<Device.Device> = await Device.DeviceModel.find();
    for (const dev of devices) {
      await Device.processDevice(dev);
    }
    return Promise.resolve(devices);
  }

  @PUT
  @Path(":deviceId")
  public async updateDevice(
    @PathParam("deviceId") deviceId: string,
    device: any
  ): Promise<any> {
    let objectId;
    try {
      objectId = new ObjectId(deviceId);
    } catch (err) {
      console.log(err);
      return Promise.reject(`Device with id ${deviceId} not found.`);
    }
    const condition = { _id: objectId };
    const deviceValue: Device.Device = await Device.DeviceModel.findOne(
      condition
    );
    if (!deviceValue) {
      return Promise.reject(`Device with id ${deviceId} not found.`);
    } else {
      await this.updateWarrantyDate(device, deviceValue);
      await DeviceModel.updateOne({ _id: deviceValue._id }, device);
      const dev = await DeviceModel.findOne({ _id: deviceValue._id });
      const device1 = await Device.processDevice(dev);
      return Promise.resolve(device1);
    }
  }

  private async updateWarrantyDate(
    device: Device.Device,
    orignalDeviceVal: Device.Device
  ) {
    if (!device.purchaseDate) {
      return;
    }
    device.purchaseDate = new Date(device.purchaseDate);
    if (
      device.purchaseDate &&
      device.purchaseDate.getTime() !== orignalDeviceVal.purchaseDate.getTime()
    ) {
      const warranties = orignalDeviceVal.warranties;
      for (const warr of warranties) {
        const warranty: IWarranty = await WarrantyModel.findOne({ _id: warr });
        console.log(warranty);
        if (
          warranty.warrantyStartDate.getTime() ===
          orignalDeviceVal.purchaseDate.getTime()
        ) {
          warranty.warrantyStartDate = device.purchaseDate;
          await WarrantyModel.updateOne({ _id: warr }, warranty);
        }
        if (
          warranty.extendedWarrantyDetails &&
          warranty.extendedWarrantyDetails.previousWarrantyId
        ) {
          const time =
            device.purchaseDate.getTime() -
            orignalDeviceVal.purchaseDate.getTime();
          const warrantyStartDate: Date = warranty.warrantyStartDate;
          const startTime = warrantyStartDate.getTime();
          warranty.warrantyStartDate = new Date(startTime + time);
          await WarrantyModel.updateOne({ _id: warr }, warranty);
        }
      }
      console.log(warranties);
    }
  }

  @PUT
  @Path("/image/:deviceId")
  public async addImageToDevice(
    @PathParam("deviceId") deviceId: string,
    value: any
  ): Promise<Device.Device> {
    let objectId;
    try {
      objectId = new ObjectId(deviceId);
    } catch (err) {
      console.log(err);
      return Promise.reject(`Device with id ${deviceId} not found.`);
    }
    const condition = { _id: objectId };
    const deviceValue: Device.Device = await Device.DeviceModel.findOne(
      condition
    );
    if (!deviceValue) {
      return Promise.reject(`Device with id ${deviceId} not found.`);
    } else {
      const images: Array<string> = value.images;
      for (const image of images) {
        deviceValue.docImages = deviceValue.docImages.filter(
          (img) => img !== image
        );
        deviceValue.docImages.push(image);
      }
      await DeviceModel.updateOne(condition, deviceValue);
      return Promise.resolve(await Device.DeviceModel.findOne(condition));
    }
  }

  @GET
  @Path("/search/:query")
  public async search(
    @PathParam("query") query: string
  ): Promise<Array<IDevice>> {
    const regex = ".*" + query + ".*";
    const searchStr = {
      $or: [
        { name: { $regex: regex, $options: "i" } },
        { category: { $regex: regex, $options: "i" } },
        { countryCode: { $regex: regex, $options: "i" } },
        { manuName: { $regex: regex, $options: "i" } },
        { productModel: { $regex: regex, $options: "i" } },
        { modelDesc: { $regex: regex, $options: "i" } },
        { type: { $regex: regex, $options: "i" } },
        { description: { $regex: regex, $options: "i" } },
      ],
    };
    const devices: Array<Device.Device> = await Device.DeviceModel.find(
      searchStr
    ).populate("warranties");
    const devs: Array<IDevice> = new Array<IDevice>();
    for (const device of devices) {
      const dev: any = await processDevice(device);
      devs.push(dev);
    }
    return Promise.resolve(devs);
  }

  @GET
  @Path("/listCategory")
  public async catList(): Promise<any> {
    return Promise.resolve(deviceCat);
  }

  private getObjectId(id: string): ObjectId {
    try {
      return new ObjectId(id);
    } catch (err) {
      console.log(err);
      throw new NotFoundError(`Object with id ${id} not found.`);
    }
  }
}

// async function test() {
//     const dev: Device.Device = await new DeviceController().updateDevice("5eb91ce10d96255d7e43b8ea",{
//         "purchaseDate": "2024-10-23T00:09:00.000Z"
//     });
//     console.log(dev);
// }
//
// test();
