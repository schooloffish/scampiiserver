import { GET, Path, Security } from 'typescript-rest';
import { codes } from '../db/CountryCodes06Aug2019';
import { ICountryCodes } from '../models/ICountryCodes';
import { ScampiiController } from './ScampiiController';
/**
 * Created by rohit on 05/09/19.
 */

@Path('/countries')
@Security()
export class CountryController extends ScampiiController {

    @GET
    public async countryCodes(): Promise<Array<ICountryCodes>> {
        const lines: Array<string> = codes.toString().split(/\r\n|\r|\n/);
        const CCcodes: Array<ICountryCodes> = new Array();
        lines.forEach((line: string) => {
            const code: ICountryCodes = {} as ICountryCodes;
            code.country = line.split(',')[0];
            code.code = line.split(',')[1];
            CCcodes.push(code);
        });
        return Promise.resolve(CCcodes);

    }
}