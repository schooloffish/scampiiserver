import { GET, POST, PUT, Path, PathParam, Security }  from "typescript-rest";
import ServiceActivity, { ServiceModel } from '../models/ServiceActivity';
import { ScampiiController } from './ScampiiController';
import { NotFoundError } from 'typescript-rest/dist/server/model/errors';


@Path('serviceActivity')
@Security()
export class ServiceActivityController extends ScampiiController{

   //Get All the data
   @GET
   @Path('get/service')
   public async getAllServices() {
       try{
           var state1 = await ServiceActivity.find({status: 'Scheduled'}).sort({serviceStartDate:'asc'});
           var state2 = await ServiceActivity.find({status: 'Completed'}).sort({serviceStartDate:'desc'});
           var state3 = await ServiceActivity.find({status: 'Cancelled'}).sort({serviceStartDate:'desc'});
           let state: any[] = state1.concat(state2); 
           state = state.concat(state3);
           return state;
       }catch(e){
           console.log(e);
           throw e;
       }
   }

    //Get the data Based On ID
    @GET
    @Path('getServiceActivity/:deviceId')
    public async deviceDetails(@PathParam('deviceId') deviceId: string){
        try{
            const activity = await ServiceActivity.find( {deviceId} ).sort({serviceStartDate:'asc'});
            if(activity == null){
                throw new NotFoundError(`Object with id ${deviceId} not found.`)
            }
            return Promise.resolve(activity);

        }catch(e){
            console.log(e);
            throw e;
        }   
    }

    //Post the data
    @POST
    @Path('addServiceActivity/:deviceId')
    public async createService(@PathParam('deviceId') deviceId: string, newRecord: ServiceModel){
        try{
            var activity = await ServiceActivity.create(newRecord);
            var updated: any = await activity.updateOne({deviceId}, newRecord);
            updated = `Service Activity with Device ID: ${deviceId} has been saved`;
            return [updated, activity, {deviceId} ]; 
        }catch(e){
            console.log(e);
            throw new NotFoundError(`Cannot POST with ${deviceId}`);
        }
    }

    //Update the data
    @PUT
    @Path('updateServiceActivity/:serviceId')
    public async updateService(@PathParam('serviceId') serviceId: string, newRecord: ServiceModel){
        try{
            var activity = await ServiceActivity.findOneAndUpdate({_id: serviceId}, newRecord);
            var updated = await activity.updateOne( {_id: serviceId}, newRecord );
            updated = `Service Activity with Service ID: ${serviceId} has been updated`;
            return [updated, newRecord, {serviceId}]; 
        }catch(e){
            console.log(e);
            throw e;
        }
    }

};
