import { Model } from 'mongoose';
import { Context, ServiceContext } from 'typescript-rest';
import { ScampiiMongoose } from '../db/ScampiiMongoose';
import { User } from '../models/User';

/**
 * Created by rohit on 09/08/19.
 */

export class ScampiiController {

    @Context
    protected context: ServiceContext;

    constructor() {
        ScampiiMongoose.initDB();
    }

    public async handleEntityUpdate(response: any, condition: any, model: Model<any>): Promise<any> {
        if(response && response.ok && response.ok === 1 && response.nModified &&  response.nModified === 1) {
            return model.findOne(condition);
        } else if(response.nModified === 0) {
            return Promise.reject(`Entity not found for update with the condition ${JSON.stringify(condition)}`);
        } else {
            return Promise.reject(`Error updating model with condition ${JSON.stringify(condition)}. 
                Details :: ${JSON.stringify(response)}`);
        }

    }

    public getUserDetails(): User {
        return this.context.request.user;
    }


}
