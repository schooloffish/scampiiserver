import {GET, Path, PathParam, Security} from 'typescript-rest';
import { ScampiiController } from './ScampiiController';
import {SharedDevice, SharedDeviceModel} from "../models/SharedDevice";
import {BadRequestError, NotFoundError} from "typescript-rest/dist/server/model/errors";
import {ScampiiConstants} from "../utils/ScampiiConstants";
import {Types} from "mongoose";
import ObjectId = Types.ObjectId;
import {User, UserModel} from "../models/User";

/**
 * Created by rohit on 05/09/19.
 */

@Path('/sharedDevices')
@Security()
export class SharedDeviceController extends ScampiiController {

    @GET
    public async all(): Promise<Array<SharedDevice>> {
        return SharedDeviceModel.find();
    }

    @GET
    @Path('/share/accept/:deviceId')
    public async acceptDevice(@PathParam('deviceId') deviceId: string): Promise<SharedDevice> {
        const device = await SharedDeviceModel.findOne({device: new ObjectId(deviceId), sharedWith: this.context.request.user._id});
        if(!device) {
            throw new NotFoundError(`Device with id ${deviceId} not found, shared with this user ${this.context.request.user}.`);
        }
        device.status = ScampiiConstants.sharedDeviceStatusAccepted;
        await SharedDeviceModel.updateOne({device: new ObjectId(deviceId), sharedWith: this.context.request.user._id}, device);
        return Promise.resolve(device);
    }

    @GET
    @Path('/unshare/:deviceId/:userMobile')
    public async unshareDevice(@PathParam('deviceId') deviceId: string, @PathParam('userMobile') userMobile: string): Promise<any> {
        const user: User = await UserModel.findOne({mobile: userMobile});
        if(user) {
            const devi:SharedDevice = await SharedDeviceModel.findOne({device: deviceId, sharedWith: user._id});
            if (devi) {
                console.log(`Device with ${deviceId} is already shared with the user ${user.mobile}`);
                if(devi.owner._id.toString() !== this.context.request.user._id.toString()) {
                    throw new BadRequestError(`Device ${deviceId} does not belong to user ${this.context.request.user.mobile}.`);
                }
                await SharedDeviceModel.deleteOne({device: deviceId, sharedWith: user._id});
                return Promise.resolve({message:'Device unshared successfully.'});
            } else {
                throw new NotFoundError(`Device with id ${deviceId} is not shared with user ${userMobile}`);
            }
        } else {
            throw new NotFoundError(`User with mobile ${userMobile} not found.`);
        }

    }

    @GET
    @Path('/unshareDeviceUsingEmail/:deviceId/:userEmail')
    public async unshareDeviceUsingEmail(@PathParam('deviceId') deviceId: string, @PathParam('userEmail') userEmail: string): Promise<any> {
        const user: User = await UserModel.findOne({email: userEmail});
        if(user) {
            const devi:SharedDevice = await SharedDeviceModel.findOne({device: deviceId, sharedWith: user._id});
            if (devi) {
                console.log(`Device with ${deviceId} is already shared with the user ${user.email}`);
                if(devi.owner._id.toString() !== this.context.request.user._id.toString()) {
                    throw new BadRequestError(`Device ${deviceId} does not belong to user ${this.context.request.user.email}.`);
                }
                await SharedDeviceModel.deleteOne({device: deviceId, sharedWith: user._id});
                return Promise.resolve({message:'Device unshared successfully.'});
            } else {
                throw new NotFoundError(`Device with id ${deviceId} is not shared with user ${userEmail}`);
            }
        } else {
            throw new NotFoundError(`User with email ${userEmail} not found.`);
        }

    }

    @GET
    @Path('/sharedWithMe')
    public async sharedWithMe(): Promise<Array<SharedDevice>> {
        const device = await SharedDeviceModel.find({sharedWith: this.context.request.user._id});
        return Promise.resolve(device);
    }

    @GET
    @Path('/share/reject/:deviceId')
    public async rejectDevice(@PathParam('deviceId') deviceId: string): Promise<SharedDevice> {
        const device = await SharedDeviceModel.findOne({device: new ObjectId(deviceId), sharedWith: this.context.request.user._id});
        if(!device) {
            throw new NotFoundError(`Object with id ${deviceId} not found, shared with this user ${this.context.request.user}.`);
        }
        device.status = ScampiiConstants.sharedDeviceStatusRejected;
        await SharedDeviceModel.deleteOne({device: new ObjectId(deviceId), sharedWith: this.context.request.user._id});
        return Promise.resolve(device);
    }

    @GET
    @Path('/sharedWithUsers')
    public async sharedWithUsers(): Promise<Array<any>> {
        const devices: Array<SharedDevice> = await SharedDeviceModel.find({owner: this.context.request.user._id});
        if(!devices) {
            throw new NotFoundError(`No shared users found`);
        }
        const users = new Array<any>();
        const userIds = {} as any;
        for (const device of devices) {
            userIds[device.sharedWith.toString()] = '';
        }

        for (const key in userIds) {
            if (userIds.hasOwnProperty(key) && key && key !== 'undefined') {
                const user: User = await UserModel.findOne({_id: new ObjectId(key)});

                // users.push({_id:user._id,name:user.name,mobile:user.mobile,image:user.image});
            users.push({_id:user._id,
                name:user.name,
                mobile:user.mobile,
                image:user.image,
                email:user.email    
            });
            }
        }
        return Promise.resolve(users);
    }


}