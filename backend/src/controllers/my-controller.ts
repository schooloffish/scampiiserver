import * as mongoose from 'mongoose';
import { GET, Path, PathParam } from 'typescript-rest';

/**
 * This is a demo operation to show how to use typescript-rest library.
 */
@Path('/my')
export class MyController {
    /**
     * Send a greeting message.
     * @param name The name that will receive our greeting message
     */
    @Path(':name')
    @GET
    public sayHello(@PathParam('name') name: string): string {
        const greet = 'This is for just testing my name controller :: ' + name;
        console.log('sending the greeting back to the server');
        const connection = mongoose.createConnection('mongodb://localhost/local');
        console.log(connection);
        return greet;
    }
}
