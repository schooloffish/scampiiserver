import {GET, Path, POST, PUT} from 'typescript-rest';
import {Config, ConfigModel} from "../models/Config";
import {ScampiiConstants} from "../utils/ScampiiConstants";
import { ScampiiController } from './ScampiiController';
/**
 * Created by rohit on 05/09/19.
 */

@Path('/configs')
export class ConfigController extends ScampiiController {

    @GET
    public async all(): Promise<Array<Config>> {
        return ConfigModel.find();
    }

    @POST
    public async updateConfigs(configs: Array<any>): Promise<Array<Config>> {
        for (const configuration of configs) {
            const config: any = configuration;
            const value: Config = await ConfigModel.findOne({key:config.key});
            if(value) {
                await ConfigModel.updateOne({key:config.key},
                    {key: config.key, value: config.value});
            } else {
                await ConfigModel.create({key: config.key, value: config.value});
            }
        }
        return ConfigModel.find();
    }

    @GET
    @Path('/aboutUs')
    public async getAboutUs(): Promise<any> {
        const contains: Array<String> = ['privacyPolicyURL','termsAndConditionsURL','feedbackURL'];
        const configs = await ConfigModel.find();
        const result: Array<any> = new Array<any>();
        // tslint:disable-next-line:forin
        for (const config of configs) {
            const conf: any = config;
            const strings = contains.filter((key) => key === conf.key);
            if(strings && strings.length>0) {
                result.push(conf);
            }
        }
        return Promise.resolve(result);
    }

    @GET
    @Path('/feedbackURL')
    public async getFeedbackURL(): Promise<String> {
        return ScampiiConstants.getFeedbackURL();
    }

    @PUT
    @Path('/feedbackURL')
    public async updateFeedbackURL(feedbackURL: any): Promise<String> {
        const feedback: Config = await ConfigModel.findOne({key:ScampiiConstants.feedbackURL});
        if(feedback) {
            await ConfigModel.updateOne({key:ScampiiConstants.feedbackURL},
                {key: ScampiiConstants.feedbackURL, value: feedbackURL.value});
        } else {
            await ConfigModel.create({key: ScampiiConstants.feedbackURL, value: feedbackURL.value});
        }
        return Promise.resolve(feedbackURL.value);
    }

    @GET
    @Path('/adminEmail')
    public async getAdminEmail(): Promise<String> {
        return ScampiiConstants.getAdminEmail();
    }

    @PUT
    @Path('/adminEmail')
    public async updateAdminEmail(adminEmail: any): Promise<String> {
        const email: Config = await ConfigModel.findOne({key:ScampiiConstants.adminEmail});
        if(email) {
            await ConfigModel.updateOne({key:ScampiiConstants.adminEmail},
                {key: ScampiiConstants.adminEmail, value: adminEmail.value});
        } else {
            await ConfigModel.create({key: ScampiiConstants.adminEmail, value: adminEmail.value});
        }
        return Promise.resolve(adminEmail.value);
    }


    @GET
    @Path('/appDownloadLink')
    public async getAppLink(): Promise<String> {
        return ScampiiConstants.getAppDownloadLink();
    }

    @PUT
    @Path('/appDownloadLink')
    public async updateAppLink(link: any) : Promise<String>{
        const linkConfig: Config = await ConfigModel.findOne({key:ScampiiConstants.appDownloadLink});
        if(linkConfig) {
            await ConfigModel.updateOne({key:ScampiiConstants.appDownloadLink},
                {key: ScampiiConstants.appDownloadLink, value: link.value});
        } else {
            await ConfigModel.create({key: ScampiiConstants.appDownloadLink, value: link.value});
        }
        return Promise.resolve(link.value);
    }
}


async function test() {
    const dev = await new ConfigController().getAboutUs();
    console.log(dev);
}

test();
