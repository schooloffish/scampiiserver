/**
 * Created by rohit on 09/09/19.
 */
import { Types } from 'mongoose';
import { DELETE, GET, Path, PathParam, POST, PUT, Security } from 'typescript-rest';
import ObjectId = Types.ObjectId;
import { NotFoundError } from 'typescript-rest/dist/server/model/errors';
import { Manufacturer, ManufacturerModel } from '../models/Manufacturer';
import { ServiceCenter, ServiceCenterModel } from '../models/ServiceCenter';
import { ScampiiController } from './ScampiiController';


@Path('/serviceCenters')
@Security()
export class ServiceCenterController extends ScampiiController {

    @GET
    public async getCenters(): Promise<Array<ServiceCenter>> {
        return ServiceCenterModel.find().sort({'createdAt': -1}).limit(10000);
    }

    @GET
    @Path('/:manuId')
    public async getCentersForManu(@PathParam('manuId') manuId: string): Promise<Array<ServiceCenter>> {
        return ServiceCenterModel.find({manufacturer: new ObjectId(manuId)}).sort({'createdAt': -1}).limit(10000);
    }

    @POST
    @Path('/:manuId')
    public async createCenter(@PathParam('manuId') manuId: string, serviceCenter: ServiceCenter) {
        const conditions = {_id: new ObjectId(manuId)};
        const manu: Manufacturer = await ManufacturerModel.findOne(conditions);
        if(!manu) {
            throw new NotFoundError(`Manu with ${serviceCenter.manufacturer} not found.`);
        }
        if(!manu.serviceCenters) {
            manu.serviceCenters = new Array<ServiceCenter>();
        }

        serviceCenter.manufacturer = manu;
        serviceCenter.manuName = manu.name;
        const sv: any = await ServiceCenterModel.create(serviceCenter);
        manu.serviceCenters.push(sv);
        await ManufacturerModel.updateOne(conditions,manu);
        return Promise.resolve(serviceCenter);
    }

    @PUT
    @Path('/:id')
    public async updateCenter(@PathParam('id') id: string, serviceCenter: ServiceCenter) {
        const condition = {_id: new ObjectId(id)};
        const center: ServiceCenter = await ServiceCenterModel.findOne(condition);
        if(!center) {
            throw new NotFoundError(`Center with id ${id} not found.`);
        }
        const manu: Manufacturer = await ManufacturerModel.findOne({_id: new ObjectId(serviceCenter.manufacturer.toString())});
        if(!manu) {
            throw new NotFoundError(`Manu with ${serviceCenter.manufacturer} not found.`);
        }
        await ServiceCenterModel.updateOne(condition, serviceCenter);
        return ServiceCenterModel.findOne(condition);
    }

    @DELETE
    @Path('/:id')
    public async delete(@PathParam('id') id: string) {
        const condition = {_id: new ObjectId(id)};
        const center: ServiceCenter = await ServiceCenterModel.findOne(condition);
        const manu = await ManufacturerModel.findOne({_id: center.manufacturer});
        if(manu && manu.serviceCenters && manu.serviceCenters.length > 0) {
            manu.serviceCenters =  manu.serviceCenters.filter(serviceCenter => serviceCenter.toString()!==id);
            await ManufacturerModel.updateOne({_id: center.manufacturer}, manu);
        }
        if(!center) {
            throw new NotFoundError(`Center with id ${id} not found.`);
        }
        return ServiceCenterModel.deleteOne(condition);
    }

}

// async function test() {
//     ScampiiMongoose.initDB();
//     const serv = {
//     "centerAddress": [{
//         "address":"Alpahs",
//         "contactName":"center1",
//         "contactNumber":"7878787",
//     }],
//     "countryCode": "IN",
//     "custCareNum": "+91-9538001985",
//         "manufacturer": "5d5e26bd3bfa1517e08d4b52",
//      "website": "http://www.goooogle.com"
//     };
//     return new ServiceCenterModel(serv).save();
// }
//
// test().then(result=>console.log(result)).catch(error=>console.log(error));