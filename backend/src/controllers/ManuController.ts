import { Types } from 'mongoose';
import { DELETE, GET, Path, PathParam, POST, PUT, Security } from 'typescript-rest';
import { NotFoundError } from 'typescript-rest/dist/server/model/errors';
import { Manufacturer, ManufacturerModel } from '../models/Manufacturer';
import {ServiceCenter, ServiceCenterModel} from "../models/ServiceCenter";
import ObjectId = Types.ObjectId;
/**
 * Created by rohit on 21/06/19.
 */

@Path('/manus')
@Security()
export class ManuController {

    @POST
    public async add(manu: Manufacturer): Promise<Manufacturer|any> {
        return Promise.resolve(await new ManufacturerModel(manu).save());
    }

    @PUT
    @Path('/servicecenter/:id')
    public async addServiceCenter(@PathParam('id') id: string, serviceCenter: ServiceCenter): Promise<Manufacturer|any> {
        const condition = {_id: new ObjectId(id)};
        const manufac: Manufacturer = await ManufacturerModel.findOne(condition);
        if (!manufac) {throw new NotFoundError(`Manufacturer with ${id} not found.`);}
        serviceCenter.manuName  =  manufac.name;
        serviceCenter.manufacturer = manufac;
        const center: ServiceCenter = await ServiceCenterModel.create(serviceCenter);
        if(!manufac.serviceCenters) {
            manufac.serviceCenters = new Array<ServiceCenter>();
        }
        manufac.serviceCenters.push(center);
        await ManufacturerModel.update(condition,manufac);
        return ManufacturerModel.findOne(condition);
    }

    @PUT
    @Path('/:id')
    public async update(@PathParam('id') id: string,  manu: Manufacturer): Promise<Manufacturer|any> {
        const condition = {_id: new ObjectId(id)};
        const manufac = await ManufacturerModel.findOne(condition);
        if (!manufac) {throw new NotFoundError(`Manufacturer with ${id} not found.`);}
        await ManufacturerModel.update(condition,manu);
        return ManufacturerModel.findOne(condition);
    }

    @GET
    public async allManus() : Promise<Array<Manufacturer>|any> {
        return ManufacturerModel.find();
    }

    @GET
    @Path('/search/:query')
    public async search(@PathParam('query') query: string) : Promise<Array<Manufacturer>> {
        const regex = '.*'+query+'.*';
        const searchStr = {$or: [ { name: { $regex: regex } } ]};
        return ManufacturerModel.find(searchStr);
    }

    @DELETE
    @Path('/:id')
    public async delete(@PathParam('id') id: string): Promise<any> {
        return ManufacturerModel.deleteOne({_id: new ObjectId(id)});
    }
}
