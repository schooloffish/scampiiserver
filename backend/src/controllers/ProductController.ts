import * as File from "fs";
import {ValidationResult} from 'joi';
import {Types} from 'mongoose';
import * as ObjectHash from 'object-hash';
import {DELETE, GET, Path, PathParam, POST, PUT, QueryParam, Security} from 'typescript-rest';
import {BadRequestError, NotFoundError} from 'typescript-rest/dist/server/model/errors';
import {ScampiiMongoose} from "../db/ScampiiMongoose";
import {Manufacturer, ManufacturerModel} from '../models/Manufacturer';
import * as Product from '../models/Product';
import {ProductModel} from '../models/Product';
import {ProductTypes, ProductTypesModel} from '../models/ProductTypes';
import {WarrantyTemplates, WarrantyTemplatesModel} from '../models/WarrantyTemplates';
import {EmailService} from "../utils/EmailService";
import {ExcelUtils} from "../utils/ExcelUtils";
import {ScampiiConstants} from "../utils/ScampiiConstants";
import {ScampiiUtils} from "../utils/ScampiiUtils";
import {ScampiiController} from './ScampiiController';
import ObjectId = Types.ObjectId;

const soundex = require('soundex-code');
const metaphone = require('metaphone');
const xlsxFile = require('read-excel-file/node');

/**
 * Created by rohit on 22/08/19.
 */

@Path('/products')
@Security()
export class ProductController extends ScampiiController {

    constructor(){
        super();
        ScampiiMongoose.initDB();
    }

    @POST
    @Path('/bulk')
    public async bulkUploadProducts(value: any) : Promise<any> {
        console.log(value.value);
        const fileData: Buffer = Buffer.from(value.value, 'base64');
        const fileName = 'temp' +new Date().getTime()+'.xlsx';
        File.writeFileSync(fileName,fileData);
        const data: Array<Array<string>> = await xlsxFile(fileName);
        const result = new Array();
        const row1 = data[0];
        if (row1.length < 8) { return Promise.reject(new BadRequestError('The excel sheet must have 8 columns.')); }
        row1.push ('Result');
        row1.push('Status');
        row1.push('ProductId');
        row1.push('ProductImage');
        result.push(row1);
        const promises = new Array();
        for (let i=1;i<data.length;i++) {
            const row: any = data[i];
            if (row.length < 8) { return Promise.reject(new BadRequestError('The excel sheet must have all 8 columns for all the rows specified as per the template')); }
            const product:Product.Product = this.getPopulatedProduct(row);
            product.name = `${product.manuName} ${product.modelDesc} ${product.type} ${product.productModel}`;
            this.populatePhonixs(product);
            promises.push(this.saveProduct(product,row, result));
        }
        Promise.all(promises).then(() => {
            this.populateResult(result);
            File.unlinkSync(fileName);
        }).catch(() => this.populateResult(result));
        console.log(result);
        return Promise.resolve({message:'Bulk products upload successfully. Detailed Report will be sent to email ' + await ScampiiConstants.getAdminEmail()});
    }

    @POST
    public async add(product: any, @QueryParam('manuId') manuId: string): Promise<Product.Product|any> {
        const manu:Manufacturer = await ManufacturerModel.findOne({_id: new ObjectId(manuId)});
        if(!manu) {throw Error(`Not able to find manufac for id ${manuId}`);}
        product.manufacturer = manu;
        product.manuName = manu.name;
        const typeId = product.type;
        const prodType: ProductTypes = await ProductTypesModel.findOne({type: typeId});
        if(!prodType) {
            await ProductTypesModel.create({type: typeId});
        } else {
            product.typeImage = prodType.image;
        }
        this.populatePhonixs(product);
        const result: ValidationResult<any> = Product.validate(product);
        if(result && result.error) {
            {throw Error(`Product details are not correct to create ${result.error}`);}
        }
        return ProductModel.create(product);
    }

    @PUT
    @Path('/:id')
    public async updateProduct(@PathParam('id') id: string, product: Product.Product) : Promise<Product.Product> {
        const prod = await ProductModel.findOne({_id: new ObjectId(id)});
        if (!prod) { throw new NotFoundError(`Product with id ${id} not found.`);}
        const manuId: string = product.manufacturer.toString();
        const manu:Manufacturer = await ManufacturerModel.findOne({_id: new ObjectId(manuId)});
        if(manu) {
            product.manuName = manu.name;
        }
        product.manufacturer = manu;
        const result: ValidationResult<any> = Product.validate(product);
        if(result && result.error) {
            {throw Error(`Product details are not correct to create ${result.error}`);}
        }
        const typeId = product.type;
        const prodType: ProductTypes = await ProductTypesModel.findOne({type: typeId});
        product.typeImage = prodType.image;
        this.populatePhonixs(product);
        await ProductModel.updateOne({_id: new ObjectId(id)}, product);
        return ProductModel.findOne({_id: new ObjectId(id)});
    }


    @GET
    public async productList() : Promise<Array<Product.Product>> {
        const prods:Array<any> = await ProductModel.find().sort({'createdAt': -1}).limit(100);
        const newProds = Array<any>();
        for (const prod of prods) {
            await new ProductController().processProduct(prod);
            newProds.push(prod);
        }
        return Promise.resolve(newProds);
    }


    @GET
    @Path('/manu/:manuId')
    public async productListByManu(@PathParam('manuId') manuId: string) : Promise<Array<Product.Product>> {
        return ProductModel.find({manufacturer:manuId});
    }

    @GET
    @Path('/search/:query')
    public async search(@PathParam('query') query: string) : Promise<Array<Product.Product>> {
        let products: Array<any> = await ProductModel.find({$text:{$search:query}},{score:{$meta:"textScore"}}).sort({score:{$meta:"textScore"}}).limit(100);
// do soundex search
        if(!products  || products.length  === 0) {
            const regex = '.*'+query+'.*';
            const searchStr = {$or: [
                    { name: { $regex: regex, '$options' : 'i' } }
                ]};

            products = await ProductModel.find(searchStr).sort({'createdAt': -1}).limit(100);
        }

        if(!products || products.length  === 0) {
            products  = await this.doSoundexSearch(query);
        }
        if(!products || products.length  === 0) {
            products  = await this.doMetaphoneSearch(query);
        }

        for (const product of products) {
            await new ProductController().processProduct(product);
        }
        return Promise.resolve(products);
    }


    @GET
    @Path('/types')
    public async getTypes(): Promise<Array<ProductTypes>> {
        return ProductTypesModel.find();
    }

    @POST
    @Path('/types')
    public async addType(typeObj: any): Promise<any> {
        return ProductTypesModel.create({type: typeObj.type, image: typeObj.image});
    }

    @DELETE
    @Path('/:id')
    public async delete(@PathParam('id') id: string): Promise<any> {
        return ProductModel.deleteOne({_id: new ObjectId(id)});
    }

    @GET
    @Path('/:id')
    public async getProduct(@PathParam('id') id: string): Promise<any> {
        const produ: any = await ProductModel.find({_id: new ObjectId(id)});
        await new ProductController().processProduct(produ[0]);
        return Promise.resolve(produ);
    }


    @GET
    @Path('/warrantyTemplates')
    public async getTemplates(): Promise<Array<WarrantyTemplates>> {
        return WarrantyTemplatesModel.find();
    }

    @POST
    @Path('/warrantyTemplates')
    public async addTemplates(template: WarrantyTemplates) : Promise<WarrantyTemplates> {
        const hashObj = {covers: template.covers, duration: template.durations};
        template.hash = ObjectHash(hashObj);
        return new WarrantyTemplatesModel(template).save();
    }

    @POST
    @Path('/warrantyTemplates/hash')
    public async templateHash(template: WarrantyTemplates) : Promise<{hash: string;}> {
        const hashObj = {covers: template.covers, duration: template.durations};
        return Promise.resolve({hash: ObjectHash(hashObj)});
    }

    @DELETE
    @Path('/warrantyTemplates/:id')
    public async deleteTemplate(@PathParam('id') id: string): Promise<any> {
        return WarrantyTemplatesModel.deleteOne({_id: new ObjectId(id)});
    }

    private async processProduct(product: Product.Product){
        const warranty = product.warranty;
        const covers: Array<any> = warranty.covers;
        const newCovers = new Array;
        for (const cover of covers) {
            if('NA' === cover.value) {
                continue;
            } else {
                newCovers.push(cover);
            }
        }
        product.warranty.covers = newCovers;
        const typeId = product.type;
        if(typeId) {
            const prodType: ProductTypes = await ProductTypesModel.findOne({type: typeId});
            if(prodType) {
                product.type = {type: prodType.type, image: prodType.image};
                product.typeImage = prodType.image;
            }
        }
    }

    private getPopulatedProduct(row: any): any {
        return {
            countryCode: row[1],
            manuName: row[2],
            modelDesc: row[4],
            productModel: row[3],
            type: row[0],
        };
    }

    private async doSoundexSearch(query: string){
        if(!query) {return Promise.resolve(null);}
        const nameVals = query.split(' ');
        let soundx = '';
        let metaph = '';
        for (const nameVal of nameVals) {
            if(nameVal && nameVal.trim().length>0) {
                const meta = metaphone(nameVal);
                const sound = soundex(nameVal);
                soundx =  soundx +  ' ' +  sound;
                metaph =  metaph + ' ' + meta;
            }
        }
        return  ProductModel.find({$text:{$search:soundx}},{score:{$meta:"textScore"}}).sort({score:{$meta:"textScore"}}).limit(100);
    }
    private async doMetaphoneSearch(query: string){
        if(!query) {return Promise.resolve(null);}
        const nameVals = query.split(' ');
        let soundx = '';
        let metaph = '';
        for (const nameVal of nameVals) {
            if(nameVal && nameVal.trim().length>0) {
                const meta = metaphone(nameVal);
                const sound = soundex(nameVal);
                soundx =  soundx +  ' ' +  sound;
                metaph =  metaph + ' ' + meta;
            }
        }
        return  ProductModel.find({$text:{$search:metaph}},{score:{$meta:"textScore"}}).sort({score:{$meta:"textScore"}}).limit(100);
    }
    private populatePhonixs(product : Product.Product) {
        if(!product.name) {return;}
        const nameVals = product.name.split(' ');
        let soundx = '';
        let metaph = '';
        for (const nameVal of nameVals) {
            if(nameVal && nameVal.trim().length>0) {
                const meta = metaphone(nameVal);
                const sound = soundex(nameVal);
                soundx =  soundx +  ' ' +  sound;
                metaph =  metaph + ' ' + meta;
            }
        }
        product.metaphone = metaph.trim();
        product.soundex = soundx.trim();
        console.log(product);
    }

    private async saveProduct(product: any, row: any, result: Array<any>): Promise<Product.Product> {

        try {
            if(!product.type) {
                const manuErr = `Type is mandatory for ${product}`;
                throw Error(manuErr);
            }
            const templateName = row[5];
            const duration = row[7];
            const warrantyTemplate: WarrantyTemplates = await WarrantyTemplatesModel.findOne({name: templateName});
            if (!warrantyTemplate) {
                const errorMsg = `Warrranty template does not exists for template name ${templateName}`;
                console.error(errorMsg);
                throw new Error(errorMsg);
            }
            if(!row[6] || row[6].toString().trim().length === 0) {
                row[6] = 'Factory';
            }
            const warranty = {
                covers: warrantyTemplate.covers,
                name: templateName,
                warrantyType: row[6],
                warrantyTypeDuration: duration,
            };
            product.warranty = warranty;
            console.log('saving product' + JSON.stringify(product));
            const prod = await ProductModel.findOne({productModel: product.productModel, manuName: row[2].trim()});
            const manu = await ManufacturerModel.findOne({name: product.manuName});
            if (!manu) {
                const manuErr = `manufacturer with the name ${product.manuName} does not exits`;
                throw Error(manuErr);
            }

            product.manufacturer = manu;
            if (prod) {
                await ProductModel.updateOne({productModel: product.productModel}, product);
            } else {
                await ProductModel.create(product);
            }
            const prod1 = await ProductModel.findOne({productModel:product.productModel});
            const imagePath = await ScampiiConstants.getImagesProdDir() + product.productModel + '.jpg';
            product.image = imagePath;
            await ProductModel.updateOne({productModel: product.productModel}, product);
            row.push('Product saved successfully.');
            row.push('Success');
            row.push(prod1._id.toString());
            row.push(imagePath);
            result.push(row);
            console.log(`saved product details successfully ${row}`);
            return Promise.resolve(prod1);
        }catch (e) {
            row.push(e);
            row.push('Failed');
            row.push('NA');
            result.push(row);
            console.log(e);
            console.log(`failed product saving ${row}`);
        }
        return Promise.resolve(product);

    }

    private populateResult(result: Array<any>) {
        console.log(`sending email for result ${result}`);
        const promise = this.sendResultMail(result);
        ScampiiUtils.logPromiseResults(promise);
    }

    private async sendResultMail(result: Array<any>) : Promise<any>{
        const adminEmail = await ScampiiConstants.getAdminEmail();
        const filePath = ExcelUtils.writeDataToFile(result);
        await EmailService.sendMailWithAttachment('Product Upload Results',
            adminEmail,'Upload products detailed report.',filePath);
        File.unlinkSync(filePath);
        return Promise.resolve('Product file upload successfully.');
    }


}

// async function test() {
//     const arr: Array<any> = [['Category','Country Code','Manufacturer','Model','Model Description','Warrant Template','warrantyType','Default Factory Warranty'],
//     ['TV','INDIA','Apple','test model','test model for description','LG DVD Burner','Factory',12]];
//     new ProductController().bulkUploadProducts({value:arr}).then(()=>console.log('done'));
// }
//
// test();
