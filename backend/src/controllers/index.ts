import { ConfigController } from "./ConfigController";
import { CountryController } from "./CountryController";
import { DeviceController } from "./DeviceController";
import { HelloController } from "./hello-controller";
import {
  HelloIocDirectController,
  HelloIocInterfaceController,
} from "./hello-ioc-controller";
import { HelloObjectController } from "./hello-object-controller";
import { ImageController } from "./ImageController";
import {
  HelloServiceBase,
  HelloServiceImpl,
  IocHelloService,
} from "./ioc-services";
import { ManuController } from "./ManuController";
import { MyController } from "./my-controller";
import { ProductController } from "./ProductController";
import { ServiceCenterController } from "./ServiceCenterController";
import { SharedDeviceController } from "./SharedDeviceController";
import { UserController } from "./UserController";
import { WarrantyTemplateContrl } from "./WarrantyTemplateContrl";
import { WarrentyController } from "./WarrentyController";
import { NotificationController } from "./NotificationController";
import { ServiceActivityController } from "./ServiceActivityController";
import AlertTemplate, { NotificationModel } from "../models/Notification";
import { NotificationTemplateService } from "../utils/NotificationTemplateService";
import { NotificationTargetAudienceService } from "../utils/NotificationTargetAudienceService";
import { NotificationHistoryModel } from "../models/NotificationHistory";
import * as moment from "moment";
var cron = require("node-cron");
import * as admin from "firebase-admin";

async function sendNotification(message: any) {
  console.log("sendNotification message", message);
  admin
    .messaging()
    .sendMulticast(message)
    .then(function (response) {
      console.log("sendNotification success", response);
      return message;
    })
    .catch((error) => {
      console.log("sendNotification error", error);
    });
}

function checkScheduledTimeForAlertTemplate(alertTemplate: NotificationModel) {
  const currentServerUtcTime = moment().utc().format("H:mm");

  const serverUtcTimeAfterFifteenMinutes = moment()
    .utc()
    .add(15, "minute")
    .format("H:mm");

  const alertTemplateScheduledTimeUTC = moment
    .utc(alertTemplate.selectedTimeUTC + "", "")
    .format("H:mm");

  const isScheduledTime = isScheduledTimeNow(
    currentServerUtcTime,
    serverUtcTimeAfterFifteenMinutes,
    alertTemplateScheduledTimeUTC
  );

  return isScheduledTime;
}

function isScheduledTimeNow(
  currentServerUtcTime: any,
  serverUtcTimeAfterFifteenMinutes: any,
  alertTemplateScheduledTimeUTC: any
) {
  let start = moment(currentServerUtcTime, "H:mm");
  let end = moment(serverUtcTimeAfterFifteenMinutes, "H:mm");
  let scheduled = moment(alertTemplateScheduledTimeUTC, "H:mm");
  if (end < start) {
    return (
      (scheduled >= start && scheduled <= moment("23:59:59", "h:mm:ss")) ||
      (scheduled >= moment("0:00:00", "h:mm:ss") && scheduled < end)
    );
  }
  return scheduled >= start && scheduled < end;
}

cron.schedule(`*/15 * * * *`, () => {
  // cron.schedule(`*/1 * * * *`, () => {
  console.log("Cron job started for every 15 minutes");

  triggerScheduledWarrantyNotifications();

  triggerScheduledServiceActivityNotifications();
});

async function triggerScheduledWarrantyNotifications() {
  try {
    const warrantyAlertTemplates: Array<any> = await AlertTemplate.find({
      enable: true,
      alertType: "scheduledWarranty",
    });
    // console.log(
    //   "triggerScheduledWarrantyNotifications warrantyAlertTemplates:",
    //   warrantyAlertTemplates
    // );

    for (const alertTemplate of warrantyAlertTemplates) {
      const isScheduledTime: any =
        checkScheduledTimeForAlertTemplate(alertTemplate);
      // console.log(
      //   "triggerScheduledWarrantyNotifications isScheduledTime:",
      //   isScheduledTime
      // );

      if (isScheduledTime) {
        // console.log(
        //   "triggerScheduledWarrantyNotifications alertTemplate:",
        //   alertTemplate
        // );

        await sendScheduledNotification(alertTemplate);
      }
    }
  } catch (error) {
    console.log("triggerScheduledWarrantyNotifications error:", error);
  }
}

async function triggerScheduledServiceActivityNotifications() {
  try {
    const serviceActivityAlertTemplates: Array<any> = await AlertTemplate.find({
      enable: true,
      alertType: "scheduledServiceActivity",
    });
    // console.log(
    //   "triggerScheduledServiceActivityNotifications serviceActivityAlertTemplates:",
    //   serviceActivityAlertTemplates
    // );

    for (const alertTemplate of serviceActivityAlertTemplates) {
      const isScheduledTime: any =
        checkScheduledTimeForAlertTemplate(alertTemplate);
      // console.log(
      //   "triggerScheduledServiceActivityNotifications isScheduledTime:",
      //   isScheduledTime
      // );

      if (isScheduledTime) {
        // console.log(
        //   "triggerScheduledServiceActivityNotifications alertTemplate:",
        //   alertTemplate
        // );

        await sendScheduledNotification(alertTemplate);
      }
    }
  } catch (error) {
    console.log("triggerScheduledWarrantyNotifications error:", error);
  }
}

async function sendScheduledNotification(alertTemplate: NotificationModel) {
  const { alertType, itemHave, expired, activityExpiry } = alertTemplate;
  let notificationHistory = [];

  const targetAudienceItems: Array<any> =
    await NotificationTargetAudienceService.filterAlertTargetAudience(
      alertTemplate
    );
  // console.log(
  //   "sendScheduledNotification targetAudienceItems:",
  //   targetAudienceItems
  // );

  for (const itemDetails of targetAudienceItems) {
    const {
      deviceId,
      deviceName,
      manufacturer,
      category,
      warranties,
      serviceActivities,
      receipient,
    } = itemDetails;
    // console.log("itemDetails:", itemDetails);

    let scheduledNotificationBody: any;
    const strAlertType: String = alertType.toString();
    switch (strAlertType) {
      case "scheduledWarranty":
        scheduledNotificationBody =
          await NotificationTemplateService.checkWarrantyAlertTemplate(
            expired,
            deviceName,
            category,
            manufacturer,
            warranties
          );
        break;
      case "scheduledServiceActivity":
        scheduledNotificationBody =
          await NotificationTemplateService.checkServiceAlertTemplate(
            activityExpiry,
            deviceName,
            category,
            manufacturer,
            serviceActivities
          );
        break;

      default:
        break;
    }

    for (const receipientDetails of receipient) {
      const { _id, fcm_token, mobile, email, city, countryName } =
        receipientDetails;

      const message = {
        tokens: [fcm_token],
        data: {
          screenName: itemHave ? "ItemDetails" : "",
          itemInfo: itemHave ? deviceId.toString() : "",
        },
        notification: {
          title:
            strAlertType == "scheduledWarranty"
              ? "Warranty Alert"
              : "Service Activity Alert",
          body: scheduledNotificationBody,
        },
      };
      // console.log("itemDetails receipientDetails fcm message:", message);

      const sendNotificationStatus = await sendNotification(message);
      console.log(
        "itemDetails receipientDetails fcm sendNotificationStatus:",
        sendNotificationStatus
      );

      let notificationObj = {
        mobile: mobile ? mobile : "",
        email: email ? email : "",
        user_Id: _id.toString(),
        city: city ? city : "",
        countryName: countryName,
        userDeviceId: itemHave ? deviceId.toString() : "",
        messageBody: message.notification,
        sendAt: Date.now(),
      };

      notificationHistory.push(notificationObj); // push notification obj in notification history
    }
  }

  // bulk notification histoy
  await NotificationHistoryModel.insertMany(notificationHistory);
}

export default [
  HelloController,
  HelloObjectController,
  MyController,
  UserController,
  DeviceController,
  ManuController,
  ProductController,
  WarrentyController,
  WarrantyTemplateContrl,
  ImageController,
  CountryController,
  ServiceCenterController,
  NotificationController,
  ServiceActivityController,
  // The IOC controllers
  HelloIocDirectController,
  HelloIocInterfaceController,

  // Don't forget to load these services, or IOC won't find them.
  IocHelloService,
  HelloServiceBase,
  HelloServiceImpl,
  ConfigController,
  SharedDeviceController,
];
