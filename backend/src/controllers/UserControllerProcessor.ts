import * as NodeCache from 'node-cache';
import { AuthDetails } from '../models/AuthDetails';
import { UserOps } from '../models/ops/UserOps';
import { User,UserModel } from '../models/User';
import { IOTPResponse } from '../types/ScampiiTypes';
import { ScampiiUtils } from '../utils/ScampiiUtils';
import { SMSService } from '../utils/SMSService';
import { LoginEmailService } from '../utils/LoginEmailService';


/**
 * Created by rohit on 25/03/19.
 */

export class UserControllerProcessor {

    public static cache: NodeCache = new NodeCache({
        checkperiod:600,
        deleteOnExpire:true,
        stdTTL:600,
    });

    public static async getEmailUsers(email:string): Promise<any> {
        try {
            const response: IOTPResponse = {};
        const userOps = new UserOps('email',email);
        const getemailuser = await userOps.findUser();
        if(!getemailuser) {
            response.success = false;
            response.message = `User not found for email ${email}`;
            return Promise.resolve(response);
        }else{
              return  getemailuser; 
        }  
        } catch (error) {
            return  error; 
        }
    }

    public static async isSessionActive(mobile: string): Promise<any> {
        const response: IOTPResponse = {};
        const userOps = new UserOps('mobile',mobile);
        const userAuthToken = await userOps.findUser();
        if(!userAuthToken) {
            response.success = false;
            response.message = `User not found for mobile ${mobile}`;
            return Promise.resolve(response);
        }  
        const checkSession=userAuthToken.authToken;
        if(checkSession.length>0){
            return true;
        }else{
            return false;
        }  
    }

    public static async isEmailSessionActive(email: string): Promise<any> {
        const response: IOTPResponse = {};
        const userOps = new UserOps('email',email);
        const userAuthToken = await userOps.findUser();
        if(!userAuthToken) {
            response.success = false;
            response.message = `User not found for email ${email}`;
            return Promise.resolve(response);
        }  
        const checkSession=userAuthToken.authToken;
        if(checkSession.length>0){
            return true;
        }else{
            return false;
        }  
    }

    public static async logout(mobile: string): Promise<any> {
        try {
            const response: IOTPResponse = {};
        const userOps = new UserOps('mobile',mobile);
        const getuser = await userOps.findUser();
        if(!getuser) {
            response.success = false;
            response.message = `User not found for mobile ${mobile}`;
            return Promise.resolve(response);
        }else{
            var logoutSession=UserModel.updateMany(
                { mobile: mobile },
                { $unset : { authToken : 1} }
              );
              return  logoutSession; 
        }  
        } catch (error) {
            return  error; 
        }
    }

    public static async logoutemail(email:string): Promise<any> {
        try {
            const response: IOTPResponse = {};
        const userOps = new UserOps('email',email);
        const getemailuser = await userOps.findUser();
        if(!getemailuser) {
            response.success = false;
            response.message = `User not found for email ${email}`;
            return Promise.resolve(response);
        }else{
            var logoutSession=UserModel.updateMany(
                { email:email},
                { $unset : { authToken : 1} }
              );
              return  logoutSession; 
        }  
        } catch (error) {
            return  error; 
        }
    }

    public static async generateOTP(mobile: string,countryName:string,fcm_token:string): Promise<any> {
        if(mobile === '2971215073') {
            return await this.hardCodedOTP(mobile,countryName,fcm_token);
        }
        var sendOTP;
        const authDetails: AuthDetails = ScampiiUtils.generateOTP();
        UserControllerProcessor.cache.set(mobile,authDetails.mobileOTP);
        // Lets update the user async with the auth token.
        // sending OTP sms.
        var finduser:User = await UserModel.findOne({mobile:mobile},{otpCount:1,otpRequestTime:1});
        if(finduser!=null){
            if(finduser.otpCount==0){
                const userOps = new UserOps('mobile',mobile);
                finduser.otpCount=1;
                finduser.countryName=countryName;
                finduser.fcm_token=fcm_token;
                const authDetails: AuthDetails = ScampiiUtils.generateOTP();
                sendOTP = await SMSService.sendOTPMessage(mobile, authDetails.mobileOTP);
            UserControllerProcessor.cache.set(mobile,authDetails.mobileOTP);
            finduser.mobileOTP=authDetails.mobileOTP;
                finduser = await userOps.updateUser(finduser);
                return Promise.resolve({otpCount:1});
            }
        else if(finduser.otpCount==1){
            const userOps = new UserOps('mobile',mobile);
            finduser.otpCount=2;
            finduser.countryName=countryName;
            finduser.fcm_token=fcm_token;
            const authDetails: AuthDetails = ScampiiUtils.generateOTP();
            sendOTP = await SMSService.sendOTPMessage(mobile, authDetails.mobileOTP);
        UserControllerProcessor.cache.set(mobile,authDetails.mobileOTP);
        finduser.mobileOTP=authDetails.mobileOTP;
            finduser = await userOps.updateUser(finduser);
                return Promise.resolve({otpCount:2});
        }else if(finduser.otpCount==2){
            const userOps = new UserOps('mobile',mobile);
            finduser.otpCount=3;
            finduser.countryName=countryName;
            finduser.fcm_token=fcm_token;
            const authDetails: AuthDetails = ScampiiUtils.generateOTP();
            sendOTP = await SMSService.sendOTPMessage(mobile, authDetails.mobileOTP);
        UserControllerProcessor.cache.set(mobile,authDetails.mobileOTP);
        finduser.mobileOTP=authDetails.mobileOTP;
            finduser = await userOps.updateUser(finduser);
              return Promise.resolve({otpCount:3});
        }else if(finduser.otpCount==3){
            const userOps = new UserOps('mobile',mobile);
            finduser.otpCount=4;
            finduser.countryName=countryName;
            finduser.fcm_token=fcm_token;
            const authDetails: AuthDetails = ScampiiUtils.generateOTP();
            sendOTP = await SMSService.sendOTPMessage(mobile, authDetails.mobileOTP);
        UserControllerProcessor.cache.set(mobile,authDetails.mobileOTP);
        finduser.mobileOTP=authDetails.mobileOTP;
            finduser = await userOps.updateUser(finduser);
           return Promise.resolve({otpCount:4});
        }else if(finduser.otpCount==4){
            const userOps = new UserOps('mobile',mobile);
            finduser.countryName=countryName;
            finduser.fcm_token=fcm_token;
            finduser.otpCount=5;
            const authDetails: AuthDetails = ScampiiUtils.generateOTP();
            sendOTP = await SMSService.sendOTPMessage(mobile, authDetails.mobileOTP);
        UserControllerProcessor.cache.set(mobile,authDetails.mobileOTP);
        finduser.mobileOTP=authDetails.mobileOTP;
        const updateTime = new Date();
        const currentTime = updateTime.getTime();
        finduser.otpRequestTime=currentTime;
            finduser = await userOps.updateUser(finduser);
            return Promise.resolve({otpCount:5});
        }else{
           var otpTime:any=finduser.otpRequestTime;
            //console.log(otpTime);
            const updateTime1 = new Date();
            const currentTime1 = updateTime1.getTime();
            //console.log(currentTime1);
            var difference = currentTime1 - otpTime;
            var resultInHours = Math.round(difference / 60000)/60;
            //console.log(resultInHours);
           if(finduser.otpCount >= 5 && resultInHours >= 24){
                const userOps = new UserOps('mobile',mobile);
                finduser.otpCount=1;
                finduser.countryName=countryName;
                finduser.fcm_token=fcm_token;
                finduser.otpRequestTime=currentTime1;
                const authDetails: AuthDetails = ScampiiUtils.generateOTP();
                sendOTP = await SMSService.sendOTPMessage(mobile, authDetails.mobileOTP);
                UserControllerProcessor.cache.set(mobile,authDetails.mobileOTP);
                finduser.mobileOTP=authDetails.mobileOTP;
                finduser = await userOps.updateUser(finduser);
                return Promise.resolve({otpCount:1});
            }
            const getTime:any = finduser.otpRequestTime;
            const getTimeInHumanFormat=new Date (getTime);
            return Promise.resolve({"Message":"User is Blocked" ,"Blocked Time:": getTimeInHumanFormat});
        }
        }else{
        const user: Promise<User> = UserControllerProcessor.createUser(mobile, authDetails,countryName,fcm_token);
        sendOTP = await SMSService.sendOTPMessage(mobile, authDetails.mobileOTP);
        ScampiiUtils.logPromiseResults(user);
        console.log(sendOTP);
        console.log(` returning SMS otp for user for mobile ${mobile}` );
        return Promise.resolve({message: 'SMS sent to the user for authentication.'});
    }
}

public static async generateEmailOTP(email: string, countryName:string,fcm_token:string): Promise<any> {
    var sendOTPEmail;
    const authDetails: AuthDetails = ScampiiUtils.generateEmailOTP();
    UserControllerProcessor.cache.set(email,authDetails.mailOTP);
    var finduser:User = await UserModel.findOne({email:email},{otpCount:1,otpRequestTime:1});
    if(finduser!=null){
        if(finduser.otpCount==0){
             const userOps = new UserOps('email',email);
            finduser.otpCount=1;
            finduser.countryName=countryName;
            finduser.fcm_token=fcm_token;
            const authDetails: AuthDetails = ScampiiUtils.generateEmailOTP();
            UserControllerProcessor.cache.set(email,authDetails.mailOTP);
            sendOTPEmail = await LoginEmailService.sendOTPEmail(email, authDetails.mailOTP);
        finduser.emailOTP=authDetails.mailOTP;
            finduser = await userOps.updateUser(finduser);
            return Promise.resolve({otpCount:1});
        }
    else if(finduser.otpCount==1){
        const userOps = new UserOps('email',email);
        finduser.otpCount=2;
        finduser.countryName=countryName;
        finduser.fcm_token=fcm_token;
        const authDetails: AuthDetails = ScampiiUtils.generateEmailOTP();
        UserControllerProcessor.cache.set(email,authDetails.mailOTP);
        sendOTPEmail = await LoginEmailService.sendOTPEmail(email, authDetails.mailOTP);
         console.log(sendOTPEmail);
    finduser.emailOTP=authDetails.mailOTP;
        finduser = await userOps.updateUser(finduser);
        return Promise.resolve({otpCount:2});
    }else if(finduser.otpCount==2){
        const userOps = new UserOps('email',email);
            finduser.otpCount=3;
            finduser.countryName=countryName;
            finduser.fcm_token=fcm_token;
            const authDetails: AuthDetails = ScampiiUtils.generateEmailOTP();
            UserControllerProcessor.cache.set(email,authDetails.mailOTP);
             sendOTPEmail = await LoginEmailService.sendOTPEmail(email, authDetails.mailOTP);
            finduser.emailOTP=authDetails.mailOTP;
            finduser = await userOps.updateUser(finduser);
            return Promise.resolve({otpCount:3});
    }else if(finduser.otpCount==3){
        const userOps = new UserOps('email',email);
            finduser.otpCount=4;
            finduser.countryName=countryName;
            finduser.fcm_token=fcm_token;
            const authDetails: AuthDetails = ScampiiUtils.generateEmailOTP();
            UserControllerProcessor.cache.set(email,authDetails.mailOTP);
             sendOTPEmail = await LoginEmailService.sendOTPEmail(email, authDetails.mailOTP);
            finduser.emailOTP=authDetails.mailOTP;
            finduser = await userOps.updateUser(finduser);
            return Promise.resolve({otpCount:4});
    }else if(finduser.otpCount==4){
        const userOps = new UserOps('email',email);
        finduser.otpCount=5;
        finduser.countryName=countryName;
        finduser.fcm_token=fcm_token;
        const authDetails: AuthDetails = ScampiiUtils.generateEmailOTP();
        UserControllerProcessor.cache.set(email,authDetails.mailOTP);
         sendOTPEmail = await LoginEmailService.sendOTPEmail(email, authDetails.mailOTP);
      finduser.emailOTP=authDetails.mailOTP;
      const updateTime = new Date();
      const currentTime = updateTime.getTime();
      finduser.otpRequestTime=currentTime;
        finduser = await userOps.updateUser(finduser);
        return Promise.resolve({otpCount:5});
    }else{
       var otpTime:any=finduser.otpRequestTime;
        //console.log(otpTime);
        const updateTime1 = new Date();
        const currentTime1 = updateTime1.getTime();
        //console.log(currentTime1);
        var difference = currentTime1 - otpTime;
        var resultInHours = Math.round(difference / 60000)/60;
        //console.log(resultInHours);
       if(finduser.otpCount >= 5 && resultInHours >= 24){
        const userOps = new UserOps('email',email);
        finduser.otpCount=1;
        finduser.countryName=countryName;
        finduser.fcm_token=fcm_token;
        const authDetails: AuthDetails = ScampiiUtils.generateEmailOTP();
        UserControllerProcessor.cache.set(email,authDetails.mailOTP);
         sendOTPEmail = await LoginEmailService.sendOTPEmail(email, authDetails.mailOTP);
    finduser.emailOTP=authDetails.mailOTP;
        finduser = await userOps.updateUser(finduser);
        return Promise.resolve({otpCount:1});
        }
        const getTime:any = finduser.otpRequestTime;
        const getTimeInHumanFormat=new Date (getTime);
        return Promise.resolve({"Message":"User is Blocked" ,"Blocked Time:": getTimeInHumanFormat});
    }
    }else{
     const user: Promise<User> = UserControllerProcessor.createEmailUser(email, authDetails);
     sendOTPEmail = await LoginEmailService.sendOTPEmail(email, authDetails.mailOTP);
    console.log(sendOTPEmail);
     ScampiiUtils.logPromiseResults(user);
    console.log(` returning SMS otp for user for email ${email}` );
     return Promise.resolve({message: 'Email sent to the user for authentication.'});
}
}

    public static async validateOTP(mobile: string, otp: string): Promise<IOTPResponse> {
        const userOps = new UserOps('mobile',mobile);
        const response: IOTPResponse = {}; 
        const user = await userOps.findUser();
        if(!user) {
            response.success = false;
            response.message = `User not found for mobile ${mobile}`;
            return Promise.resolve(response);
        }  
        const cacheOTP = UserControllerProcessor.cache.get(mobile);
        if (cacheOTP === otp && user.mobileOTP === otp) {
            response.message = 'Validation successful';
            response.success = true;
            response.authToken = ScampiiUtils.generateAuthToken();
            if(user.authToken) { 
                if( Array.isArray(user.authToken)) {
                    user.authToken = new Array();
                user.authToken.push(response.authToken);
                } else {
                    user.authToken = new Array();
                user.authToken.push(response.authToken);
                }
            } else {
                user.authToken = new Array();
                user.authToken.push(response.authToken);
            }

            // lets update the user async
            user.otpCount = 0;
            userOps.updateUser(user);
            return Promise.resolve(response);
        }
        response.success = false;
        return Promise.resolve(response);
    }

    public static async validateEmailOTP(email: string, otp: string): Promise<IOTPResponse> {
        const userOps = new UserOps('email',email);
        const response: IOTPResponse = {};
        const user = await userOps.findUser();
        if(!user) {
            response.success = false;
            response.message = `User not found for email ${email}`;
            return Promise.resolve(response);
        }
        const cacheOTP = UserControllerProcessor.cache.get(email);
        if (cacheOTP === otp && user.emailOTP === otp) {
            response.message = 'Validation successful';
            response.success = true;
            response.authToken = ScampiiUtils.generateAuthToken();
            if(user.authToken) { 
                if( Array.isArray(user.authToken)) {
                    user.authToken = new Array();
                user.authToken.push(response.authToken);
                } else {
                    user.authToken = new Array();
                user.authToken.push(response.authToken);
                }
            } else {
                user.authToken = new Array();
                user.authToken.push(response.authToken);
            }
            user.otpCount = 0;
            userOps.updateUser(user);
            return Promise.resolve(response);
        }
        response.success = false;
        return Promise.resolve(response);
    }


    public static async createUser(mobile: string, authDetails: AuthDetails,countryName:string,fcm_token:string): Promise<User> {
        console.log('User creation started for the user ' + mobile);
                const userOps = new UserOps('mobile', mobile);
           let user = await userOps.createUserIfNotExists();
           user.mobileOTP = authDetails.mobileOTP;
           user.countryName = countryName;
           user.fcm_token = fcm_token;
           user = await userOps.updateUser(user);
           return Promise.resolve(user);
 
    }

    public static async createEmailUser(email: string, authDetails: AuthDetails): Promise<User> {
        console.log('User creation started for the user ' + email);
           const userOps = new UserOps('email', email);
           let user = await userOps.createUserIfNotExists();
           user.emailOTP = authDetails.mailOTP;
           user = await userOps.updateUser(user);
           return Promise.resolve(user);
  
    }

    public static async updateUser(user: User) : Promise<User> {
        console.log('User update started for the user ' + user.email);
        try {
            const userOps = new UserOps('email', user.email);
            user = await userOps.updateUser(user);
            return Promise.resolve(user);
        }catch (err) {
            return Promise.reject(err);
        }
    }

    private static async hardCodedOTP(mobile: string,countryName:string,fcm_token:string) {
        const otp = "999983";
        const details = {mobileOTP: otp};
        await UserControllerProcessor.createUser(mobile, details,countryName,fcm_token);
        await UserControllerProcessor.cache.set(mobile, details.mobileOTP);
        return Promise.resolve({otp: otp});
    }

}

// async function test() {
//     UserControllerProcessor.cache.set('test','testing');
//     setTimeout(()=>console.log(UserControllerProcessor.cache.get('test')),2000);
//
// }
//
// test();
