import { ValidationResult } from 'joi';
import { Types } from 'mongoose';
import {DELETE, GET, Path, PathParam, POST, PUT, QueryParam, Security} from 'typescript-rest';
import {BadRequestError, NotFoundError} from "typescript-rest/dist/server/model/errors";
import { ScampiiMongoose } from '../db/ScampiiMongoose';
import { Device, DeviceModel } from '../models/Device';
import { Manufacturer, ManufacturerModel } from '../models/Manufacturer';
import { Product, ProductModel } from '../models/Product';
import * as Warranty from '../models/Warrenty';
import {IExtendedWarranty, IWarrantyCover, WarrantyCover, WarrantyCoverModel, WarrantyModel} from '../models/Warrenty';
import { ScampiiController } from './ScampiiController';
import ObjectId = Types.ObjectId;


/**
 * Created by rohit on 22/08/19.
 */

@Path('/warranty')
@Security()
export class WarrentyController extends ScampiiController {

    constructor() {
        super();
        ScampiiMongoose.initDB();
    }

    @POST
    @Path('/product')
    public async addForProduct(warranty: Warranty.Warranty, @QueryParam('deviceId') deviceId: string, @QueryParam('productId') productId: string): Promise<Warranty.Warranty> {
        const device: Device = await DeviceModel.findOne({_id: new ObjectId(deviceId)});
        if (!device) {throw Error(`Not able to find device for id ${deviceId}`);}

        const prod: Product = await ProductModel.findOne({_id: new ObjectId(productId)});
        if(!prod) {throw Error(`Not able to find product for id ${productId}`);}

        warranty.device = device;
        warranty.product = prod;

        const result: ValidationResult<Warranty.Warranty> = Warranty.validateWarranty(warranty);
        if(result && result.error) {
            {throw Error(`Warranty.Warranty details are not correct to create ${result.error}`);}
        }
        return Warranty.WarrantyModel.create(warranty);
    }

    @POST
    @Path('/manu')
    public async addForManu(warranty: Warranty.Warranty, @QueryParam('deviceId') deviceId: string, @QueryParam('manuId') manuId: string): Promise<Warranty.Warranty> {
        const device: Device = await DeviceModel.findOne({_id: new ObjectId(deviceId)});
        if (!device) {throw Error(`Not able to find device for id ${deviceId}`);}

        const manu: Manufacturer = await ManufacturerModel.findOne({_id: new ObjectId(manuId)});
        if(!manu) {throw Error(`Not able to find product for id ${manuId}`);}

        warranty.device = device;
        warranty.manufacturer = manu;

        const result: ValidationResult<Warranty.Warranty> = Warranty.validateWarranty(warranty);
        if(result && result.error) {
            {throw Error(`Warranty.Warranty details are not correct to create ${result.error}`);}
        }
        return Warranty.WarrantyModel.create(warranty);
    }

    @GET
    public async getAll(): Promise<Array<Warranty.Warranty>> {
        return WarrantyModel.find();
    }

    @GET
    @Path('covers')
    public async getCovers(): Promise<Array<IWarrantyCover>> {
        const covers = await WarrantyCoverModel.find().sort({'label': 1});
        return Promise.resolve(covers);
    }

    @DELETE
    @Path(' covers/:id')
    public async deleteCover(@PathParam('id') id: string): Promise<any> {
        const result = await WarrantyCoverModel.deleteOne({_id: new ObjectId(id)});
        return Promise.resolve(result);
    }

    @PUT
    @Path(' covers/:id')
    public async updateCoverDetails(@PathParam('id') id: string, cover: IWarrantyCover): Promise<any> {
        const result = await WarrantyCoverModel.updateOne({_id: new ObjectId(id)}, cover);
        return Promise.resolve(result);
    }

    @PUT
    @Path(' extendedWarranty/:id')
    public async updateExtendedWarranty(@PathParam('id') id: string, extendedWarranty: IExtendedWarranty): Promise<any> {
        const warr = await WarrantyModel.findOne({_id: new ObjectId(id)});
        if(!warr) { return Promise.reject(new NotFoundError(`Warranty not found with id ${id}`))}
        if(!extendedWarranty.previousWarrantyId || !extendedWarranty.extendedServiceContract) {
            return Promise.reject(new BadRequestError(`Previous Warranty id  and extended service contract is mandatory.`));
        }
        if(extendedWarranty.previousWarrantyId === id) {
            return Promise.reject(new BadRequestError(`Previous Warranty id ${extendedWarranty.previousWarrantyId} and current warranty id ${id} cannot be same.`));
        }
        warr.extendedWarrantyDetails = extendedWarranty;
        await WarrantyModel.updateOne({_id: new ObjectId(id)}, warr);
        return WarrantyModel.findOne({_id: new ObjectId(id)});
    }

    @POST
    @Path('covers')
    public async addCover(cover: IWarrantyCover): Promise<WarrantyCover> {
        const wCover = await WarrantyCoverModel.create(cover);
        return Promise.resolve(wCover);
    }

}

// async function test() {
//     await ScampiiMongoose.initDB();
//     const warrentyController = new WarrentyController();
//     const details = await warrentyController.updateExtendedWarranty('5e71cb95e4193f3a6d81a60a',
//         {extendedServiceContract:true, previousWarrantyId: '5e71cba3e4193f3a6d81a60e'});
//     console.log(JSON.stringify(details));
//     await ScampiiMongoose.closeCon();
//
// }
//
// test();
