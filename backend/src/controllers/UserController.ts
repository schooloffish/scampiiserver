import { DELETE, GET, Path, PathParam, POST, PUT, Security } from 'typescript-rest';
import {ForbiddenError, NotFoundError} from "typescript-rest/dist/server/model/errors";
import { AuthDetails } from '../models/AuthDetails';
import { UserOps } from '../models/ops/UserOps';
import { User, UserModel } from '../models/User';
import {ILocation, UserLocation, UserLocationModel} from "../models/UserLocation";
import { IOTPResponse } from '../types/ScampiiTypes';
import {ScampiiUtils} from "../utils/ScampiiUtils";
import { ScampiiController } from './ScampiiController';
import { UserControllerProcessor } from './UserControllerProcessor';
import { Types } from 'mongoose';
import ObjectId = Types.ObjectId;

@Path('/users')
export class UserController extends ScampiiController{

    constructor(){
        super();
    }

    @GET
    @Security()
    public async users(): Promise<Array<User>> {
        const users: Array<User> = await UserModel.find();
        const res:Array<any> = new Array<any>();
        for (const user of users) {
            const userObj = {} as any;
            if(user.name) {
                userObj['name'] = user.name;
            }
            if(user.city) {
                userObj['city'] = user.city;
            }
            userObj['mobile'] = user.mobile;
            if(user.image) {
                userObj['image'] = user.image;
            }
            res.push(userObj);
        }
        return Promise.resolve(res);
    }

    @Path('email/:email')
    @GET
    @Security()
    public async getEmailUsers(@PathParam('email') email: string): Promise<User> {
        return UserControllerProcessor.getEmailUsers(email);
    }

    @Path('/locations')
    @GET
    @Security()
    public async locations(): Promise<Array<UserLocation>> {
        const user = this.context.request.user;
        return UserLocationModel.find({users: new ObjectId(user._id)});
    }

    @Path(':mobile')
    @GET
    @Security()
    public async getUser(@PathParam('mobile') mobile: string): Promise<User> {
        return new UserOps('mobile',mobile).findUser();
    }

    @Path('/mobile/:mobile')
    @POST
    @Security()
    public async createNewUser(@PathParam('mobile') mobile: string , user: User): Promise<User> {
        const userOps = new UserOps('mobile',mobile);
        const user2: User = await userOps.findUser();
        if(user2) {
            return this.updateUserMobile(mobile,user);
        } else {
            await userOps.createUserIfNotExists();
            return this.updateUserMobile(mobile,user);
        }
    }

    @DELETE
    @Path(':email')
    @Security()
    public async delete(@PathParam('email') email: string): Promise<User> {
        return new UserOps('email',email).findUser();
    }

    @Path('/email/:email')
    @PUT
    @Security()
    public async updateUserEmail(@PathParam('email') email: string, user: User): Promise<User> {
        const userOps: UserOps = new UserOps('email',email);
        const response = await userOps.updateUser(user);
        return this.handleEntityUpdate(response,userOps.condition,UserModel);
    }

    @Path('/token/:mobile')
    @PUT
    @Security()
    public async updateToken(@PathParam('mobile') mobile: string, token: any): Promise<User> {
        // probably will need a optimized function to update the user profile only.
        // In case there are other relationships we will have to take care of that later.
        const user: any = await UserModel.find({mobile:mobile});
        user[0].authToken.push(token.token);
        await UserModel.updateOne({mobile:mobile}, user[0]);
        return Promise.resolve(user);
    }

    @Path('/deviceId')
    @PUT
    @Security()
    public async updatedeviceId(deviceId: any): Promise<User> {
        // probably will need a optimized function to update the user profile only.
        // In case there are other relationships we will have to take care of that later.
        const user: User = await UserModel.findOne({mobile:this.context.request.user.mobile});
        user.userDeviceId = deviceId.userDeviceId;
        await UserModel.updateOne({mobile:this.context.request.user.mobile}, user);
        return Promise.resolve(user);
    }

    @Path('/mobile/:mobile')
    @PUT
    @Security()
    public async updateUserMobile(@PathParam('mobile') mobile: string, user: User): Promise<User> {
        // probably will need a optimized function to update the user profile only.
        // In case there are other relationships we will have to take care of that later.
        const userOps: UserOps = new UserOps('mobile',mobile);
        const response = await userOps.updateUser(user);
        return this.handleEntityUpdate(response,userOps.condition,UserModel);
    }

    @Path('/otp/generate/:mobile/:countryName/:fcm_token')
    @GET
    public async generateOTP(@PathParam('mobile') mobile: string,@PathParam('countryName') countryName:string, @PathParam('fcm_token') fcm_token:string): Promise<AuthDetails> {
        return UserControllerProcessor.generateOTP(mobile,countryName,fcm_token);
    }

    @Path('/emailotp/generate/:email/:countryName/:fcm_token')
    @GET
    public async generateEmailOTP(@PathParam('email') email: string,@PathParam('countryName') countryName:string, @PathParam('fcm_token') fcm_token:string): Promise<AuthDetails> {
        return UserControllerProcessor.generateEmailOTP(email,countryName,fcm_token);
    }

    @Path('/oAuthLogin/:email')
    @GET
    public async oAuthLogin(@PathParam('email') email: string): Promise<User> {
        const user: User = await UserModel.findOne({email:email});
        if(!user) {
            throw new NotFoundError(`User with ${email} not found.`);
        }
        if(!user.isAdmin) {
            throw new ForbiddenError(`User with ${email} is not an admin.`);
        }
        if(!user.authToken || user.authToken.length === 0) {
            user.authToken = new Array<string>();
            const authToken = ScampiiUtils.generateAuthToken();
            user.authToken.push(authToken);
        }
        return Promise.resolve(user);
    }

    @Path('/otp/validate/:mobile/:otp')
    @GET 
    public async validateOTP(@PathParam('mobile') mobile: string, @PathParam('otp') otp: string): Promise<IOTPResponse> {
        console.log(`calling with the user details`);
        return UserControllerProcessor.validateOTP(mobile,otp);
    }

    @Path('/otp/emailvalidate/:email/:otp')
    @GET 
    public async validateEmailOTP(@PathParam('email') email: string, @PathParam('otp') otp: string): Promise<IOTPResponse> {
        console.log(`calling with the user details`);
        return UserControllerProcessor.validateEmailOTP(email,otp);
    }

    @Path('/isSessionActive/:mobile')
    @GET
    public async isSessionActive(@PathParam('mobile') mobile: string): Promise<AuthDetails> {
        return UserControllerProcessor.isSessionActive(mobile);
    }

    @Path('/isEmailSessionActive/:email')
    @GET
    public async isEmailSessionActive(@PathParam('email') email: string): Promise<AuthDetails> {
        return UserControllerProcessor.isEmailSessionActive(email);
    }

    @Path('/logout/:mobile')
    @GET
    //@Security()
    public async logout(@PathParam('mobile') mobile: string): Promise<AuthDetails> {
        return UserControllerProcessor.logout(mobile);
    }

    @Path('/emailLogout/:email')
    @GET
    //@Security()
    public async logoutemail(@PathParam('email') email:string): Promise<AuthDetails> {
        return UserControllerProcessor.logoutemail(email);
    }

    @GET
    @Path('/search/:query')
    @Security()
    public async search(@PathParam('query') query: string) : Promise<User> {
        const searchStr = {$or: [ { name: query },
            { mobile: query},
            { email: query } ]};
        try {
            const user = await UserModel.find(searchStr);
            const userObj = {} as any;
            if(user[0].name) {
                userObj['name'] = user[0].name;
            }
            if(user[0].city) {
                userObj['city'] = user[0].city;
            }
            userObj['mobile'] = user[0].mobile;
            return Promise.resolve(userObj);
        }catch (e) {
            console.log(e);
            throw new NotFoundError(`User with ${query} not found.`);
        }
    }

    @Path('/location/:mobile')
    @POST
    @Security()
    public async addUserLocation(@PathParam('mobile') mobile: string , location: ILocation): Promise<any> {
        const userOps = new UserOps('mobile',mobile);
        const user2: User = await userOps.findUser();
        if(!user2) {throw Error(`Not able to find user with mobile ${mobile}`);}

        const loc: UserLocation = await UserLocationModel.create(location);
        if (!loc.users) { loc.users = new Array();}
        loc.users.push(user2);
        await UserLocationModel.updateOne({_id:loc._id}, loc);
        if( !user2.locations) {user2.locations = new Array(); }
        user2.locations.push(loc);
        await UserModel.updateOne({_id: user2._id}, user2);
        const resultLoc: any = await UserLocationModel.findOne({_id:loc._id});
        const result = { locationId: loc._id, message: 'Location added successfully.','loc':resultLoc};
        return Promise.resolve(result);
    }

    @Path('/EmailUserlocation/:email')
    @POST
    @Security()
    public async addEmailUserLocation(@PathParam('email') email: string , location: ILocation): Promise<any> {
        const userOps = new UserOps('email',email);
        const user2: User = await userOps.findUser();
        if(!user2) {throw Error(`Not able to find user with email ${email}`);}

        const loc: UserLocation = await UserLocationModel.create(location);
        if (!loc.users) { loc.users = new Array();}
        loc.users.push(user2);
        await UserLocationModel.updateOne({_id:loc._id}, loc);
        if( !user2.locations) {user2.locations = new Array(); }
        user2.locations.push(loc);
        await UserModel.updateOne({_id: user2._id}, user2);
        const resultLoc: any = await UserLocationModel.findOne({_id:loc._id});
        const result = { locationId: loc._id, message: 'Location added successfully.','loc':resultLoc};
        return Promise.resolve(result);
    }

}

async function test() {
    await new UserController().generateOTP("9845065113","India","testfcmtoken");
    // console.log(dev.warranties);
}

test();
