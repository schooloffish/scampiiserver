import * as cors from "cors";
import * as express from "express";
import * as fs from "fs";
import * as http from "http";
import * as https from "https";
import * as path from "path";
import { PassportAuthenticator, Server } from "typescript-rest";
import controllers from "./controllers";
import { Strategy } from "passport-http-bearer";
import { User, UserModel } from "./models/User";
import { ScampiiMongoose } from "./db/ScampiiMongoose";
import { HttpError } from "typescript-rest/dist/server/model/errors";
import bodyParser = require("body-parser");

export class ApiServer {
  // Local HTTP_PORT
  // public HTTP_PORT: number = +process.env.PORT || 7080;

  // Prod & Dev HTTP_PORT
  public HTTP_PORT: number = +process.env.PORT || 80;

  public HTTPS_PORT: number = +process.env.HTTPS_PORT || 443;
  private readonly app: express.Application;
  private server: http.Server = null;
  private strategy = new Strategy(function (token, done) {
    UserModel.findOne({ authToken: `Bearer ${token}` })
      .then((user: User) => {
        if (user) {
          return done(null, user, { message: "success", scope: "all" });
        } else {
          return done(null, null, { message: "failure", scope: "all" });
        }
      })
      .catch((err) => {
        return done(err, null, { message: "failure", scope: "all" });
      });
  });
  private passportOpts = {
    deserializeUser: function (user: string) {
      return JSON.parse(user);
    },
    serializeUser: function (user: User) {
      return JSON.stringify(user);
    },
  };

  constructor() {
    ScampiiMongoose.initDB();
    this.app = express();
    this.config();
    Server.useIoC();
    Server.registerAuthenticator(
      new PassportAuthenticator(this.strategy, this.passportOpts)
    );
    Server.buildServices(this.app, ...controllers);
    // TODO: enable for Swagger generation error
    // Server.loadServices(this.app, 'controllers/*', __dirname);
    if (fs.existsSync("./dist/swagger.json")) {
      Server.swagger(this.app, { filePath: "./dist/swagger.json" });
    }
  }

  /**
   * Start the server
   * @returns {Promise<any>}
   */
  public start(): Promise<any> {
    const serverOptions = {
      // cert : fs.readFileSync('cert.pem'),
      // key  : fs.readFileSync(  'privatebiz.key')

      //Dev Certificate & Key
      // cert: fs.readFileSync("fullchain-scampii-in.pem"),
      // key: fs.readFileSync("private-scampii-in.pem"),

      //Prod Certificate & Key
      cert: fs.readFileSync("full-chain-scampii-biz.pem"),
      key: fs.readFileSync("private-scampii-biz.key"),
    };
    return new Promise<any>((resolve, reject) => {
      // https.createServer(server_options,this.app).listen(7000);
      this.server = this.app.listen(this.HTTP_PORT, (err: any) => {
        if (err) {
          return reject(err);
        }

        // TODO: replace with Morgan call
        // tslint:disable-next-line:no-console
        console.log(
          `Listening on the server at to http://127.0.0.1:${this.HTTP_PORT}`
        );
        https
          .createServer(serverOptions, this.app)
          .listen(this.HTTPS_PORT, (err1: any) => {
            if (err1) {
              return reject(err1);
            }

            // TODO: replace with Morgan call
            // tslint:disable-next-line:no-console
            console.log(
              `Listening on the https server at to http://127.0.0.1:${this.HTTPS_PORT}`
            );
            return resolve(err1);
          });
      });
    });
  }

  /**
   * Stop the server (if running).
   * @returns {Promise<boolean>}
   */
  public stop(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      if (this.server) {
        this.server.close(() => {
          return resolve(true);
        });
      } else {
        return resolve(true);
      }
    });
  }

  /**
   * Configure the express app.
   */
  private config(): void {
    // Native Express configuration
    // this.app.use( bodyParser.urlencoded( { extended: false } ) );
    // this.app.use( bodyParser.json( { limit: '1mb' } ) );
    this.app.use(
      express.static(path.join(__dirname, "public"), { maxAge: 31557600000 })
    );
    this.app.use(cors());
    this.app.use(bodyParser.json({ limit: "200mb" }));
    this.app.use(bodyParser.urlencoded({ limit: "200mb", extended: true }));
    this.app.use(
      (
        err: any,
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
      ) => {
        console.log(err);
        if (err instanceof HttpError) {
          if (res.headersSent) {
            // important to allow default error handler to close connection if headers already sent
            return next(err);
          }
          res.set("Content-Type", "application/json");
          res.status(err.statusCode);
          res.json({ error: err.message, code: err.statusCode });
        } else {
          next(err);
        }
      }
    );
  }
}
