import * as mongoose from 'mongoose';
import { ProductModel } from '../models/Product';
import { UserModel } from '../models/User';
import { ScampiiMongoose } from './ScampiiMongoose';
/**
 * Created  on 23/03/19.
 */
ScampiiMongoose.initDB();
export async function testCreate() {
    console.log('Test create is called');
    try {
        const user = {
            email:'testMail@gmail.com',
            name:'testing'
        };
        const userModel = new UserModel(user);
        const savedUser = await userModel.save();
        console.log(savedUser);
    }catch (error) {
        console.log(error);
    }
}

export async function testFetch() {
    // let newUser = await UserModel.findOne({_id:new mongoose.Types.ObjectId('5c96532422ccb7fc64169a3c')}).
    // populate('product');
    // console.log(newUser);

    ProductModel.findOne({_id:new mongoose.Types.ObjectId('5c96532522ccb7fc64169a3d')})
        .populate('user').
    exec(function (err, story) {
        if (err) { console.log(err); }
        console.log('The author is %s', story);
        // prints "The author is Ian Fleming"
    });

    // UserModel.findOne({_id:new mongoose.Types.ObjectId("5c96532422ccb7fc64169a3c")})
    //     .populate('products').
    // exec(function (err, story) {
    //     if (err) console.log(err);
    //     console.log('The author is %s', story.products);
    //     // prints "The author is Ian Fleming"
    // });
}

testCreate();


