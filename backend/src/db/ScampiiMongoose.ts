/**
 * Created  on 23/03/19.
 */
import * as mongoose from "mongoose";

export class ScampiiMongoose {
  public static async initDB() {
    if (mongoose.connection.readyState === 1) {
      return Promise.resolve("Connection is already open to db.");
    }
    // mongoose.connect('mongodb://username:password@host:port/d
    //return mongoose.connect('mongodb://scampiiuser:mong0$soft@13.232.233.60:27017/local?authSource=admin',{ useNewUrlParser: true });
    //return mongoose.connect('mongodb://localhost:27017/local');

    // Prod DB Connection
    return mongoose.connect(
      "mongodb://scampiiuser:mong0%24soft@3.7.28.63:27017/local?authSource=admin",
      { useNewUrlParser: true }
    );

    // Dev DB Connection
    // return mongoose.connect(
    //   "mongodb://scampiiuser:mong0%24soft@13.232.233.60:27017/local?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false",
    //   { useNewUrlParser: true }
    // );

    // Local DB Connection
    // return mongoose.connect("mongodb://localhost:27017/temp-Prod-Copy");
  }

  public static async closeCon() {
    return mongoose.connection.close(true);
  }
}
