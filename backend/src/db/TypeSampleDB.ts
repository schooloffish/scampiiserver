// /**
//  * Created  on 23/03/19.
//  */
// import { ModelType, prop, Typegoose } from 'typegoose';
// import * as mongoose from 'mongoose';
// import { ScampiiMongoose } from './ScampiiMongoose';
// import { User, UserModel } from '../models/sample/UserTypegoose';
//
//
//
// class Member extends Typegoose {
//     @prop({required:true})
//     name: string;
// }
//
// class Person extends Typegoose {
//     @prop({index:true, required: true})
//     name: string;
//
//     @prop()
//     member: Member;
//     public test() {
//
//     }
// }
//
// // const PersonModel = new Person().getModelForClass(Person);
//
// const PersonModel: ModelType<Person> = new Person().setModelForClass(Person, {
//     existingMongoose: mongoose,
//     schemaOptions: {collection: 'person', validateBeforeSave: true}
// });
//
// // PersonModel is a regular Mongoose Model with correct types
// (async () => {
//     await ScampiiMongoose.initDB();
//     console.log('Created DB connection successfully');
//     let person = new Person();
//     person.name = 'testing this object instance';
//     let member = new Member();
//     member.name = 'testMember';
//
//     person.member = member;
//
//     const u = new PersonModel(person);
//     try {
//         await u.save();
//         const person = await PersonModel.findOne();
//         console.log(person);
//
//         let user = new User();
//         user.name = 'testing';
//         // user._id = new mongoose.Types.ObjectId();
//         const userDetails = new UserModel(user);
//         await userDetails.save();
//
//         const newUser = await UserModel.findOne({_id:new mongoose.Types.ObjectId('5c963d12da93a3e9c8c70708')});
//         console.log(newUser);
//
//
//     }catch (error) {
//         console.log(error);
//     }
// })();
//
