import * as mongoose from 'mongoose';
import { Types } from 'mongoose';
import { Manufacturer } from './Manufacturer';
import { Product } from './Product';
import { ScampiiSchema } from './ScampiiSchema';
import ObjectId = Types.ObjectId;

/**
 * Created by rohit on 23/04/19.
 */

export interface ServiceOffering extends mongoose.Document {
    _id: ObjectId;
    name: string;
    product: Product;
    manufacturer: Manufacturer;
}

const ServiceOfferingSchema = new ScampiiSchema({
    manufacturer: { type: mongoose.Schema.Types.ObjectId, ref: 'Manufacturer' },
    name: { type: String, required: true },
    product: { type: mongoose.Schema.Types.ObjectId, ref: 'Product' },
});

export const ServiceOfferingModel = mongoose.model<ServiceOffering>("ServiceOffering", ServiceOfferingSchema, 'ServiceOffering');



