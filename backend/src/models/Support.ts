import * as mongoose from 'mongoose';
import { ScampiiSchema } from './ScampiiSchema';


export interface Support extends mongoose.Document {
    email: any;
    password: any;
}

const SupportScheme = new ScampiiSchema({
    email:{type:String},
    password: {type:String},
});

 
 export const SupportModel: mongoose.Model<Support> = mongoose.model('Support',SupportScheme,'Support');