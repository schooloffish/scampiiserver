import {number} from "joi";
import * as mongoose from 'mongoose';
import { Types } from 'mongoose';
import ObjectId = Types.ObjectId;
import { Manufacturer } from './Manufacturer';
import { ScampiiSchema } from './ScampiiSchema';

/**
 * Created by rohit on 23/04/19.
 */

export interface ServiceCenter extends mongoose.Document {
    _id: ObjectId;
    manufacturer: Manufacturer|string;
    countryCode: string;
    custCareNums: string;
    whatsappNumbers: string;
    website: string;
    address: string;
    gpsLocation?: {
        lat:number;
        lng: number;
    };
    manuName?: string;
}

const ServiceCenterSchema = new ScampiiSchema({
    address: {type: String},
    countryCode: {type: String, required: true},
    custCareNums: {type: String, required: true},
    gpsLocation:{type:{lat:number, lng:number}},
    manuName: {type: String, required: true},
    manufacturer: { type: mongoose.Schema.Types.ObjectId, ref: 'Manufacturer' },
    website: {type: String, required: true},
    whatsappNumbers: {type: String},

}, {
    strict: false,
    timestamps:true,
});

export const ServiceCenterModel = mongoose.model<ServiceCenter>("ServceCenter", ServiceCenterSchema, 'ServceCenter');



