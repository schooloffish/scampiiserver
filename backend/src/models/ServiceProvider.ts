import * as mongoose from 'mongoose';
import { Types } from 'mongoose';
import { ScampiiSchema } from './ScampiiSchema';
import { ServiceContract } from './ServiceContract';
import ObjectId = Types.ObjectId;

/**
 * Created by rohit on 23/04/19.
 */

export interface ServiceProvider extends mongoose.Document {
    _id: ObjectId;
    name: string;
    serviceContracts: Array<ServiceContract>;
}

const ServiceProviderSchema = new ScampiiSchema({
    name: { type: String, required: true },
    serviceContracts: [{ type: mongoose.Schema.Types.ObjectId, ref: 'ServiceContract' }],
});

export const ServiceProviderModel = mongoose.model<ServiceProvider>("ServiceProvider", ServiceProviderSchema, 'ServiceProvider');



