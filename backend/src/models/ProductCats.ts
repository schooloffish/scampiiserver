import * as mongoose from 'mongoose';
import { Types } from 'mongoose';
import ObjectId = Types.ObjectId;
import { LenientScampiiSchema } from './ScampiiSchema';

/**
 * Created by rohit on 10/09/19.
 */

export interface ProductCats extends mongoose.Document {
    _id: ObjectId;
    name: string;
    image: string;
}

const ProductCatsSchema = new LenientScampiiSchema({
    image: { type: String, unique: false },
    name: { type: String, required: true, unique: true },
});

export const ProductCatsModel = mongoose.model<ProductCats>("ProductCats", ProductCatsSchema, 'ProductCats');
