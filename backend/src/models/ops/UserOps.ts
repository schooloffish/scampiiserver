import { ScampiiMongoose } from '../../db/ScampiiMongoose';
import { User, UserModel } from '../User';

export class UserOps {

    public condition: any = {};

    constructor(identifier: string, value: string) {
        this.condition[identifier === 'email'?'email':'mobile'] = value;
        ScampiiMongoose.initDB();
    }

    public async deleteUser(): Promise<any> {
        return UserModel.deleteMany(this.condition);
    }

    public async findUser(): Promise<User> {
        try {
            const user: User = await UserModel.findOne(this.condition);
            if(!user) {
                return null;
            }
            console.info('Found user ' + user);
            return Promise.resolve(user);
        }catch (err) {
            console.error('Error finding user for condition ' + this.condition);
            return Promise.reject(err);
        }
        return Promise.reject('User not found for condition ' + this.condition);
    }

    public async createUserIfNotExists(): Promise<User> {
        const user:User = await UserModel.findOne(this.condition);
        if(!user) {
           // user.otpCount = 1;
            return new UserModel(this.condition).save();
        }
        return Promise.resolve(user);
    }

    public async updateUser(user: User): Promise<User> {
        return UserModel.updateOne(this.condition, user); 
    }


}
