import * as mongoose from 'mongoose';
import { Types } from 'mongoose';
import { ScampiiSchema } from './ScampiiSchema';
import { ServiceRequest } from './ServiceRequest';
import ObjectId = Types.ObjectId;

/**
 * Created by rohit on 23/04/19.
 */

export interface ServiceRequestDetail extends mongoose.Document {
    _id: ObjectId;
    name: string;
    serviceRequest:ServiceRequest;
}

const ServiceRequestDetailsSchema = new ScampiiSchema({
    name: { type: String, required: true },
    serviceRequest: { type: mongoose.Schema.Types.ObjectId, ref: 'ServiceRequest' },
});

export const ServiceRequestModel = mongoose.model<ServiceRequestDetail>("ServiceRequestDetail", ServiceRequestDetailsSchema, 'ServiceRequestDetail');



