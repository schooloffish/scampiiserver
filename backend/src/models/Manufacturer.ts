import * as mongoose from 'mongoose';
import { Types } from 'mongoose';
import { Product } from './Product';
import { ScampiiSchema } from './ScampiiSchema';
import {ServiceCenter, ServiceCenterModel} from "./ServiceCenter";
import ObjectId = Types.ObjectId;

/**
 * Created by rohit on 23/04/19.
 */

export class ManufacturerUtils {

    public static async getManufacturerForResponse(condition: any): Promise<any>  {
        const manu:Manufacturer = await ManufacturerModel.findOne(condition);
        if(!manu) { return Promise.resolve({}); }
        const resp = {} as any;
        resp.name = manu.name;
        resp.logo = manu.logo;
        resp.serviceCenters  = new Array();
        const serviceCenters: Array<ServiceCenter> = manu.serviceCenters;
        for (const serviceCenter of serviceCenters) {
            const service: ServiceCenter = await ServiceCenterModel.findOne( {_id:new ObjectId(serviceCenter.toString())});
            if(service) {
                const svr = {
                    address: service.address,
                    countryCode: service.countryCode,
                    custCareNums: service.custCareNums,
                    whatsappNumbers: service.whatsappNumbers
                };
                resp.serviceCenters.push(svr);
            }
        }
        return Promise.resolve(resp);
    }
}

export interface Manufacturer extends mongoose.Document {
    _id: ObjectId;
    id:number;
    name: string;
    logo: string;
    products: Array<Product>;
    serviceCenters: Array<ServiceCenter>;

}

const manufacturerSchema = new ScampiiSchema({
    id: {type: Number},
    logo: {type:String},
    name: { type: String, required: true },
    products: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Product' }],
    serviceCenters: [{ type: mongoose.Schema.Types.ObjectId, ref: 'ServiceCenter' }]

});

export const ManufacturerModel = mongoose.model<Manufacturer>("Manufacturer", manufacturerSchema, 'Manufacturer');
