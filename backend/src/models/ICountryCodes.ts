/**
 * Created by rohit on 05/09/19.
 */

export interface ICountryCodes {
    country: string;
    code: string;
}
