import * as Joi from 'joi';
import * as mongoose from 'mongoose';
import { Types } from 'mongoose';
import { Device } from './Device';
import { Manufacturer } from './Manufacturer';
import { Product } from './Product';
import { ScampiiSchema } from './ScampiiSchema';
import ObjectId = Types.ObjectId;


/**
 * Created by rohit on 23/04/19.
 */

export interface IWarrantyCover {
    label: string;
    value?: string;
    duration?: number;
    type: string;
    warrantyCoverIcon: string;
}

export interface WarrantyCover extends mongoose.Document, IWarrantyCover{
}

export interface IWarrantyDuration {
    label: string;
    duration: number;
}

export interface IExtendedWarranty {
    extendedServiceContract: boolean;
    previousWarrantyId: string;
}

export interface IWarranty {
    expired?: boolean;
    endDate?: Date;
    name?: string;
    warrantyStartDate: Date;
    daysLeft?:number;
    totalMonths?:number;
    covers?: Array<WarrantyCover>;
    durations?: Array<IWarrantyDuration>;
    warrantyType: string;
    warrantyTypeDuration: number;
    defaultWarranty: boolean;
    extendedWarrantyDetails : IExtendedWarranty;
    status: string;
    serviceVisit: boolean;
    serviceFrequency: number;

}

export interface Warranty extends mongoose.Document, IWarranty {
    _id: ObjectId;
    device: Device;
    manufacturer: Manufacturer;
    product: Product;
}

const WarrantySchema = new ScampiiSchema({
    covers: {type: Array},
    device: { type: mongoose.Schema.Types.ObjectId, ref: 'Device' },
    durations: {type: Array},
    endDate: {type: Date },
    extendedWarrantyDetails : {
        extendedServiceContract: {type: Boolean},
        previousWarrantyId: { type: mongoose.Schema.Types.ObjectId }
    },
    manufacturer: { type: mongoose.Schema.Types.ObjectId, ref: 'Manufacturer' },
    name: { type: String},
    product: { type: mongoose.Schema.Types.ObjectId, ref: 'Product' },
    totalMonths: {type: Number},
    warrantyStartDate: {type: Date, required:true},
    warrantyType: { type: String },
    warrantyTypeDuration: { type: String },
    status: {type: String},
    serviceVisit: {type: Boolean},
    serviceFrequency: {type: Number}

});

const CoverSchema = new ScampiiSchema({
    duration: {type: Number },
    label: {type: String, required: true, unique: true},
    type: {type: String, required: true},
    value: { type: String},
    warrantyCoverIcon: { type: String}
});

export const WarrentyValidationSchema = Joi.object().keys({
    device: Joi.object().optional(),
    manufacturer: Joi.object().optional(),
    name: Joi.string().optional(),
    product: Joi.object().optional(),
});

export const DeviceWarrentyValidationSchema = Joi.object().keys({
    covers:Joi.array().required(),
    extendedWarrantyDetails: Joi.object().optional(),
    name: Joi.string().optional(),
    warrantyStartDate: Joi.string().required(),
    warrantyTypeDuration: Joi.number().required(),
    status: Joi.string().required(),
    serviceVisit: Joi.boolean().required(),
    serviceFrequency: Joi.number().required(),

});

export const WarrantyModel = mongoose.model<Warranty>("Warranty", WarrantySchema, 'Warranty');

export const WarrantyCoverModel = mongoose.model<WarrantyCover>("WarrantyCover", CoverSchema, 'WarrantyCover');

export const validateWarranty = function (warrenty: Warranty): Joi.ValidationResult<Warranty> {
    const options: Joi.ValidationOptions = {};
    options.abortEarly = false;
    return WarrentyValidationSchema.validate(warrenty,options);
};

export const validateDeviceWarranty = function (warrenty: IWarranty): Joi.ValidationResult<IWarranty> {
    const options: Joi.ValidationOptions = {};
    options.abortEarly = false;
    return DeviceWarrentyValidationSchema.validate(warrenty,options);
};
