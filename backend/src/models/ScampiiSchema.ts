import * as mongoose from 'mongoose';
import { SchemaOptions } from 'mongoose';
import { LinientScampiiOptions, ScampiiOptions } from './ScampiiOptions';
/**
 * Created by rohit on 21/06/19.
 */
export class ScampiiSchema extends mongoose.Schema {

    constructor(schema: any, options?: SchemaOptions) {
        schema.__v = { type: Number, select: false};
        if (options) {
            super(schema,ScampiiOptions);
        } else {
            super(schema, ScampiiOptions);
        }
    }
}


export class LenientScampiiSchema extends mongoose.Schema {
    constructor(schema: any) {
            super(schema, LinientScampiiOptions);
    }
}



