import * as mongoose from "mongoose";
import { Model } from "mongoose";
import { Types } from "mongoose";
import ObjectId = Types.ObjectId;
import { UserModel } from "../models/User";

type NotificationType = NotificationModel & mongoose.Document;

export interface NotificationModel {
  template: {
    type: String;
    required: true;
  };
  enable: {
    type: Boolean;
    enum: ["true", "false"];
    default: true;
  };
  country: {
    type: String[];
    required: true;
  };
  city: {
    type: String[];
  };
  itemHave: {
    type: Boolean;
  };
  purchase_duration: {
    type: Number;
    required: true;
  };
  alertType: {
    type: String;
    enum: ["OneTime", "scheduledWarranty", "scheduledServiceActivity"];
    required: true;
  };
  isWeekWarranty: {
    type: Boolean;
  };
  isWeekServiceActivity: {
    type: Boolean;
  };
  product_categories: {
    type: String[];
    required: true;
  };
  manufacturer: {
    type: String[];
    required: true;
  };
  warranty: {
    type: String;
  };
  expired: {
    type: Boolean;
  };
  expire_duration: {
    type: String;
  };
  activityExpiry: {
    type: Boolean;
  };
  activityExpiryDuration: {
    type: String;
  };
  searchString: {
    type: [String];
    required: true;
  };
  scheduleDay: {
    type: String;
  };
  timeZone: {
    type: String;
  };
  selectedTime: {
    type: String;
  };
  selectedTimeUTC: {
    type: String;
  };
}

const NotificationSchema = new mongoose.Schema(
  {
    template: {
      type: String,
      required: true,
    },
    enable: {
      type: Boolean,
      enum: ["true", "false"],
      default: true,
    },
    country: [
      {
        type: String,
        required: true,
      },
    ],
    city: [
      {
        type: String,
      },
    ],
    itemHave: {
      type: Boolean,
    },
    purchase_duration: {
      type: Number,
    },
    product_categories: [
      {
        type: String,
        required: true,
      },
    ],
    manufacturer: [
      {
        type: String,
        required: true,
      },
    ],
    alertType: {
      type: String,
      enum: ["OneTime", "scheduledWarranty", "scheduledServiceActivity"],
      required: true,
    },
    isWeekWarranty: {
      type: Boolean,
    },
    isWeekServiceActivity: {
      type: Boolean,
    },
    warranty: {
      type: String,
    },
    expired: {
      type: Boolean,
    },
    expire_duration: {
      type: String,
    },
    activityExpiry: {
      type: Boolean,
    },
    activityExpiryDuration: {
      type: String,
    },
    searchString: {
      type: [String],
      required: true,
    },
    scheduleDay: {
      type: String,
    },
    timeZone: {
      type: String,
    },
    selectedTime: {
      type: String,
    },
    selectedTimeUTC: {
      type: String,
    },
  },
  { timestamps: true }
);

const Notification: Model<NotificationType> = mongoose.model<NotificationType>(
  "AlertTemplate",
  NotificationSchema,
  "AlertTemplate"
);

export default Notification;
export const getFcmToken = function (id: ObjectId) {
  return id;
};

export const getUserFcmToken = function (mobileOrEmail: string) {
  return UserModel.findOne(
    { $or: [{ mobile: mobileOrEmail }, { email: mobileOrEmail }] },
    {
      fcm_token: 1,
      userDeviceId: 1,
    }
  );
};

export const getAlertTemplate = function (mobile: string) {
  return UserModel.findOne(
    {
      mobile: mobile,
    },
    {
      fcm_token: 1,
      userDeviceId: 1,
    }
  );
};
