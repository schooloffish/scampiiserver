import * as mongoose from 'mongoose';
import { Model } from 'mongoose';

type ServiceType = ServiceModel & mongoose.Document; 

export interface ServiceModel {
    deviceId:{
        type: String,
        required: true
    }
    serviceStartDate:{ 
        type: String,
        required: true
    }
    serviceEndDate: {
        type: String
    }
    status: {
        type: String,
        required: true
        enum: ['Completed','Scheduled','Cancelled']
        default: 'Scheduled'
    }
    notes: {
        type: String
    }
};

const ServiceSchema = new mongoose.Schema({
    deviceId: {
        type: String
    },
    serviceStartDate: {
        type:String,
        required: true
    },
    serviceEndDate: {
        type: String
    },
    status: {
        type: String,
        required: true,
        enum: ['Completed','Scheduled','Cancelled'],
        default: 'Scheduled'
    },
    notes: {
        type: String
    }
}, {timestamps: true});

const Service: Model<ServiceType> = mongoose.model<ServiceType>('ServiceActivity', ServiceSchema,'ServiceActivity');


export default Service;
