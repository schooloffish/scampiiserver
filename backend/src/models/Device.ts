import * as mongoose from 'mongoose';
import {Model, Types} from 'mongoose';
import { ScampiiUtils } from '../utils/ScampiiUtils';
import {ManufacturerUtils} from './Manufacturer';
import {Product} from './Product';
import ObjectId = Types.ObjectId;
import {ProductTypes, ProductTypesModel} from "./ProductTypes";
import { ScampiiSchema } from './ScampiiSchema';
import { ServiceContract, ServiceContractModel } from './ServiceContract';
import { ServiceRequest, ServiceRequestModel } from './ServiceRequest';
import {SharedDevice, SharedDeviceModel} from "./SharedDevice";
import { User, UserModel } from './User';
import { ILocation } from './UserLocation';
import {IWarranty, Warranty, WarrantyModel} from './Warrenty';

// import { ScampiiMongoose } from '../db/ScampiiMongoose';
/**
 * Created by rohit on 11/04/19.
 */

export interface IDevice {
    image?:string;
    masterProductId?: string;
    boughtFrom: string;
    categoryImage?: string;
    description: string;
    images:Array<string>;
    docImages:Array<string>;
    location: ILocation;
    name: string;
    notes?: string;
    owner: User| string | any;
    product?:Product|string;
    serviceContracts:Array<ServiceContract>;
    serviceRequests:Array<ServiceRequest>;
    sharedUser: Array<any>;
    test:any;
    countryCode: string;
    manufacturer:any;
    manuName?: any;
    purchasePrice: number;
    productModel: string;
    modelDesc: string;
    serviceType: string;
    warranties: Array<IWarranty>;
    warrantyType?: string;
    purchaseDate:Date;
    serialNo: string;
    warranty: Warranty;
    warrantyStartDate?: Date;
    totalMonths?: number;
    category: any;
}

export interface Device extends mongoose.Document, IDevice {

}

export const deviceCat = ['Appliances','Mobiles','Laptops','Accessories','Others'];
const schema = {
    boughtFrom: { type: String},
    category: [{ type: String }],
    countryCode: { type: String, required: true },
    description: {type: String, text:true},
    docImages:{type:Array},
    images: {type: Array},
    location: {type: Object},
    manu: {type: mongoose.Schema.Types.ObjectId, ref: 'Manufacturer'},
    manuName: [{ type: String, required: true }],
    manufacturer: { type: mongoose.Schema.Types.ObjectId, ref: 'Manufacturer' },
    modelDesc: { type: String, required: true },
    name: {type: String, text:true},
    notes: {type: String, text:true},
    owner: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    product: {type: mongoose.Schema.Types.ObjectId, ref: 'Product'},
    productModel: { type: String, required: true },
    purchaseDate: {type: Date},
    purchasePrice: {type: Number},
    serialNo: {type: String},
    serviceContracts: [{type: mongoose.Schema.Types.ObjectId, ref: 'serviceContract'}],
    serviceRequests: [{type: mongoose.Schema.Types.ObjectId, ref: 'ServiceRequest'}],
    serviceType: { type: String },
    warranties: [{type: mongoose.Schema.Types.ObjectId, ref: 'Warranty'}],
    warrantyType: { type: String },

};

const DeviceSchema = new ScampiiSchema(schema);

export const DeviceModel: Model<Device> = mongoose.model<Device>("Device", DeviceSchema, 'Device');

export const deleteDevice = function (id: ObjectId) {
    let exec = ServiceContractModel.deleteMany({device: id}).exec();
    ScampiiUtils.logPromiseResults(exec);
    exec = ServiceRequestModel.deleteMany({device: id}).exec();
    ScampiiUtils.logPromiseResults(exec);
    exec = WarrantyModel.deleteMany({device: id}).exec();
    ScampiiUtils.logPromiseResults(exec);
    exec = UserModel.update(
        { devices : id},
        { $pull: { devices: id } },
        { multi: true }).exec();
    ScampiiUtils.logPromiseResults(exec);
    exec = DeviceModel.deleteOne({'_id': id}).exec();
    ScampiiUtils.logPromiseResults(exec);
    exec = SharedDeviceModel.deleteMany({device: new ObjectId(id)}).exec();
    ScampiiUtils.logPromiseResults(exec);
    return exec;
};
 
//   export const deleteDeviceImage = function (id: ObjectId, value:any) {
//     const images: Array<string> = value.images;
//     const url ="http://13.127.39.213/devclient/scampii/images/";
//     var docImageString=images.toString();
//     var docName=docImageString.split(",");
//       let docNamearray = docName.map(i => url + i);
//     var deleteDocImage=DeviceModel.updateMany(
//             { _id: id },
//             { $pullAll: { docImages: docNamearray } }
//           );
//        return  deleteDocImage;
//   };

export const processDevice = async function populateDevice(device : Device): Promise<IDevice> {
    if(!device) {return Promise.resolve(null);}
    const warranties: Array<IWarranty> = device.warranties;
    device.warranties = new Array();
    if(warranties) {
        for (let war of warranties) {
            war = await WarrantyModel.findOne({_id: war});
            if (war) {
                const endDate: Date = war.endDate;
                const curDate: Date = new Date();
                if (endDate) {
                    const days = endDate.getTime() - curDate.getTime();
                    // todo :-  complete calcullations
                    war.daysLeft = Math.trunc(days / (3600 * 24 * 1000));
                }
                if (war.warrantyStartDate) {
                    // const daysPassed = Math.trunc(curDate.getTime() - war.startDate.getTime() / (3600 * 24 * 1000));
                    war.daysLeft = 10;
                }
                device.warranties.push(war);
            }
        }
    }

    const dev: IDevice  = device.toJSON();

    const tempWars = new Array();
    const expWars = new Array();

    const currentDate = new Date();

    for (const warr of dev.warranties) {
        const startDate = new Date(warr.warrantyStartDate.getTime());
        // tslint:disable-next-line:radix
        const lastDate = new Date(startDate.setMonth(startDate.getMonth()+parseInt(String(warr.warrantyTypeDuration))));
        if(lastDate.getTime() > currentDate.getTime()) {
            tempWars.push(warr);
        } else {
            warr.expired = true;
            expWars.push(warr);
        }
    }

    dev.warranties = tempWars;
    dev.warranties.sort((war1,war2) => {
        if(!war1.warrantyStartDate || !war2.warrantyStartDate)  {return 0;}
        if(war1.warrantyStartDate.getTime() === war2.warrantyStartDate.getTime()) {
            if(!war1.warrantyTypeDuration ||  !war2.warrantyTypeDuration) {return 0;}
            return war2.warrantyTypeDuration - war1.warrantyTypeDuration;
        } else  {
            return war1.warrantyStartDate.getTime() - war2.warrantyStartDate.getTime();
        }
    });
    if(dev.warranties && dev.warranties[0]) {
        dev.warranties[0].defaultWarranty = true;
    }

    for (const expWar of expWars) {
        dev.warranties.push(expWar);
    }

    const owner: any = device.owner;
    if(owner) {
    const userDev = await UserModel.findOne({_id: owner._id});
    // dev.owner = {name: userDev.name, mobile: userDev.mobile, image: userDev.image};
    dev.owner = {
         name: userDev.name,
         mobile: userDev.mobile,
         image: userDev.image,
         email: userDev.email
        };
                }

    if(dev.location) {
        delete dev.location.devices;
        delete dev.location.users;
    }

    if(device.manufacturer) {
        const manu = await ManufacturerUtils.getManufacturerForResponse({_id: new ObjectId(device.manufacturer.toString())});
        dev.manufacturer = manu;
    }


    const cat: ProductTypes = await ProductTypesModel.findOne({type: device.category});
    if (cat) {
        dev.categoryImage = cat.image;
    }

    const sharedDevs: Array<SharedDevice> = await SharedDeviceModel.find({device: new ObjectId(device._id)});
    dev.sharedUser = new Array<any>();
    for (const sharedDev of sharedDevs) {
        const user1 = await UserModel.findOne({_id: sharedDev.sharedWith});
    if(user1) {
        // dev.sharedUser.push({name: user1.name, mobile: user1.mobile, image: user1.image});
        dev.sharedUser.push({
            name: user1.name,
            mobile: user1.mobile,
            image: user1.image,
            email: user1.email
        });
    }
    }
    return Promise.resolve(dev);
};
