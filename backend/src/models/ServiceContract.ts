import * as mongoose from 'mongoose';
import { Types } from 'mongoose';
import { Device } from './Device';
import { Manufacturer } from './Manufacturer';
import { Product } from './Product';
import { ScampiiSchema } from './ScampiiSchema';
import ObjectId = Types.ObjectId;

/**
 * Created by rohit on 23/04/19.
 */

export interface ServiceContract extends mongoose.Document {
    _id: ObjectId;
    device: Device;
    name: string;
    product: Product;
    manufacturer: Manufacturer;
}

const ServiceContractSchema = new ScampiiSchema({
    device: { type: mongoose.Schema.Types.ObjectId, ref: 'Device' },
    manufacturer: { type: mongoose.Schema.Types.ObjectId, ref: 'Manufacturer' },
    name: { type: String, required: true },
    product: { type: mongoose.Schema.Types.ObjectId, ref: 'Product' },
});

export const ServiceContractModel = mongoose.model<ServiceContract>("ServiceContract", ServiceContractSchema, 'ServiceContract');


