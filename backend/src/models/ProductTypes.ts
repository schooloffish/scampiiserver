import * as mongoose from 'mongoose';
import { Types } from 'mongoose';
import ObjectId = Types.ObjectId;
import { ScampiiSchema } from './ScampiiSchema';


/**
 * Created by rohit on 23/04/19.
 */

export interface ProductTypes extends mongoose.Document {
    _id: ObjectId;
    type: string;
    image: string;
}

const ProductTypesSchema = new ScampiiSchema({
    image: { type: String, unique: false },
    type: { type: String, required: true, unique: true }
});

export const ProductTypesModel = mongoose.model<ProductTypes>("ProductTypes", ProductTypesSchema, 'ProductTypes');


