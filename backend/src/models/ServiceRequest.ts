import * as mongoose from 'mongoose';
import { Types } from 'mongoose';
import { Device } from './Device';
import { ScampiiSchema } from './ScampiiSchema';
import ObjectId = Types.ObjectId;

/**
 * Created by rohit on 23/04/19.
 */

export interface ServiceRequest extends mongoose.Document {
    _id: ObjectId;
    name: string;
    device:Device;
}

const ServiceRequestSchema = new ScampiiSchema({
    name: { type: String, required: true },
    serviceContracts: [{ type: mongoose.Schema.Types.ObjectId, ref: 'ServiceContract' }],
});

export const ServiceRequestModel = mongoose.model<ServiceRequest>("ServiceRequest", ServiceRequestSchema, 'ServiceRequest');



