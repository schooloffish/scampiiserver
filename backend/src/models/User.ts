import * as mongoose from 'mongoose';
import { Device } from './Device';
import { ScampiiSchema } from './ScampiiSchema';
import { UserLocation } from './UserLocation';
/**
 * Created  on 24/03/19.
 */



export interface User extends mongoose.Document {
    _id: mongoose.Types.ObjectId;
    email: string;
    fcm_token: string;
    city: string;
    image:string;
    name: string;
    age: number;
    products: any;
    // Check to see if JWT token has to be used. That can be integrated later
    authToken: Array<string>;
    mobileOTP: string;
    emailOTP: string;
    mobile: string;
    devices: Array<Device>;
    sharedDevices: Array<Device>;
    locations: Array<UserLocation>;
    userDeviceId?: string;
    isAdmin: boolean;
    updatedAt:Date;
    otpRequestTime:Number;
    otpCount:Number;
    countryName:string;
}

const UserScheme = new ScampiiSchema({
    age: {type: Number},
    authToken: {type:Array},
    city: {type:String},
    fcm_token: {type:String},
    devices: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Device' }],
    // unique is not working for nulls will have to check the emails seperately.
    email:{type:String},
    emailOTP: {type:String},
    image:{type:String},
    isAdmin:{type:Boolean},
    locations: [{ type: mongoose.Schema.Types.ObjectId, ref: 'UserLocation' }],
    mobile: {type:String},
    mobileOTP: {type:String},
    name: {type: String},
    products: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Product' }],
    sharedDevices: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Device' }],
    userDeviceId: {type: String},
    updatedAt:{type: Date},
    otpCount:{type:Number, default: 1},
    otpRequestTime:{type:Number},
    countryName:{type:String}
});

  
export const UserModel: mongoose.Model<User> = mongoose.model('User',UserScheme,'User');
mongoose.set('useFindAndModify', false);
// export async function testCreate() {
//     try {
//         await ScampiiMongoose.initDB();
//         try {
//             await UserModel.collection.drop();
//             await ProductModel.collection.drop();
//         }catch (error) {
//             console.log('Ignored');
//         }
//         const mgUserModel = new UserModel({
//             age:89,
//             email:'test@test.test',
//             name:'Test User 123'
//         });
//         const savedUser = await mgUserModel.save();
//         let  prodModel = new ProductModel({
//             _id: new mongoose.Types.ObjectId(),
//             name:'Test Product',
//         });
//         let savedProd = await prodModel.save();
//         if(!savedUser.products) {
//             savedUser.products = [];
//         }
//         savedUser.products.push(savedProd._id);
//
//         prodModel = new ProductModel({
//             _id: new mongoose.Types.ObjectId(),
//             name:'Test Product 2',
//         });
//         savedProd = await prodModel.save();
//         if(!savedUser.products) {
//             savedUser.products = [];
//         }
//         savedUser.products.push(savedProd._id);
//
//         UserModel.update({name:savedUser.name},savedUser,(err, raw)=>{
//             if(err) {console.log(err); }
//             // if(raw)console.log(raw);
//         });
//         const foundUser = await UserModel.findOne({name:savedUser.name}).populate('products');
//         const prodFound = await ProductModel.findOne({name:savedProd.name}).populate('user');
//         console.log(foundUser);
//         console.log(prodFound);
//         new UserOps('email','test@test.test').updateAuthToken('testing')
//             .then((out)=>console.log(out))
//             .catch((err) => console.log(err));
//         console.log('Update started');
//     }catch (error) {
//         console.log(error);
//     }
// }
//
//
// export async function checkUser() {
//     await ScampiiMongoose.initDB();
//     try {
//         console.log('checked user');
//         const ids = ['5d8db25d5cc8691e35100b21'];
//         const newVar = await UserModel.find().where('_id').in(ids).exec();
//         console.log(newVar);
//         ScampiiMongoose.closeCon();
//     }catch (error) {
//         console.log(error);
//     }
// }

// checkUser();
