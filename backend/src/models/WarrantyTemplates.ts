import * as mongoose from 'mongoose';
import { Types } from 'mongoose';
import { LenientScampiiSchema } from './ScampiiSchema';
import ObjectId = Types.ObjectId;
import { IWarrantyDuration, WarrantyCover } from './Warrenty';

/**
 * Created by rohit on 10/09/19.
 */

export interface WarrantyTemplates extends mongoose.Document {
    _id: ObjectId;
    hash: string;
    name: string;
    durations: Array<IWarrantyDuration>;
    covers: Array<WarrantyCover>;
}

const WarrantyTemplatesSchema = new LenientScampiiSchema({
    covers: { type: Array<WarrantyCover>(), required: true },
    durations: { type: Array<IWarrantyDuration>(), required: true },
    hash: { type: String, required: true, unique: true },
    name: { type: String, required: true, unique: true },
});

export const WarrantyTemplatesModel = mongoose.model<WarrantyTemplates>("WarrantyTemplates", WarrantyTemplatesSchema, 'WarrantyTemplates');
