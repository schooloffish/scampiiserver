export class ProductExcelReader {

    public static readonly type = "__EMPTY";
    public static readonly countryCode = "__EMPTY_1";
    public static readonly manuName = "__EMPTY_2";
    public static readonly productModel = "__EMPTY_3";
    public static readonly modelDesc = "__EMPTY_4";
    public static readonly warrantyTemplate = "__EMPTY_5";
    public static readonly warrantyType = "__EMPTY_6";
    public static readonly factoryDuration = "__EMPTY_7";
    public static readonly productId = "productId";
}