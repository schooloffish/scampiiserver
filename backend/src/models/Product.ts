import ObjectId = Types.ObjectId;
import * as Joi from 'joi';
import * as mongoose from 'mongoose';
import { Types } from 'mongoose';
import { Manufacturer } from './Manufacturer';
import { ScampiiSchema } from './ScampiiSchema';
import { IWarranty } from './Warrenty';
/**
 * Created  on 24/03/19.
 */


export interface Product extends mongoose.Document {
    _id: ObjectId;
    countryCode: string;
    manufacturer:Manufacturer|string;
    manuName?: string;
    metaphone?: string,
    productModel: string;
    modelDesc: string;
    name?: string;
    type: any;
    typeImage: string;
    image: string;
    serviceType: string;
    soundex?: string;
    warranty: IWarranty;
    documents: Array<IProdDocument>;
}

export interface IProdDocument {
    documentLabel: string;
    documentURL: string;
}

const MgProductSchema = new ScampiiSchema({
    countryCode: { type: String, required: true },
    documents: {type: Array},
    image: { type: String },
    manuName: { type: String, required: true },
    manufacturer: { type: mongoose.Schema.Types.ObjectId, ref: 'Manufacturer' },
    metaphone: { type: String, required: true },
    modelDesc: { type: String, required: true },
    name: { type: String},
    productModel: { type: String, required: true },
    serviceType: { type: String },
    soundex: { type: String },
    type: { type: String, required: true },
    typeImage: { type: String},
    warranty: {type: Object},
},{strict: false,
    timestamps:true});

export const ProductModel = mongoose.model<Product>("Product", MgProductSchema, 'Product');

export const ProductValidationSchema = Joi.object().keys({
    _id:Joi.object().optional(),
    createdAt:Joi.object().optional(),
    countryCode:Joi.string().required(),
    documents:Joi.array().optional(),
    image:Joi.string().optional(),
    manuName:Joi.string().optional(),
    manufacturer: Joi.object().required(),
    metaphone: Joi.string().optional(),
    modelDesc: Joi.string().required(),
    name: Joi.string().optional(),
    productModel: Joi.string().required(),
    serviceType:Joi.string().optional(),
    soundex:Joi.string().optional(),
    type: Joi.string().required(),
    typeImage: Joi.string().optional(),
    updatedAt: Joi.object().optional(),
    warranty:Joi.object().optional(),

});

export const validate = function (product: Product): Joi.ValidationResult<Product> {
    const options: Joi.ValidationOptions = {};
    options.abortEarly = false;
    return ProductValidationSchema.validate(product,options);
};


// export async function testCreate() {
//     try {
//         await ScampiiMongoose.initDB();
//         const mgUserModel = new ProductModel({
//         countryCode: 'IN',
//         manuName: 'test',
//         productModel: 'testModel',
//         modelDesc: 'testDesc',
//         name: 'testProd',
//         type: 'TV',
//         image: 'http',
//         serviceType: 'All',
//             warranty : {
//                 covers: [
//                     {label: 'Repair', value: 'No'}
//                 ],
//                 durations: [
//                     {label: 'Motor', duration: 24}
//                 ]
//             }
//         });
//         const savedUser = await mgUserModel.save();
//         console.log(JSON.stringify(savedUser));
//
//     }catch (error) {
//         console.log(error);
//     }
// }
//
// testCreate();
