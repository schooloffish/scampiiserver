import * as mongoose from 'mongoose';
import { Types } from 'mongoose';
import ObjectId = Types.ObjectId;
import { ScampiiSchema } from './ScampiiSchema';


/**
 * Created by rohit on 23/04/19.
 */


export interface Config extends mongoose.Document {
    _id: ObjectId;
    key: string;
    value: string;
}

const ConfigSchema = new ScampiiSchema({
    key: { type: String, required: true, unique: true },
    value: { type: String, required: true}
});

export const ConfigModel = mongoose.model<Config>("Config", ConfigSchema, 'Config');


