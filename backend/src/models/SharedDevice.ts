import * as mongoose from 'mongoose';
import { Types } from 'mongoose';
import ObjectId = Types.ObjectId;
import { ScampiiSchema } from './ScampiiSchema';
import {User} from "./User";
import {IDevice} from "./Device";


/**
 * Created by rohit on 23/04/19.
 */


export interface SharedDevice extends mongoose.Document {
    _id: ObjectId;
    device: IDevice;
    owner: User;
    sharedWith: User;
    status: 'Accepted'|'Rejected'|'Shared';
}

const SharedDeviceSchema = new ScampiiSchema({
    device: {type: mongoose.Schema.Types.ObjectId, ref: 'Device'},
    owner: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    sharedWith: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    status: { type: String, required: true}
});

export const SharedDeviceModel = mongoose.model<SharedDevice>("SharedDevice", SharedDeviceSchema, 'SharedDevice');


