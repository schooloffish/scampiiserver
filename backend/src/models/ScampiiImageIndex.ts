import { unlinkSync, writeFileSync } from 'fs';
import * as Joi from 'joi';
import * as mongoose from 'mongoose';
import { Types } from 'mongoose';
import * as PromiseFtp from 'promise-ftp';
import { BadRequestError } from 'typescript-rest/dist/server/model/errors';
import { ScampiiMongoose } from '../db/ScampiiMongoose';
import {ScampiiConstants} from "../utils/ScampiiConstants";
import { ScampiiSchema } from './ScampiiSchema';
import ObjectId = Types.ObjectId;
import { v4 as uuidv4 } from 'uuid';


/**
 * Created by rohit on 23/04/19.
 */

export interface ScampiiImage extends mongoose.Document {
    _id: ObjectId;
    name: string;
    cat: string;
    url: string;
}

export const ImageSchema = new ScampiiSchema({
    cat: { type: String, enum: ['user', 'device', 'manufacturer'], required: true },
    name: { type: String,  required: true,  },
    url:{type:String,required:true}
});

// ImageSchema.pre('insertMany', (next,docs) => {
//     console.log('checking for URL1' + docs[0].url);
//     ImageModel.findOne({url: docs[0].url}).then((doc) => {
//         console.log(doc);
//         if (doc) {
//             docs[0]._id = doc._id;
//         }
//         next();
//     });
// });
//

export const ImageValidSchema = Joi.object().keys({
    cat: Joi.string().required().valid('user', 'device', 'manufacturer'),
    data: Joi.string().required(),
    name: Joi.string().required()
});

export const ImageModel = mongoose.model<ScampiiImage>("ScampiiImage", ImageSchema, 'ScampiiImage');


export const validate = function (image: any): Joi.ValidationResult<ScampiiImage> {
    const options: Joi.ValidationOptions = {};
    options.abortEarly = false;
    return ImageValidSchema.validate(image,options);
};


export class ScampiiImageOps {


    public static async saveImage(image: any) : Promise<string>{
        ScampiiMongoose.initDB();
        this.validateInput(image);
        const imageName = this.getNewImageName(image.name);
        const fileURL = await ScampiiConstants.getImagesCDNDir() + imageName;
        image.url = fileURL;
        const img = await ImageModel.findOne({url:fileURL});
        if(!img) {
            await ImageModel.create(image);
        } else {
            await ImageModel.update({url:fileURL},image);
        }
        await this.uploadFile(image.data, imageName);
        return Promise.resolve(fileURL);
    }

    public static async saveUserImage(image: any) : Promise<string>{
        ScampiiMongoose.initDB(); 
        this.validateInput(image);
        const imageName = this.getNewImageName(image.name);
        const userFileURL = await ScampiiConstants.getImagesCDNDir() + imageName;
        image.url = userFileURL;
        const img = await ImageModel.findOne({url:userFileURL});
        if(!img) {
            await ImageModel.create(image);
        } else {
            await ImageModel.update({url:userFileURL},image);
        }
        await this.uploadFile(image.data, imageName);
        const file_Url=userFileURL;
        return file_Url;
    }

    private static getNewImageName(imageName: string) {
        const strings = imageName.split('.');
        if(strings.length >1 ) {
            return uuidv4() + '.' + strings[strings.length-1];
        }
        console.log('Not able to find a extension for the imag, hence returning jpeg as default ');
        return uuidv4() + '.jpeg';
    }

    private static async uploadFile(fileData: string, fileName: string){
        const buf: Buffer = Buffer.from(fileData, 'base64');
        writeFileSync(fileName, buf);
        await this.uploadFileToFTPServer(fileName);
        unlinkSync(fileName);
        console.log('file deleted successfully.');
    }

    private static validateInput(image: any) {
        const result: Joi.ValidationResult<ScampiiImage> = validate(image);
        if (result && result.error) {
            throw new BadRequestError(JSON.stringify(result.error));
        }
    }

    private static async uploadFileToFTPServer(fileName: string) {
        const ftp = new PromiseFtp();
        const ftpHost = await ScampiiConstants.getFTPHost();
        const port = await ScampiiConstants.getFTPPort();
        const userName = await ScampiiConstants.getFTPUsername();
        const password = await ScampiiConstants.getFTPPassword();
        const options = {host: ftpHost, port: port, user: userName, password: password};
        console.log(options);
        console.log(await ftp.connect(options));
        console.log(await ftp.cwd(await ScampiiConstants.getImagesDir()));
        await ftp.put(fileName,fileName);
        console.log('file copied successfully');
        console.log(await ftp.site('chmod 777 ' + fileName));
        console.log(await ftp.end());
        console.log(await ftp.destroy());
    }

}
