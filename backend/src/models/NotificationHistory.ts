import * as mongoose from "mongoose";
import { Model } from "mongoose";
import { ScampiiSchema } from "./ScampiiSchema";

// type NotificationHistoryType = NotificationHistoryModel & mongoose.Document;

export interface NotificationHistoryType extends mongoose.Document {
  mobile: {
    type: String;
  };
  email: {
    type: String;
  };
  user_Id: {
    type: String;
  };
  city: {
    type: any;
  };
  countryName: {
    type: String;
  };
  userDeviceId: {
    type: String;
  };
  messageBody: {
    type: String;
  };
  sendAt: {
    type: String;
  };
}

const NotificationHistorySchema = new ScampiiSchema(
  {
    mobile: {
      type: String,
    },
    email: {
      type: String,
    },
    user_Id: {
      type: String,
    },
    city: {
      type: String,
    },
    countryName: {
      type: String,
    },
    userDeviceId: {
      type: String,
    },
    messageBody: {
      title: {
        type: String,
      },
      body: {
        type: String,
      },
    },
    sendAt: {
      type: String,
    },
  },
  { timestamps: true }
);

export const NotificationHistoryModel: Model<NotificationHistoryType> =
  mongoose.model<NotificationHistoryType>(
    "NotificationHistory",
    NotificationHistorySchema,
    "NotificationHistory"
  );
