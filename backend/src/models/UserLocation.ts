import * as mongoose from 'mongoose';
import { Device } from './Device';
import { ScampiiSchema } from './ScampiiSchema';
import { User } from './User';
/**
 * Created by rohit on 19/06/19.
 */

export interface ILocation {
    lat?: number;
    lng?: number;
    address: string;
    addressLine1: string;
    addressLocality: string;
    addressCity: string;
    countryCode: string;
    addressPincode: string;
    locationTag: string;
    name: string;
    devices?: Array<any>;
    users?: Array<any>;
}

export interface UserLocation  extends mongoose.Document, ILocation {
    devices: Array<Device>;
    users: Array<User>;
    deviceCount?: number;
}

const UserLocationScheme = new ScampiiSchema({
    address: {type: String},
    addressCity: {type: String},
    addressLine1: {type: String},
    addressLocality: {type: String},
    addressPincode: {type: String},
    countryCode: {type: String},
    devices: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Device' }],
    lat: {type:Number},
    lng: {type:Number},
    locationTag: {type: String},
    name: {type: String, required: true},
    users: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
});

export const UserLocationModel: mongoose.Model<UserLocation> = mongoose.model('UserLocation',UserLocationScheme,'UserLocation');

