/**
 * Created on 25/03/19.
 */

export interface AuthDetails {
    mobileOTP: string; 
    mailOTP?: string;
    authToken?: string;
}
