import * as FileWrite from "fs";
import * as XLS from "node-xlsx";

export class ExcelUtils {

    public static  writeDataToFile(data: Array<Array<String>>): string {
        const buffer = XLS.build([{name: 'result', data: data}]);
        const path = 'test'+ new Date().getTime()+'.xlsx';
        FileWrite.writeFileSync(path,buffer);
        return path;
    }
}