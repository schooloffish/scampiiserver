import * as moment from "moment";

import { NotificationModel } from "../models/Notification";
import { DeviceModel } from "../models/Device";
import { SharedDeviceModel } from "../models/SharedDevice";
import { WarrantyModel } from "../models/Warrenty";
import ServiceActivity from "../models/ServiceActivity";
import { UserModel } from "../models/User";
import { Types } from "mongoose";
import ObjectId = Types.ObjectId;

export class NotificationTargetAudienceService {
  public static eligibleUsersCount: any = 0;
  public static targetAudience: Array<any> = [];

  public static async filterAlertTargetAudience(
    alertTemplate: NotificationModel
  ): Promise<any> {
    const {
      itemHave,
      product_categories,
      manufacturer,
      searchString,
      purchase_duration,
      expire_duration,
      alertType,
      expired,
      activityExpiry,
      activityExpiryDuration,
      city,
      country,
    } = alertTemplate;

    const eligibleItems: Array<any> = await this.searchEligibleItems(
      searchString,
      purchase_duration,
      product_categories,
      manufacturer
    );
    // console.log(`Get eligibleItems (${eligibleItems.length}):`, eligibleItems);

    let notificationItemsAndAudience: Array<any> = [];

    switch (alertType.toString()) {
      case "OneTime":
        notificationItemsAndAudience =
          await this.filterOneTimeAlertItemsAndAudience(
            eligibleItems,
            city,
            country,
            itemHave
          );
        break;
      case "scheduledWarranty":
        notificationItemsAndAudience =
          await this.filterWarrantyAlertItemsAndAudience(
            eligibleItems,
            city,
            country,
            expire_duration,
            expired
          );
        break;
      case "scheduledServiceActivity":
        notificationItemsAndAudience =
          await this.filterServiceActivityAlertItemsAndAudience(
            eligibleItems,
            city,
            country,
            activityExpiryDuration,
            activityExpiry
          );
        break;
      default:
        break;
    }

    return Promise.resolve(notificationItemsAndAudience);
  }

  private static async searchEligibleItems(
    arrSerchStrings: any,
    numItemsPurchasedBeforeMonths: any,
    product_categories: any,
    manufacturer: any
  ) {
    const itemPurchasedMonth = moment().subtract(
      numItemsPurchasedBeforeMonths,
      "months"
    );

    // prepare regex string to filter devices whose name contains one or more of the words in the description query
    //regex format => (?=.*\bAppliances\b)|(?=.*\bVideocon\b)

    let regExItemsNameSearch = arrSerchStrings.length
      ? "(?=.*\\b" + arrSerchStrings[0] + "\\b)"
      : "";

    for (let i = 1; i < arrSerchStrings.length; i++) {
      regExItemsNameSearch += "|(?=.*\\b" + arrSerchStrings[i] + "\\b)";
    }
    // console.log("regExItemsNameSearch: " + regExItemsNameSearch);

    let queryEligibleDevices: any = {
      $and: [
        { category: { $in: product_categories } },
        { manuName: { $in: manufacturer } },
      ],
    };

    if (regExItemsNameSearch.length > 0) {
      queryEligibleDevices[`$and`].push({
        name: { $regex: regExItemsNameSearch, $options: "i" },
      });
    }

    if (numItemsPurchasedBeforeMonths != null) {
      queryEligibleDevices[`$and`].push({
        purchaseDate: { $lt: itemPurchasedMonth },
      });
    }
    // console.log("queryEligibleDevices: ", JSON.stringify(queryEligibleDevices));

    return DeviceModel.find(
      queryEligibleDevices,
      "name warranties purchaseDate owner manuName category serialNo"
    );
  }

  // Find shared and owners of filtered devices.
  private static async filterEligibleUsersForItem(
    itemDetails: any,
    alertTemplateCity: any,
    alertTemplateCountry: any
  ) {
    const { _id, owner } = itemDetails;

    const sharedDeviceCondition: any = {
      $and: [{ device: { $eq: _id } }, { status: { $eq: "Accepted" } }],
    };
    const sharedOwners: any = await SharedDeviceModel.find(
      sharedDeviceCondition
    );

    // ownerAndSharedOwnerArr will store all owner and shared owners of current device
    let ownerAndSharedOwnerArr: Array<any> = [];
    for (let sharedOwnerIndex in sharedOwners) {
      ownerAndSharedOwnerArr.push(
        sharedOwners[sharedOwnerIndex].sharedWith.toString()
      );
    }
    ownerAndSharedOwnerArr.push(owner.toString());

    let eligibleUsersCondition: any = {
      $and: [
        { countryName: { $in: alertTemplateCountry } },
        { _id: { $in: ownerAndSharedOwnerArr } },
      ],
    };

    if (alertTemplateCity.length > 0) {
      eligibleUsersCondition[`$and`].push({
        city: { $in: alertTemplateCity },
      });
    }
    // console.log(
    //   "eligibleUsersCondition:",
    //   JSON.stringify(eligibleUsersCondition)
    // );

    // target audience for item
    return UserModel.find(
      eligibleUsersCondition,
      "name mobile email countryName city fcm_token"
    );
  }

  // Returns filterd Items with Notifiaction Reciepients for OneTime Alert Template
  private static async filterOneTimeAlertItemsAndAudience(
    eligibleItems: Array<any>,
    alertTemplateCity: any,
    alertTemplateCountry: any,
    shouldUserHaveItem: any
  ) {
    const arrFilteredOneTimeAlertItems: Array<any> = [];
    const arrCheckedInUserIds: Array<ObjectId> = [];
    const arrReciepientIdsForUserHaveItems: Array<ObjectId> = [];

    for (let index in eligibleItems) {
      const itemDetails = eligibleItems[index];
      // console.log("itemDetails:", itemDetails);

      const { name, _id, serialNo, purchaseDate, manuName, category } =
        itemDetails;

      const eligibleUsers: Array<any> = await this.filterEligibleUsersForItem(
        itemDetails,
        alertTemplateCity,
        alertTemplateCountry
      );
      // console.log(_id, "eligibleUsers:", eligibleUsers);

      // if the current device owner or shared owner belongs filtered country and city then add them to targetAudiece
      if (eligibleUsers.length > 0) {
        if (shouldUserHaveItem) {
          const currentDevice: Object = {
            deviceName: name,
            deviceId: _id,
            deviceSerial: serialNo,
            purchaseDate: purchaseDate,
            manufacturer: manuName,
            category: category,
            receipient: eligibleUsers,
          };
          // console.log(_id, "currentDevice:", currentDevice);

          arrFilteredOneTimeAlertItems.push(currentDevice);
        } else {
          eligibleUsers.forEach((userData) => {
            const { _id } = userData;
            // console.log(_id, "userData:", userData);
            const isUserCheckedIn = arrCheckedInUserIds.some((userId) =>
              userId.equals(_id)
            );
            // console.log(_id, "isUserCheckedIn:", isUserCheckedIn);
            if (!isUserCheckedIn) {
              arrCheckedInUserIds.push(_id);
              arrReciepientIdsForUserHaveItems.push(_id.toString());
            }
          });
        }
      }
    }

    let arrReciepientListForUserDontHaveItems: Array<any> = [];

    if (!shouldUserHaveItem) {
      // console.log(
      //   "arrReciepientIdsForUserHaveItems:",
      //   arrReciepientIdsForUserHaveItems
      // );

      let eligibleUsersCondition: any = {
        $and: [
          { countryName: { $in: alertTemplateCountry } },
          { _id: { $nin: arrReciepientIdsForUserHaveItems } },
        ],
      };

      if (alertTemplateCity.length > 0) {
        eligibleUsersCondition[`$and`].push({
          city: { $in: alertTemplateCity },
        });
      }
      arrReciepientListForUserDontHaveItems = await UserModel.find(
        eligibleUsersCondition,
        "name mobile email countryName city fcm_token"
      );
    }

    const oneTimeAlertItemsAndAudience = shouldUserHaveItem
      ? arrFilteredOneTimeAlertItems
      : [{ receipient: arrReciepientListForUserDontHaveItems }];

    // console.log("oneTimeAlertItemsAndAudience:", oneTimeAlertItemsAndAudience);

    return oneTimeAlertItemsAndAudience;
  }

  private static async filterWarrantyAlertItemsAndAudience(
    eligibleItems: Array<any>,
    alertTemplateCity: any,
    alertTemplateCountry: any,
    warrantyExpiryDuration: any,
    isWarrantyExpired: any
  ) {
    const warrantyAlertItemsAndAudience: Array<any> = [];

    for (const itemDetails of eligibleItems) {
      // console.log("itemDetails:", itemDetails);
      const {
        name,
        _id,
        serialNo,
        purchaseDate,
        manuName,
        category,
        warranties,
      } = itemDetails;

      const eligibleUsers: Array<any> = await this.filterEligibleUsersForItem(
        itemDetails,
        alertTemplateCity,
        alertTemplateCountry
      );
      // console.log(_id, "eligibleUsers:", eligibleUsers);

      if (eligibleUsers.length > 0) {
        // console.log(_id, "warranties:", warranties);

        // fetch all warranties whose id are in itemDetails warranties array
        const itemWarranties: Array<any> = await WarrantyModel.find(
          { _id: { $in: warranties } },
          "name warrantyType warrantyStartDate warrantyTypeDuration"
        );
        // console.log("itemWarranties: ", itemWarranties);

        // console.log("warrantyExpiryDuration:", warrantyExpiryDuration);
        const expiryThresholdDate = moment()
          .add(Number(warrantyExpiryDuration), "day")
          .format("MM-DD-YYYY");

        // console.log(_id, "expiryThresholdDate:", expiryThresholdDate);

        if (itemWarranties.length > 0) {
          const arrFilteredWarranties: Array<any> = [];
          for (const warrantyDetails of itemWarranties) {
            const { warrantyTypeDuration, warrantyStartDate } = warrantyDetails;

            const warrantyEndDate = moment(warrantyStartDate)
              .add(Number(warrantyTypeDuration), "months")
              .subtract(1, "day");

            const formatedWarrantyEndDate =
              moment(warrantyEndDate).format("MM-DD-YYYY");

            // console.log(
            //   _id,
            //   "warrantyEndDate:",
            //   warrantyEndDate,
            //   formatedWarrantyEndDate
            // );

            const isWarrantyEndDateEqualsExpiryThresholdDate =
              formatedWarrantyEndDate == expiryThresholdDate;

            // console.log(
            //   "isWarrantyEndDateEqualsExpiryThresholdDate: ",
            //   isWarrantyEndDateEqualsExpiryThresholdDate
            // );

            const isWarrantyExpiredToday =
              formatedWarrantyEndDate == moment().format("MM-DD-YYYY");

            // console.log(
            //   "Todays date:",
            //   moment(),
            //   moment().format("MM-DD-YYYY")
            // );
            // console.log("isWarrantyExpiredToday: ", isWarrantyExpiredToday);

            // check if alreadyExpired condition is expired today
            if (isWarrantyExpired && isWarrantyExpiredToday) {
              arrFilteredWarranties.push(warrantyDetails);
            } // check if already expired is false and warranty is going to be expired on the expiryThresholdDate
            else if (
              !isWarrantyExpired &&
              isWarrantyEndDateEqualsExpiryThresholdDate
            ) {
              arrFilteredWarranties.push(warrantyDetails);
            }
          }

          if (arrFilteredWarranties.length > 0) {
            const currentDevice: Object = {
              deviceName: name,
              deviceId: _id,
              deviceSerial: serialNo,
              purchaseDate: purchaseDate,
              manufacturer: manuName,
              category: category,
              receipient: eligibleUsers,
              warranties: arrFilteredWarranties,
            };
            // console.log(_id, "currentDevice:", currentDevice);

            warrantyAlertItemsAndAudience.push(currentDevice);
          }
        }
      }
    }

    return warrantyAlertItemsAndAudience;
  }

  private static async filterServiceActivityAlertItemsAndAudience(
    eligibleItems: Array<any>,
    alertTemplateCity: any,
    alertTemplateCountry: any,
    activityExpiryDuration: any,
    activityExpiry: any
  ) {
    const serviceActivityAlertItemsAndAudience: Array<any> = [];

    for (const itemDetails of eligibleItems) {
      const { name, _id, serialNo, purchaseDate, manuName, category } =
        itemDetails;

      // console.log("itemDetails:", itemDetails);

      const eligibleUsers: Array<any> = await this.filterEligibleUsersForItem(
        itemDetails,
        alertTemplateCity,
        alertTemplateCountry
      );

      // console.log(_id, "eligibleUsers:", eligibleUsers);

      if (eligibleUsers.length > 0) {
        const serviceActivityCondition = {
          $and: [
            { deviceId: { $eq: _id } },
            { status: { $eq: "Scheduled" } },
            {
              serviceStartDate: activityExpiry
                ? {
                    $gte: moment().format("YYYY-MM-DD"),
                    $lt: moment().add(1, "day").format("YYYY-MM-DD"),
                  }
                : {
                    $gte: moment()
                      .add(Number(activityExpiryDuration), "day")
                      .format("YYYY-MM-DD"),
                    $lt: moment()
                      .add(Number(activityExpiryDuration) + 1, "day")
                      .format("YYYY-MM-DD"),
                  },
            },
          ],
        };
        // console.log(
        //   _id,
        //   "serviceActivityCondition:",
        //   JSON.stringify(serviceActivityCondition)
        // );

        // store serviceActivities of current device
        const itemServiceActivities: Array<any> = await ServiceActivity.find(
          serviceActivityCondition
        );
        // console.log("itemServiceActivities: ", itemServiceActivities);

        if (itemServiceActivities.length > 0) {
          const currentDevice: Object = {
            deviceName: name,
            deviceId: _id,
            deviceSerial: serialNo,
            purchaseDate: purchaseDate,
            manufacturer: manuName,
            category: category,
            receipient: eligibleUsers,
            serviceActivities: itemServiceActivities,
          };
          // console.log(_id, "currentDevice:", currentDevice);

          serviceActivityAlertItemsAndAudience.push(currentDevice);
        }
      }
    }
    return serviceActivityAlertItemsAndAudience;
  }
}
