import {Config, ConfigModel} from "../models/Config";

export class ScampiiConstants {

    public static readonly appDownloadLink = 'appDownloadLink';
    public static readonly feedbackURL = 'feedbackURL';
    public static readonly adminEmail = 'adminEmail';
    public static readonly sharedDeviceStatusShared = 'Shared';
    public static readonly sharedDeviceStatusAccepted = 'Accepted';
    public static readonly sharedDeviceStatusRejected = 'Rejected';
    public static readonly ftpHost = 'FTPHost';
    public static readonly ftpPort = 'FTPPort';
    public static readonly ftpUsername = 'FTPUsername';
    public static readonly ftpPassword = 'FTPPassword';
    public static readonly imagesDir = 'ImagesDir';
    public static readonly prodImagesDir = 'prodImagesDir';


    public static async getFTPHost(): Promise<string> {
        return this.getConfigValue(ScampiiConstants.ftpHost,'13.127.39.213');
    }

    public static async getFTPUsername(): Promise<string> {
        return this.getConfigValue(ScampiiConstants.ftpUsername,'devclient');
    }

    public static async getFTPPassword(): Promise<string> {
        return this.getConfigValue(ScampiiConstants.ftpPassword,'s0ftL1nk');
    }

    public static async getImagesDir(): Promise<string> {
        return this.getConfigValue(ScampiiConstants.imagesDir,'scampii/images');
    }

    public static async getProdImagesDir(): Promise<string> {
        return this.getConfigValue(ScampiiConstants.prodImagesDir,'scampii/images');
    }

    public static async getFTPPort(): Promise<number> {
        const config: Config = await ConfigModel.findOne({key: ScampiiConstants.ftpPort});
        if(config) {
            return Promise.resolve(Number(config.value));
        }
        return Promise.resolve(2212);
    }

    public static async getImagesCDNDir() {
        return 'http://'+ await this.getFTPHost() + '/'+ await ScampiiConstants.getFTPUsername()+'/'+await  ScampiiConstants.getImagesDir() +'/';
    }

    public static async getImagesProdDir() {
        return 'http://'+ await this.getFTPHost() + '/'+await ScampiiConstants.getFTPUsername()+'/'+await ScampiiConstants.getProdImagesDir() +'/';
    }

    public static async getAppDownloadLink(): Promise<string> {
        const config: Config = await ConfigModel.findOne({key: ScampiiConstants.appDownloadLink});
        if(config) {
            return Promise.resolve(config.value);
        }
        return Promise.resolve('https://wwww.google.com');
    }

    public static async getFeedbackURL(): Promise<string> {
        const config: Config = await ConfigModel.findOne({key: ScampiiConstants.feedbackURL});
        if(config) {
            return Promise.resolve(config.value);
        }
        return Promise.resolve('Feedback URL has not been populated.');
    }

    public static async getAdminEmail(): Promise<string> {
        const config: Config = await ConfigModel.findOne({key: ScampiiConstants.adminEmail});
        if(config) {
            return Promise.resolve(config.value);
        }
        return Promise.resolve('Admin email is not populated.');

    }

    private static async getConfigValue(key: string, defaultValue: string): Promise<string> {
        const config: Config = await ConfigModel.findOne({key: key});
        if (config) {
            return Promise.resolve(config.value);
        }
        return Promise.resolve(defaultValue);
    }


}
