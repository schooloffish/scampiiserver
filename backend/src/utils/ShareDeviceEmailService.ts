import * as nodemail from 'nodemailer';
import * as Mail from 'nodemailer/lib/mailer';
import { SupportModel } from '../models/Support';

export class ShareDeviceEmailService {
    
    public static async sendShareDeviceEmail(template: any, email: any): Promise<string> {
        return ShareDeviceEmailService.sendMail('SCAMPII Share Device', email, `${template}`);
    }

    public static async sendMail(subject: string, email: string, msg: string) {
        var getFromUserEmail = await SupportModel.findOne({},{_id: 0,email:1,password:1}).lean();

        const transporter: Mail = nodemail.createTransport(this.getTransportOptions(getFromUserEmail)
        );

        const mailOptions = {
            from: getFromUserEmail.email, // sender address
            subject: subject,
            text: msg, 
            to: email, 
        };
       // console.log('mailOptions',mailOptions);
        return transporter.sendMail(mailOptions);
    } 

    private static getTransportOptions(getFromUserEmail: { password: any; email: any; }) {
        return {
            auth: {
                pass: getFromUserEmail.password,
                user:getFromUserEmail.email,
            },
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            service: 'gmail'
        };
    }

}
