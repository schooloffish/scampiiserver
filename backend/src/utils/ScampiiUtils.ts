import * as UUID from 'uuid';
import { AuthDetails } from '../models/AuthDetails';


export class ScampiiUtils {

    public static generateOTP(): AuthDetails {
        // integrate the logic for random code generation
        const details: AuthDetails = {} as AuthDetails;
        details.mobileOTP = ScampiiUtils.generateMobileOTP();
        return details;
    }

    public static generateEmailOTP(): AuthDetails {
        const details: AuthDetails = {} as AuthDetails;
        details.mailOTP = ScampiiUtils.generateMobileOTP();
        return details;
    } 

    public static logPromiseResults(promise: Promise<any>): void {
        promise.then((result) => console.log(result)).catch((err) => console.error(err));
    }

    public static generateAuthToken(): string {
        // This will generate a unique uuid which can be used as a auth token.
        const value = UUID.v4();
        return `Bearer ${value.toString()}`;
    }

    public static populateFromSource(source: any, target: any) {
        for (const key in source) {
            if (source.hasOwnProperty(key) && key && key !== 'undefined') {
                target[key] = source[key];
            }
        }
    }

    private static generateMobileOTP(): string {
        return Math.floor(100000 + Math.random() * 900000) + '';
    }

}

