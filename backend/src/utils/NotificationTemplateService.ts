import moment = require("moment");
import { ConfigModel } from "../models/Config";
/**
 * Created by Satyam on 16/04/21.
 */

export class NotificationTemplateService {
  public static oneTimeTemplateKey: string = "OneTime";
  public static OneTimeAlertTemplateForNoItemsTemplateKey: string =
    "dontHaveMsg";
  public static warrantyTemplateKey: string = "warrantyTemplate";
  public static warrantyExpiredTemplateKey: string = "warrantyExpiredTemplate";
  public static serviceActivityTemplateKey: string = "serviceActivityTemplate";
  public static serviceActivityExpiredTemplateKey: string =
    "serviceActivityExpiredTemplate";

  public static sampleOneTimeAlertMessageForItemHaveTemplate: string =
    "This is One Time  Notification for the item ##itemName## with the category ##product_category## and manufacturer ##manufacturer##.";
  public static sampleOneTimeAlertMessageForNoItemsTemplate: string =
    "This is One Time Notification for Items don't have condition. Hence, the message should not contains any tags.";
  public static sampleWarrantyTemplateMessage: string =
    "Your ##warrantyName## warranty for the item ##itemName## with the category ##product_category## and the manufacturer ##manufacturer## is going to expire in ##expireIn## days";
  public static sampleWarrantyExpiredTemplateMessage: string =
    "Your ##warrantyName## warranty for the item ##itemName## with the category ##product_category## and manufacturer ##manufacturer## has expired ##expireIn## days ago.";
  public static sampleServiceActivityTemplateMessage: string =
    "Your Service Activity for the item ##itemName## with the category ##product_category## and manufacturer ##manufacturer## scheduled on ##serviceScheduledDate## will start in ##serviceExpiryIn## day(s).";
  public static sampleServiceActivityExpiredTemplateMessage: string =
    "Your Service Activity for the item ##itemName## with the category ##product_category## and manufacturer ##manufacturer## scheduled on ##serviceScheduledDate## has passed scheduled date ##serviceExpiryIn## day(s) ago.";

  public static async checkOneTimeAlertTemplate(
    itemName: any,
    product_category: any,
    manufacturer: any
  ): Promise<string> {
    const configs = await ConfigModel.find();
    let oneTimeTemplate =
      NotificationTemplateService.sampleOneTimeAlertMessageForItemHaveTemplate;
    for (const config of configs) {
      const conf: any = config;
      if (conf["key"] === NotificationTemplateService.oneTimeTemplateKey) {
        oneTimeTemplate = conf["value"];
      }
    }
    oneTimeTemplate = oneTimeTemplate.replace("##itemName##", itemName);
    oneTimeTemplate = oneTimeTemplate.replace(
      "##product_category##",
      product_category
    );
    oneTimeTemplate = oneTimeTemplate.replace("##manufacturer##", manufacturer);

    return oneTimeTemplate;
  }

  //This is One Time Notification message Template for Items don't have condition. Hence, the message should not contains any tags.
  public static async checkOneTimeAlertTemplateForNoItems(): Promise<string> {
    const configs = await ConfigModel.find();
    let OneTimeAlertTemplateForNoItems =
      NotificationTemplateService.sampleOneTimeAlertMessageForNoItemsTemplate;
    for (const config of configs) {
      const conf: any = config;
      if (
        conf["key"] ===
        NotificationTemplateService.OneTimeAlertTemplateForNoItemsTemplateKey
      ) {
        OneTimeAlertTemplateForNoItems = conf["value"];
      }
    }
    return OneTimeAlertTemplateForNoItems;
  }

  public static async checkWarrantyAlertTemplate(
    isWarrantyExpired: any,
    itemName: any,
    product_category: any,
    manufacturer: any,
    warranties: any
  ): Promise<string> {
    const configs = await ConfigModel.find();
    let warrantyTemplate = isWarrantyExpired
      ? NotificationTemplateService.sampleWarrantyExpiredTemplateMessage
      : NotificationTemplateService.sampleWarrantyTemplateMessage;
    for (const config of configs) {
      const conf: any = config;
      if (
        isWarrantyExpired &&
        conf["key"] === NotificationTemplateService.warrantyExpiredTemplateKey
      ) {
        warrantyTemplate = conf["value"];
      } else if (
        !isWarrantyExpired &&
        conf["key"] === NotificationTemplateService.warrantyTemplateKey
      ) {
        warrantyTemplate = conf["value"];
      }
    }
    // console.log(
    //   "checkWarrantyAlertTemplate isWarrantyExpired: ",
    //   isWarrantyExpired,
    //   ", warrantyTemplate:",
    //   warrantyTemplate
    // );

    // console.log("warranties:", warranties);

    let sortedWarranties: Array<any> = [...warranties];

    sortedWarranties.sort((planA, planB) => {
      const startDatePlanA = new Date(planA.warrantyStartDate);
      const startDatePlanB = new Date(planB.warrantyStartDate);

      return startDatePlanA.getTime() - startDatePlanB.getTime();
    });
    // console.log("sortedWarranties:", sortedWarranties);

    const { name, warrantyType, warrantyTypeDuration, warrantyStartDate } =
      sortedWarranties[0];

    const selectedWarrantyName: any = name ? name : warrantyType;
    const warrantyEndDate = moment(warrantyStartDate).add(
      warrantyTypeDuration,
      "months"
    );
    const expireInDays: any = isWarrantyExpired
      ? moment.duration(moment().diff(warrantyEndDate)).asDays()
      : moment.duration(warrantyEndDate.diff(moment())).asDays();

    warrantyTemplate = warrantyTemplate.replace(
      "##warrantyName##",
      selectedWarrantyName
    );

    warrantyTemplate = warrantyTemplate.replace("##itemName##", itemName);
    // warrantyTemplate = warrantyTemplate.replace("##expireIn##", expireIn);
    warrantyTemplate = warrantyTemplate.replace(
      "##expireIn##",
      expireInDays.toFixed()
    );

    warrantyTemplate = warrantyTemplate.replace(
      "##product_category##",
      product_category
    );
    warrantyTemplate = warrantyTemplate.replace(
      "##manufacturer##",
      manufacturer
    );

    return warrantyTemplate;
  }

  public static async checkServiceAlertTemplate(
    isServiceActivityExpired: any,
    itemName: any,
    product_category: any,
    manufacturer: any,
    serviceActivities: any
  ): Promise<string> {
    const configs = await ConfigModel.find();
    let serviceActivityTemplate = isServiceActivityExpired
      ? NotificationTemplateService.sampleServiceActivityExpiredTemplateMessage
      : NotificationTemplateService.sampleServiceActivityTemplateMessage;

    for (const config of configs) {
      const conf: any = config;
      if (
        isServiceActivityExpired &&
        conf["key"] ===
          NotificationTemplateService.serviceActivityExpiredTemplateKey
      ) {
        serviceActivityTemplate = conf["value"];
      } else if (
        !isServiceActivityExpired &&
        conf["key"] === NotificationTemplateService.serviceActivityTemplateKey
      ) {
        serviceActivityTemplate = conf["value"];
      }
    }
    // console.log(
    //   "checkServiceAlertTemplate isServiceActivityExpired: ",
    //   isServiceActivityExpired,
    //   ", serviceActivityTemplate:",
    //   serviceActivityTemplate
    // );

    // console.log("serviceActivities:", serviceActivities);

    let sortedServiceActivities: Array<any> = [...serviceActivities];

    sortedServiceActivities.sort((planA, planB) => {
      const startDatePlanA = new Date(planA.warrantyStartDate);
      const startDatePlanB = new Date(planB.warrantyStartDate);

      return startDatePlanA.getTime() - startDatePlanB.getTime();
    });
    // console.log("sortedServiceActivities:", sortedServiceActivities);

    const { serviceStartDate } = sortedServiceActivities[0];

    const serviceScheduledDate =
      moment(serviceStartDate).format("dddd, MMMM Do YYYY");
    serviceActivityTemplate = serviceActivityTemplate.replace(
      "##serviceScheduledDate##",
      serviceScheduledDate
    );

    const serviceExpiryIn: any = isServiceActivityExpired
      ? moment.duration(moment().diff(serviceStartDate)).asDays()
      : moment.duration(moment(serviceStartDate).diff(moment())).asDays();
    serviceActivityTemplate = serviceActivityTemplate.replace(
      "##serviceExpiryIn##",
      serviceExpiryIn.toFixed()
    );

    serviceActivityTemplate = serviceActivityTemplate.replace(
      "##itemName##",
      itemName
    );

    serviceActivityTemplate = serviceActivityTemplate.replace(
      "##product_category##",
      product_category
    );

    serviceActivityTemplate = serviceActivityTemplate.replace(
      "##manufacturer##",
      manufacturer
    );

    return serviceActivityTemplate;
  }
}
