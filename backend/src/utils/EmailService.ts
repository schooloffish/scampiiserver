/**
 * Created by rohit on 26/03/19.
 */
import * as nodemail from 'nodemailer';
import * as Mail from 'nodemailer/lib/mailer';
export class EmailService {

    public static readonly fromEmailUser = 'scampiitest@gmail.com';
    public static readonly password = 'Welcome!1';


    public static async sendOTPEmail(email: string, otp: string): Promise<string> {
        return EmailService.sendMail('OTP Details', email, `Your OTP for scampii app <${otp}>`);
    }

    public static async sendMail(subject: string, email: string, msg: string) {
        const transporter: Mail = nodemail.createTransport(this.getTransportOptions()
        );

        const mailOptions = {
            from: EmailService.fromEmailUser, // sender address
            subject: subject, // Subject line
            text: msg, // plain text body
            to: email, // list of receivers
            // html: '<b>NodeJS Email Tutorial</b>' // html body
        };
        return transporter.sendMail(mailOptions);
    }

    public static async sendMailWithAttachment(subject: string, email: string, msg: string, filePath: string): Promise<any> {
        const transporter: Mail = nodemail.createTransport(this.getTransportOptions()
        );

        const mailOptions = {
            attachments : [
                {
                    path:filePath
                }
            ],
            from: EmailService.fromEmailUser, // sender address
            subject: subject, // Subject line
            text: msg, // plain text body
            to: email, // list of receivers

            // html: '<b>NodeJS Email Tutorial</b>' // html body
        };
        return transporter.sendMail(mailOptions);
    }

    private static getTransportOptions() {
        return {
            auth: {
                pass: EmailService.password,
                user: EmailService.fromEmailUser,
            },
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            service: 'gmail'
        };
    }



}

EmailService.sendMailWithAttachment('testAttachment','rohit324@gmail.com',
    'This is a tests message for testing','/Users/ragarwal/Desktop/Preethi2.xlsx').then((send)=>console.log(send));
