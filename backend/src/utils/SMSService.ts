import * as request from "request-promise";
import { ConfigModel } from "../models/Config";
/**
 * Created by rohit on 26/03/19.
 */

export class SMSService {
  public static url: string =
    "https://api.msg91.com/api/v5/otp?invisible=1&otp=otpvalue&authkey=304868AmuPIFeLq5dd533e4&mobile=";
  public static sendSMSURL: string = "https://api.msg91.com/api/v2/sendsms";
  public static otpTemplateKey: string = "otpSMSTemplate";
  public static sharedTemplateKey: string = "shareDeviceSMS";
  public static defaultTemplate: string =
    "##OTP## is your OTP for Scampii App login for mobile no ##mobileno##. This OTP will expire in 10 mins";
  // public static defaultSharedTemplate: string = 'A device has been shared with you, check the sampii app for details.';
  public static defaultSharedTemplate: string =
    "A device has been shared with you by {#var#}. Check SCAMPII app for details. Download now from http://onelink.to/scampii";

  public static async sendOTPMessage(
    mobile: string,
    otp: string
  ): Promise<string> {
    const configs = await ConfigModel.find();
    let smsTemplate = SMSService.defaultTemplate;
    // tslint:disable-next-line:forin
    for (const config of configs) {
      const conf: any = config;
      if (conf["key"] === SMSService.otpTemplateKey) {
        smsTemplate = conf["value"];
      }
    }
    smsTemplate = smsTemplate.replace("##OTP##", otp);
    smsTemplate = smsTemplate.replace("##mobileno##", mobile);
    return SMSService.sendMessage(smsTemplate, mobile);
  }

  public static async sendShareDeviceSMS(
    userName: string,
    mobile: string
  ): Promise<any> {
    const configs = await ConfigModel.find();
    let smsTemplate = SMSService.defaultSharedTemplate;
    // tslint:disable-next-line:defa
    for (const config of configs) {
      const conf: any = config;
      if (conf["key"] === SMSService.sharedTemplateKey) {
        smsTemplate = conf["value"];
      }
    }
    smsTemplate = smsTemplate.replace("{#var#}", userName);
    // console.log("Shared Device on Mobile smsTemplate:", smsTemplate);
    return SMSService.sendMessage(smsTemplate, mobile);
  }

  public static async sendMessage(
    messagePayload: string,
    mobile: string
  ): Promise<any> {
    const url = SMSService.sendSMSURL;
    const options: request.Options = {
      body: {
        country: "91",
        route: "4",
        sender: "SCAMPI",
        sms: [
          {
            message: messagePayload,
            to: [mobile],
          },
        ],
      },
      headers: {
        "Content-Type": "application/json",
        authKey: "304868AmuPIFeLq5dd533e4",
      },
      json: true,
      method: "POST",

      uri: url,
    };
    const response: any = await request(options);
    console.log(response);
    console.log(JSON.stringify(response));
    return Promise.resolve(response);
  }
}
