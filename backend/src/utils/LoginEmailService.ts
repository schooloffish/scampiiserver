import * as nodemail from 'nodemailer';
import * as Mail from 'nodemailer/lib/mailer';
import { SupportModel } from '../models/Support';

export class LoginEmailService {
    
    public static async sendOTPEmail(email: string, otp: string): Promise<string> {
        return LoginEmailService.sendMail('OTP Details', email, `Your OTP for scampii app is ${otp}`);
    }

    public static async sendMail(subject: string, email: string, msg: string) {
        var getFromUserEmail = await SupportModel.findOne({},{_id: 0,email:1,password:1}).lean();

        const transporter: Mail = nodemail.createTransport(this.getTransportOptions(getFromUserEmail)
        );

        const mailOptions = {
            from: getFromUserEmail.email, // sender address
            subject: subject,
            text: msg, 
            to: email, 
        };
        console.log('mailOptions',mailOptions);
        return transporter.sendMail(mailOptions);
    } 

    private static getTransportOptions(getFromUserEmail: { password: any; email: any; }) {
        return {
            auth: {
                pass: getFromUserEmail.password,
                user:getFromUserEmail.email,
            },
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            service: 'gmail'
        };
    //     host: 'smtp.gmail.com',
    //     port: 587,
    //     secure: false,
    //     requireTLS: true,
    //     auth: {
    //       pass: getFromUserEmail.password,
    //             user:getFromUserEmail.email,
    //     }
    // });
    }

}
