/**
 * Created by rohit on 18/06/19.
 */
export interface IOTPResponse {
    success?: boolean;
    message?: string;
    authToken?: string;
}

export interface TokenQueryCondition {
    authToken: string;
}
