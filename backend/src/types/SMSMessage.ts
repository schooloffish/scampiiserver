export class MessageTypes {

    public static createSMSMessageInstance(): SMSMessage {
        const smsMessage = new SMSMessage();
        smsMessage.country = '91';
        smsMessage.route = '4';
        smsMessage.sender = 'SCAMPII';
        smsMessage.sms = new Array<SMS>();
        return smsMessage;
    }
}

export class SMSMessage {
    public sender: string;
    public route: string;
    public country: string;
    public sms: Array<SMS>;
}

export class SMS {
    public message: string;
    public to: Array<string> = new Array<string>();
}
