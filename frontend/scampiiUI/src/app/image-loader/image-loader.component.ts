import { Component, Input, OnInit } from '@angular/core';
import { ScampiiServiceService } from '../scampii-service.service';

@Component({
  selector: 'app-image-loader',
  templateUrl: './image-loader.component.html',
  styleUrls: ['./image-loader.component.css']
})
export class ImageLoaderComponent implements OnInit {

  @Input() imagePath: any;
  @Input() comp: any;
  @Input() afterImageLoadFn: Function;
  @Input() beforeImageLoadFn: Function;
  reader: FileReader = new FileReader();


  constructor(public service: ScampiiServiceService) { }

  ngOnInit() {
  }


  onFileChanged(event) {
    this.beforeImageLoadFn(this.comp);
    this.reader.readAsDataURL(event.target.files[0]);
    this.reader.onloadend = (event1) => {
      this.imagePath = this.reader.result;
      this.service.addImage({cat: 'manufacturer', name: event.target.files[0].name, data: this.imagePath.split(',')[1]}).subscribe(
        (result => {
          console.log(`Image url is ${result}`);
          this.afterImageLoadFn(result.url, this.comp);
        }), error => this.afterImageLoadFn());
    };
  }

}
