import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {SocialAuthService} from 'angularx-social-login';
import {ScampiiServiceService} from '../scampii-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(public router: Router, private authService: SocialAuthService, private service: ScampiiServiceService) { }

  ngOnInit() {
  }

  signOut(): void {
    console.log('Sign out called.');
    this.authService.signOut(true);
    this.router.navigate(['login']);
  }

}
