import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {ScampiiServiceService} from '../scampii-service.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {IWarrantycover, IWarrantyTemplate} from '../common/IWarranty';
import {IMenu} from '../common/IMenu';
import {NAVIGATIONS} from '../common/Iconstants';
import {ProductComponent} from '../product/product.component';

@Component({
  selector: 'app-warranty-type',
  templateUrl: './warranty-type.component.html',
  styleUrls: ['./warranty-type.component.css']
})
export class WarrantyTypeComponent implements OnInit {

  imagePath: string;
  comp = this;
  warrantyCoverIcon;
  @Input() public cover: IWarrantycover;


  public warrantyTypeForm = new FormGroup({
    label: new FormControl(''),
    type: new FormControl('')
  });


  constructor(public service: ScampiiServiceService,
  ) {
  }

  ngOnInit() {
    this.imagePath = this.comp.warrantyCoverIcon;
  }

  public imageLoaded(imgURL: string, comp: any) {
    comp.warrantyCoverIcon = imgURL;
  }

  public imageLoadingStarted() {
  }

  public onSubmit() {
    const cover: IWarrantycover = {
      label: this.warrantyTypeForm.value.label.toString(),
      type: this.warrantyTypeForm.value.type.toString(),
      warrantyCoverIcon: this.warrantyCoverIcon
    };
    console.log(cover);
    this.service.saveWarrantyType(cover, this.service.http).subscribe((result: IMenu) => {
      alert('Saved Warranty type successfully');
      this.service.loadCovers();
    });
  }

}
