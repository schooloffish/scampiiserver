import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {ScampiiServiceService} from '../scampii-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public router: Router, public service: ScampiiServiceService) { }

  ngOnInit() {
  }

  switchToDev() {
    ScampiiServiceService.baseURL = ScampiiServiceService.devURL;
    localStorage.setItem('env', 'dev');
    this.router.navigate(['products']);
  }

  switchToProd() {
    ScampiiServiceService.baseURL = ScampiiServiceService.prodURL;
    localStorage.setItem('env', 'prod');
    this.router.navigate(['products']);
  }
}
