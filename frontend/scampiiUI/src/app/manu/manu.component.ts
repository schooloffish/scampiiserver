import { Component, OnInit } from '@angular/core';
import { ScampiiServiceService } from '../scampii-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { IMenu } from '../common/IMenu';
import { NAVIGATIONS } from '../common/Iconstants';
import {IWarrantyTemplate} from '../common/IWarranty';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {IServiceCenter} from '../common/IServiceCenter';

@Component({
  selector: 'app-manu',
  templateUrl: './manu.component.html',
  styleUrls: ['./manu.component.css']
})

export class ManuComponent implements OnInit {
  _id: string ;
  imagePath: any;
  imageURL: URL;
  disableSubmit = false;
  comp = this;
  content: any;
  serviceCenters: Array<IServiceCenter>;

  public manuForm = new FormGroup({
    manuName: new FormControl(''),
  });

  public serviceForm = new FormGroup({
    countryCode: new FormControl(''),
    custCareNums: new FormControl(''),
    whatsappNumbers: new FormControl(''),
    website: new FormControl(''),
    address: new FormControl(''),
    lat: new FormControl(''),
    lng: new FormControl(''),
    _id: new FormControl(''),
  });


  constructor(public service: ScampiiServiceService, private router: Router,
              private route: ActivatedRoute,
              private modalService: NgbModal) {
  }

  ngOnInit() {
    this.route.url.subscribe((url) => {
      if (!url[1]) { return; }
      const menu = this.service.menus.filter((val) => url[1].path === val._id.toString());
      this._id = menu[0]._id;
      this.manuForm.setValue({manuName: menu[0].name});
      this.imagePath = menu[0].logo;
      this.imageURL = menu[0].logo;
      this.service.loadServiceCenters(this._id, this);
    });
  }


  public imageLoaded(imgURL: string, comp: any) {
    console.log('image loaded ' + imgURL);
    comp.imageURL = new URL(imgURL);
    comp.disableSubmit = false;
  }

  public imageLoadingStarted(comp: any) {
    console.log('image loading');
    comp.disableSubmit = true;
  }


  public onSubmit() {
    console.log('saving manu details.');
    const list = this.service.menus.filter((val) => val._id === this._id);
    if (list && list.length === 1) {
      this.save(this.service.updateMenu);
    } else {
      this.save(this.service.saveMenu);
    }
  }

  private save(method: any) {
    console.log('saving image with ' + this.imageURL);
    const manu: IMenu = {name: this.manuForm.value.manuName, logo: this.imageURL};
    if (this._id) {
      manu._id = this._id;
    }
    method(manu, this.service.http).subscribe((result: IMenu) => {
      this.service.loadManus();
      this.router.navigate([NAVIGATIONS.manus], {replaceUrl: false});
    });
  }

  private saveServiceCenter(serviceCenter: IServiceCenter) {
    this.service.saveServiceCenter(serviceCenter, this._id).subscribe((result: IMenu) => {
      alert('Saved Service Center Details successfully');
      this.service.loadServiceCenters(this._id, this);
    });
  }

  private updateServiceCEnter(serviceCenter: IServiceCenter) {
    this.service.updateServiceCenter(serviceCenter, serviceCenter._id).subscribe(() => {
      alert('Updated Service Center Details successfully');
      this.service.loadServiceCenters(this._id, this);
    });
  }

  editServiceCenter(serviceCenter: IServiceCenter, content: any) {
    this.serviceForm.setValue({
      address: serviceCenter.address,
      countryCode: serviceCenter.countryCode,
    custCareNums: serviceCenter.custCareNums,
    lat: serviceCenter.gpsLocation.lat,
    lng: serviceCenter.gpsLocation.lng,
    website: serviceCenter.website,
    whatsappNumbers: serviceCenter.whatsappNumbers,
    _id: serviceCenter._id
  });
    this.openServiceCenterPopup(content);
  }

  openServiceCenterPopup(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result
      .then((result) => {
        if (result === 'Save') {
          const serviceCenter = {} as IServiceCenter;
          serviceCenter.address  = this.serviceForm.value.address;
          serviceCenter.countryCode = this.serviceForm.value.countryCode;
          serviceCenter.custCareNums = this.serviceForm.value.custCareNums;
          serviceCenter.gpsLocation =  {}  as any;
          serviceCenter.gpsLocation.lat = this.serviceForm.value.lat;
          serviceCenter.gpsLocation.lng = this.serviceForm.value.lng;
          serviceCenter.website = this.serviceForm.value.website;
          serviceCenter.whatsappNumbers = this.serviceForm.value.whatsappNumbers;
          serviceCenter.manufacturer = this._id;
          if (this.serviceForm.value._id) {
            serviceCenter._id = this.serviceForm.value._id;
            this.updateServiceCEnter(serviceCenter);
          } else {
            this.saveServiceCenter(serviceCenter);
          }
        }
      })
      .catch(error => console.log(error));
  }

  removeServiceCenter(center: IServiceCenter) {
    this.service.deleteServiceCenter(center._id).subscribe((result) => {
      alert('Saved Service Center Deleted successfully');
      this.service.loadServiceCenters(this._id, this);
    });
  }
}
