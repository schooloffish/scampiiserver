import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'listFilter'
})
export class ListFilterPipe implements PipeTransform {

  transform(value: any[], queryParam: string): any {
    const arr =  value.filter(val => {
      for (const key in val) {
        if (val.hasOwnProperty(key)) {
          const value1 = val[key];
          if (value1.toString().toLowerCase().indexOf(queryParam.toLowerCase()) !== -1) {
            return true;
          }
        }
      }
      return false;
    });
    return arr;
  }

}
