import { Component, OnInit } from '@angular/core';
import {ScampiiServiceService} from '../scampii-service.service';
import {IConfig} from '../common/IConfig';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.css']
})
export class ConfigComponent implements OnInit {

  configurations: Array<IConfig>;

  constructor(public service: ScampiiServiceService) {
  }

  ngOnInit() {
    this.service.loadConfigs(this.loadConfigCallback, this);
  }

  loadConfigCallback(result: Array<any>, comp: any) {
    comp.configurations = result;
  }

  submitConfig() {
    this.service.updateConfigs(this.configurations).subscribe((result) =>  {
      this.service.loadConfigs(this.loadConfigCallback, this);
      alert('Configurations saved successfully.');
      });
  }

  addConfig() {
      this.configurations.push({key: '', value: ''});
  }

  removeConfig(config) {
    this.configurations = this.configurations.filter((conf) => conf.key !== config.key);
  }
}
