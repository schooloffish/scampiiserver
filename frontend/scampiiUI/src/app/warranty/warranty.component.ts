import { Component, Input, OnInit } from '@angular/core';
import { ScampiiServiceService } from '../scampii-service.service';
import { ProductComponent } from '../product/product.component';

@Component({
  selector: 'app-warranty',
  templateUrl: './warranty.component.html',
  styleUrls: ['./warranty.component.css']
})
export class WarrantyComponent implements OnInit {

  @Input() public product: ProductComponent;

  public warrantyType: string;
  public warrantyTypeDuration: number;

  constructor(public service: ScampiiServiceService) { }

  ngOnInit() {
    if (this.product && this.product.warranty) {
      this.warrantyTypeDuration = this.product.warranty.warrantyTypeDuration;
      this.warrantyType = this.product.warranty.warrantyType;
    }
  }

  public setWarranty() {
    this.service.warrantyTypeDuration = this.warrantyTypeDuration;
    this.service.warrantyType = this.warrantyType;
  }

}
