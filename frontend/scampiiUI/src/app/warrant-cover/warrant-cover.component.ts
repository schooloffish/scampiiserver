import { Component, Input, OnInit } from '@angular/core';
import { ScampiiServiceService } from '../scampii-service.service';
import { IWarrantycover} from '../common/IWarranty';

@Component({
  selector: 'app-warrant-cover',
  templateUrl: './warrant-cover.component.html',
  styleUrls: ['./warrant-cover.component.css']
})
export class WarrantCoverComponent implements OnInit {

  @Input() public cover: IWarrantycover;

  public applicable = true;
  imagePath: string;
  comp = this;


  constructor(public service: ScampiiServiceService) { }

  public isApplicable(): boolean {
    return this.applicable;
  }

  public changeApplicable() {
    this.applicable = this.cover.type === this.service.coverTypeApplicable;
  }

  public imageLoaded(imgURL: string, comp: any) {
    comp.cover.warrantyCoverIcon = imgURL;
  }

  public imageLoadingStarted() {
  }

  public deleteWarranty() {
    this.service.warrantyCovers = this.service.warrantyCovers.filter((cov) => cov.label !== this.cover.label);
  }

  ngOnInit() {
    this.imagePath = this.cover.warrantyCoverIcon;
    if (!this.cover.type) {
      console.log(this.cover);
      this.cover.type = this.service.coverTypeApplicable;

    }
    this.applicable = this.cover.type === this.service.coverTypeApplicable;
  }

}
