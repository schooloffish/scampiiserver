import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarrantCoverComponent } from './warrant-cover.component';

describe('WarrantCoverComponent', () => {
  let component: WarrantCoverComponent;
  let fixture: ComponentFixture<WarrantCoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarrantCoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarrantCoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
