import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ScampiiServiceService } from '../scampii-service.service';
import * as XLSX from 'xlsx';
import * as saveAs from 'file-saver';
import * as xlsxFile from 'read-excel-file/node';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  public query = '';
  storeData: any;
  csvData: any;
  jsonData: any;
  textData: any;
  htmlData: any;
  fileUploaded: File;
  worksheet: any;

  constructor(private route: Router, public service: ScampiiServiceService) { }

  ngOnInit() {
    this.service.loadManus();
    this.service.loadProducts();
    this.service.loadCovers();
    this.service.loadWarrantyTemplates();
    this.service.loadProducts();
    this.service.loadProductTypes();
    this.service.loadCCs();
  }

  public gotoAddProduct() {
    this.route.navigateByUrl('product');
  }

  uploadedFile(event: any) {
    this.fileUploaded = event.target.files[0];
    this.readProducts();
  }

  readProducts() {
    const readFile = new FileReader();
    readFile.onload = (e) => {
      console.log('sending file ' + readFile);
      this.storeData = readFile.result;
      const data =  btoa(this.storeData);
      console.log(data);
      const fileDate = {
        value : data
      };
      console.log('sending file ' + fileDate);
      this.service.uploadBulkProducts(JSON.stringify(fileDate)).subscribe((result) => {
        alert(JSON.stringify(result));
      });
    };
    readFile.readAsBinaryString(this.fileUploaded);
  }

  readExcel() {
    const readFile = new FileReader();
    readFile.onload = (e) => {
      this.storeData = readFile.result;
      const data = new Uint8Array(this.storeData);

      const arr = new Array();
      for (let i = 0; i !== data.length; ++i) {
        arr[i] = String.fromCharCode(data[i]);
      }
      const bstr = arr.join("");
      const workbook = XLSX.read(bstr, { type: "binary" });
      const first_sheet_name = workbook.SheetNames[0];
      this.worksheet = workbook.Sheets[first_sheet_name];
      this.readAsJson();
      this.fileUploaded = null;
    };
    readFile.readAsArrayBuffer(this.fileUploaded);
  }

  readAsJson() {
    const jsonData = XLSX.utils.sheet_to_json(this.worksheet, { raw: false });
    const data = new Array<Array<string>>();
    for (const datum of jsonData) {
      const dataValue = new Array();
      const data1: any = datum;
      for (const key in data1) {
        if (data1.hasOwnProperty(key)) {
          const val = data1[key];
          dataValue.push(val);
        }
      }
      data.push(dataValue);
    }
    const test = {value: data};
    console.log(`sending products to backend.` + JSON.stringify(test));
    this.service.uploadBulkProducts(JSON.stringify(test)).subscribe((result) => {
      alert(JSON.stringify(result));
    });
  }

  initiateSearch(event: KeyboardEvent) {
    console.log(event);
    console.log(this.query.length);
    console.log(JSON.stringify(event));

    if (this.query.length < 3) {
      this.service.loadProducts();
    }

    if(this.query.length > 3) {
      this.service.searchProducts(this.query).subscribe((result) => {
        console.log(result);
        this.service.products = result;
      });
    }
  }
}
