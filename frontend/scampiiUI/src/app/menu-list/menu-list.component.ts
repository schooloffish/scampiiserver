import { Component, OnInit } from '@angular/core';
import { ScampiiServiceService } from '../scampii-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.css']
})
export class MenuListComponent implements OnInit {

  public query = '';

  constructor(public service: ScampiiServiceService, private router: Router) { }

  ngOnInit() {
    this.service.loadManus();
  }

  public gotoAddManu() {
    this.router.navigate(['/manu'], {replaceUrl: true});
  }

  public deleteManu(id: string) {
    const confirmation = confirm('Sure you want to delete');
    if (confirmation === true) {
      this.service.deleteMenu(id).subscribe((result) => this.service.loadManus());
    }
  }

}
