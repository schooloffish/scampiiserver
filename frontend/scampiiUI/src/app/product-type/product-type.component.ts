import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {ScampiiServiceService} from '../scampii-service.service';
import {IWarrantycover} from '../common/IWarranty';
import {IMenu} from '../common/IMenu';

@Component({
  selector: 'app-product-type',
  templateUrl: './product-type.component.html',
  styleUrls: ['./product-type.component.css']
})
export class ProductTypeComponent implements OnInit {

  imagePath: string;
  comp = this;
  image: string;


  public productCatForm = new FormGroup({
    type: new FormControl('')
  });


  constructor(public service: ScampiiServiceService,
  ) {
    this.service.loadProductTypes();
  }

  ngOnInit() {
    this.imagePath = this.comp.image;
  }

  public imageLoaded(imgURL: string, comp: any) {
    comp.image = imgURL;
  }

  public imageLoadingStarted() {
  }

  public onSubmit() {
    this.service.saveProductType(this.productCatForm.value.type.toString(), this.image);
  }

}
