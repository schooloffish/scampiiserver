import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { SocialAuthService, FacebookLoginProvider, GoogleLoginProvider, SocialUser } from 'angularx-social-login';
import {ScampiiServiceService} from '../scampii-service.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  signinForm: FormGroup;
  user: SocialUser;
  loggedIn: boolean;

  constructor(private fb: FormBuilder, private authService: SocialAuthService,
              public service: ScampiiServiceService, public router: Router) { }
  ngOnInit() {
    this.signinForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.authService.authState.subscribe((user) => {
      console.log('auth state called with user ' + JSON.stringify(user));
      this.user = user;
      this.loggedIn = (user != null);
      if (this.loggedIn) {
        console.log('logged in success');
        this.service.setAuthenticated(user.email).subscribe((result: any) => {
          console.log(result);
          this.router.navigate(['products']);
        });
      } else {
        localStorage.removeItem('accessToken');
        this.router.navigate(['login']);
      }
    });
  }
  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }
  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }
  signOut(): void {
    console.log('Sign out called.');
    this.authService.signOut(true);
  }
}
