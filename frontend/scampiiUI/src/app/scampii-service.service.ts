import { Injectable } from '@angular/core';
import { IMenu } from './common/IMenu';
import { Iimage } from './common/Iimage';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { IProduct } from './common/IProduct';
import { Icc } from './common/Icc';
import { IServiceCenter } from './common/IServiceCenter';
import {IWarrantycover, IWarrantyTemplate} from './common/IWarranty';
import {IProdType} from './product/IProdType';
import {IConfig} from './common/IConfig';
import {Router} from '@angular/router';
import { INotificationCountry } from './common/INotificationCountry';
import { ICity } from './common/Icity';
import { IAlertTemplate } from './common/IAlertTemplate';
import { IListAlertTemplate } from './common/IListAlertTemplate';
import { IEnableDisable } from './alert-templates/IEnableDisable';

@Injectable()
export class ScampiiServiceService {


  constructor(public http: HttpClient, public router: Router) {
    // load the initial list of manufacturers
  }
  public static prodURL = 'https://www.scampii.biz';
  public static devURL = 'https://www.scampii.in';
  public static baseURL = '';
  public static imageURL = '/images';
  public static bulkUploadURL = '/products/bulk';
  public static manuURL = '/manus';
  public static oAuthURL = '/users/oAuthLogin/';
  public static productsURL = '/products';
  public static configURL = '/configs';
  public static warrantyTemplatesURL = '/warrantyTemplates';
  public static warrantyTemplatesHashURL = '/warrantyTemplates/hash';
  public static productSearchURL = '/products/search';
  public static productTypesURL = '/products/types';
  public static ccURL = '/countries';
  public static notificationCountries = '/notification/get/countries'
  public static cities = '/notification/get/countryBasedCities?countryName='
  public static addAlertTemplate = '/notification/post/notifications'
  public static editAlertTemplate = '/notification/update/notifications'
  public static checkTargetAudience = '/notification/checkTargetAudience'
  public static listAlertTemplate = '/notification/get/notifications'
  public static templateSearchURL = '/notification/searchAlertTemplate';
  public static enableDisableTemplate = '/notification/enableDisable/notifications';
  public static warrantyCovers = '/warranty/covers';
  public static serviceCentersURL = '/serviceCenters';
  public static addServiceCentersURL = '/manus/servicecenter';
  public static isAuthenticated = false;
  public static accessToken;
  public static GOOGLE_CLIENT_ID = '699431023456-cqc7nv6o266b5nr910cc9haumikkufmu.apps.googleusercontent.com';
  public static GOOGLE_CLIENT_SECRET = '8dYKzZSxabIhZTEc5CwwD1P1';
  public static  devAccessToken = 'devAccessToken';
  public static  prodAccessToken = 'prodAccessToken';

  public static httpOptions = {
    headers: new HttpHeaders(
      {
        'Content-Type': 'application/json',
        Authorization: ScampiiServiceService.accessToken
      }
    )
  };

  public  coverTypeDuration = 'duration';
  public  coverTypeApplicable = 'isApplicable';
  public  isError = false;
  public  error;
  public menus: Array<IMenu> = [];
  public products: Array<IProduct> = [];
  public warrantyTemplates: Array<IWarrantyTemplate> = [];
  public productTypes: Array<IProdType> = [];
  public notificationProductTypes =[];
  public notificationManus=[];
  public countryCodes: Array<Icc> = [];
  public countryNames: Array<INotificationCountry> = [];
  public cities: Array<ICity> = [];
  public templates: Array<IListAlertTemplate> = [];
  public allTemplates: Array<IListAlertTemplate> = [];
  public template: IListAlertTemplate;
  public warrantyTypes: Array<string> = ['Factory'];
  public yesNo: Array<string> = ['Yes', 'No', 'NA'];
  public coverType: Array<string> = [this.coverTypeApplicable, this.coverTypeDuration];
  public warrantyCovers: Array<IWarrantycover> = [];
  public serviceCenters: Array<IServiceCenter> = [];
  public warrantyType: string;
  public warrantyTypeDuration: number;

  private static handleError<T>(router: Router, operation = 'operation', result?: T, ) {
    return (error: any): Observable<T> => {
      console.error(error);
      // TODO: better job of transforming isError for user consumption
      console.log(`${operation} failed: ${error.message}`);

      if (error.status === 401) {
        router.navigate(['login']).then((param) => console.log('going back to login page'));
      }
      // TODO: send the isError to remote logging infrastructure
      alert(error.message + error.error);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private static handleLoginError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the isError to remote logging infrastructure
      localStorage.removeItem('accessToken');
      alert(error.message + error.error);
      console.error(error); // log to console instead

      // TODO: better job of transforming isError for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


  public loadCovers() {
    this.http.get(ScampiiServiceService.baseURL + ScampiiServiceService.warrantyCovers, ScampiiServiceService.httpOptions)
      .pipe(tap(
        (result: Array<IWarrantycover>) => this.warrantyCovers = result),
        catchError(ScampiiServiceService.handleError<any>(this.router, 'loadCovers'))
      )
      .subscribe();
  }

  public loadWarrantyTemplates() {
    this.http.get(ScampiiServiceService.baseURL + ScampiiServiceService.warrantyTemplatesURL, ScampiiServiceService.httpOptions)
      .pipe(tap(
        (result: Array<IWarrantyTemplate>) => this.warrantyTemplates = result),
        catchError(ScampiiServiceService.handleError<any>(this.router, 'loadTemplates'))
      )
      .subscribe();
  }

  public calculateWarrantyHash(template: IWarrantyTemplate): Observable<any> {
    return this.http.post(ScampiiServiceService.baseURL + ScampiiServiceService.warrantyTemplatesHashURL, template, ScampiiServiceService.httpOptions);
  }

  public saveWarrantyTemplate(template: IWarrantyTemplate) {
    this.http.post(ScampiiServiceService.baseURL + ScampiiServiceService.warrantyTemplatesURL, template, ScampiiServiceService.httpOptions)
      .pipe(tap(
        (result: IWarrantyTemplate) => {
          console.log(`Saved template successfully ${result}`);
          this.loadWarrantyTemplates();
        }),
        catchError(ScampiiServiceService.handleError<any>(this.router, 'saveTemplates'))
      ).subscribe();
  }


  public loadCCs() {
    this.http.get(ScampiiServiceService.baseURL + ScampiiServiceService.ccURL, ScampiiServiceService.httpOptions)
      .pipe(tap(
        (result: Array<Icc>) => {this.countryCodes = result; }),
        catchError(ScampiiServiceService.handleError<any>(this.router, 'loadCCs'))
      )
      .subscribe();

  }

  public loadProductTypes() {
    this.http.get(ScampiiServiceService.baseURL + ScampiiServiceService.productTypesURL, ScampiiServiceService.httpOptions)
      .pipe(tap(
        (result: Array<any>) => {
          this.productTypes = result;
        }),
        catchError(ScampiiServiceService.handleError<any>(this.router, 'loadTypes'))
      )
      .subscribe(()=>{
        this.notificationProductTypes = this.productTypes.map((item,index)=>{
          return {
            item_id: index,
            item_text: item.type,
          };
        })
      });
  }

  public saveProductType(typeVal: string, imageVal: string) {
    const prodType = {type: typeVal, image: imageVal};
    this.http.post(ScampiiServiceService.baseURL + ScampiiServiceService.productTypesURL, prodType, ScampiiServiceService.httpOptions)
      .pipe(tap(
        (result: Array<any>) => {
          alert('Product Category saved successfully' + result);
          this.loadProductTypes();
        }),
        catchError(ScampiiServiceService.handleError<any>(this.router, 'saveProductType'))
      )
      .subscribe();
  }

  public loadManus() {
    this.http.get(ScampiiServiceService.baseURL + ScampiiServiceService.manuURL, ScampiiServiceService.httpOptions).pipe(
      tap((manus: IMenu[]) => {
        this.menus = manus;
      }),
      catchError(ScampiiServiceService.handleError<any>(this.router, 'loadManus'))
    )
    .subscribe(()=>{
      this.notificationManus = this.menus.map((item,index)=>{
        return {
          item_id: index,
          item_text: item.name,
        };
      })
    });
  }

  public loadServiceCenters(manuId: string, comp: any) {
    this.http.get(ScampiiServiceService.baseURL + ScampiiServiceService.serviceCentersURL + '/' + manuId, ScampiiServiceService.httpOptions).pipe(
      tap((servs: Array<IServiceCenter>) => {
        comp.serviceCenters = servs;
      }),
      catchError(ScampiiServiceService.handleError<any>(this.router, 'loadServiceCenters'))
    ).subscribe();
  }

  public searchProducts(searchTerm: string): Observable<Array<IProduct>> {
    this.isError = false;
    return this.http.get(ScampiiServiceService.baseURL + ScampiiServiceService.productSearchURL + '/' + searchTerm, ScampiiServiceService.httpOptions).pipe(
      tap((result: Array<IProduct>) => {
        return console.log(`found products of size =${result.length}`);
      }),
      catchError(ScampiiServiceService.handleError<any>(this.router, 'searchProducts'))
    );
  }

  public loadProducts() {
    this.http.get(ScampiiServiceService.baseURL + ScampiiServiceService.productsURL, ScampiiServiceService.httpOptions).pipe(
      tap((prods: IProduct[]) => {
        this.products = prods;
      }),
      catchError(ScampiiServiceService.handleError<any>(this.router, 'loadProducts'))
    ).subscribe();
  }

  // tslint:disable-next-line:ban-types
  public loadConfigs(callback: Function, comp: any) {
    this.http.get(ScampiiServiceService.baseURL + ScampiiServiceService.configURL, ScampiiServiceService.httpOptions).pipe(
      tap((configs: IConfig[]) => {
        console.log(configs);
      }),
      catchError(ScampiiServiceService.handleError<any>(this.router, 'loadConfigs'))
    ).subscribe((result) => callback(result, comp));
  }

  public updateConfigs(configs: Array<IConfig>): Observable<any> {
    this.isError = false;
    return this.http.post(ScampiiServiceService.baseURL + ScampiiServiceService.configURL, configs, ScampiiServiceService.httpOptions).pipe(
      tap((url) => {
        return console.log(`saved config successfully `);
      }),
      catchError(ScampiiServiceService.handleError<any>(this.router, 'updateConfigs'))
    );
  }

  public addMenu(menu: IMenu) {
    this.menus.push(menu);
  }

  public addImage(imageData: Iimage): Observable<any> {
    this.isError = false;
    return this.http.post(ScampiiServiceService.baseURL + ScampiiServiceService.imageURL, imageData, ScampiiServiceService.httpOptions).pipe(
      tap((url) => {
        return console.log(`updated image with url =${url}`);
      }),
      catchError(ScampiiServiceService.handleError<any>(this.router, 'addImage'))
    );
  }

  public uploadBulkProducts(productData: any): Observable<any> {
    this.isError = false;
    return this.http.post(ScampiiServiceService.baseURL +
      ScampiiServiceService.bulkUploadURL, productData, ScampiiServiceService.httpOptions).pipe(
      tap((response: any) => {
        console.log(`updated product `);
        return console.log(`updated product data successfull =${response}`);
      }),
      catchError(ScampiiServiceService.handleError<any>(this.router, 'uploadBulkProducts'))
    );
  }

  public saveMenu(menu: IMenu, http: HttpClient): Observable<IMenu> {
    return http.post(ScampiiServiceService.baseURL + ScampiiServiceService.manuURL, menu, ScampiiServiceService.httpOptions).pipe(
      tap((menu1: IMenu) => {
        return console.log(`saved manufactrer with id =${menu1.id}`);
      }),
    );
  }

  public saveWarrantyType(cover: IWarrantycover, http: HttpClient): Observable<IMenu> {
    return http.post(ScampiiServiceService.baseURL + ScampiiServiceService.warrantyCovers, cover, ScampiiServiceService.httpOptions).pipe(
      tap((iCover: IWarrantycover) => {
        return console.log(`saved warranty cover with id =${iCover}`);
      }),
      catchError(ScampiiServiceService.handleError<any>(this.router, 'saveWarrantyType'))
    );
  }

  public deleteMenu(id: string): Observable<any> {
    return this.http.delete(ScampiiServiceService.baseURL + ScampiiServiceService.manuURL + '/' + id, ScampiiServiceService.httpOptions).pipe(
      tap(result => console.log(result), catchError(ScampiiServiceService.handleError<any>(this.router, 'deleteMenu')))
    );
  }

  public updateMenu(menu: IMenu, http: HttpClient): Observable<IMenu> {
    console.log(JSON.stringify(menu));
    return http.put(ScampiiServiceService.baseURL + ScampiiServiceService.manuURL + '/' + menu._id, menu, ScampiiServiceService.httpOptions).pipe(
      tap((menu1: IMenu) => {
        return console.log(`updated manfactrer with id =${menu1._id}`);
      }),
    );
  }

  public saveProduct(product: IProduct, manuId: string): Observable<IProduct> {
    return this.http.post(ScampiiServiceService.baseURL + ScampiiServiceService.productsURL + '?manuId=' + manuId, product, ScampiiServiceService.httpOptions)
      .pipe(
        tap((result) => console.log(result)) ,
        catchError(ScampiiServiceService.handleError<any>(this.router, 'saveProduct')));
  }

  public updateProduct(product: IProduct, id: string): Observable<IProduct> {
    return this.http.put(ScampiiServiceService.baseURL + ScampiiServiceService.productsURL + '/' + id, product, ScampiiServiceService.httpOptions)
      .pipe(
        tap((result) => console.log(result)) ,
        catchError(ScampiiServiceService.handleError<any>(this.router, 'updateProduct')));
  }

  public saveServiceCenter(center: IServiceCenter, manufacturerId: string): Observable<IMenu> {
    return this.http.put(ScampiiServiceService.baseURL + ScampiiServiceService.addServiceCentersURL + '/' + manufacturerId,
      center, ScampiiServiceService.httpOptions)
      .pipe(
        tap((result) => console.log(result)) ,
        catchError(ScampiiServiceService.handleError<any>(this.router, 'saveServiceCenter')));
  }


  public updateServiceCenter(center: IServiceCenter, id: string): Observable<IProduct> {
    return this.http.put(ScampiiServiceService.baseURL + ScampiiServiceService.serviceCentersURL + '/' + id , center, ScampiiServiceService.httpOptions)
      .pipe(
        tap((result) => console.log(result)) ,
        catchError(ScampiiServiceService.handleError<any>(this.router, 'updateServiceCenter')));
  }

  public deleteServiceCenter(id: string): Observable<IProduct> {
    return this.http.delete(ScampiiServiceService.baseURL + ScampiiServiceService.serviceCentersURL + '/' + id , ScampiiServiceService.httpOptions)
      .pipe(
        tap((result) => console.log(result)) ,
        catchError(ScampiiServiceService.handleError<any>(this.router, 'updateServiceCenter')));
  }

  public loadNotificationCountries():Observable<INotificationCountry> {
    return this.http.get(ScampiiServiceService.baseURL + ScampiiServiceService.notificationCountries, ScampiiServiceService.httpOptions)
      .pipe(tap(
        (result: Array<INotificationCountry>) => {this.countryNames = result; }),
        catchError(ScampiiServiceService.handleError<any>(this.router, 'loadNotificationCountries'))
      )
  }

  public loadCities(country:string) {
    return this.http.get(ScampiiServiceService.baseURL + ScampiiServiceService.cities + country, ScampiiServiceService.httpOptions)
      .pipe(tap(
        (result: Array<ICity>) => {this.cities = result; }),
        catchError(ScampiiServiceService.handleError<any>(this.router, 'loadCities'))
      )
  }

  public addAlertTemplate(template: IAlertTemplate): Observable<any> {
    return this.http.post(ScampiiServiceService.baseURL + ScampiiServiceService.addAlertTemplate, template, ScampiiServiceService.httpOptions).pipe(
      tap((result) => {
        return console.log('Add alert template res',result);
      }),
      catchError(ScampiiServiceService.handleError<any>(this.router, 'addAlertTemplate'))
    );
  }

  public editAlertTemplate(template: IAlertTemplate,id:string): Observable<any> {
    return this.http.put(ScampiiServiceService.baseURL + ScampiiServiceService.editAlertTemplate + '/' + id, template, ScampiiServiceService.httpOptions).pipe(
      tap((result) => {
        return console.log('Edit alert template res',result);
      }),
      catchError(ScampiiServiceService.handleError<any>(this.router, 'editAlertTemplate'))
    );
  }

  public listAlertTemplates(){
    this.http.get(ScampiiServiceService.baseURL + ScampiiServiceService.listAlertTemplate, ScampiiServiceService.httpOptions).pipe(
      tap((temps:IListAlertTemplate[]) => {
        this.templates = temps;
        this.allTemplates = temps;
      }),
      catchError(ScampiiServiceService.handleError<any>(this.router, 'listAlertTemplates'))
    ).subscribe()
  }

  public listAlertTemplateByID(id:string){
    this.http.get(ScampiiServiceService.baseURL + ScampiiServiceService.listAlertTemplate + '/' + id , ScampiiServiceService.httpOptions).pipe(
      tap((temp:IListAlertTemplate) => {
        this.template = temp;
      }),
      catchError(ScampiiServiceService.handleError<any>(this.router, 'listAlertTemplates'))
    ).subscribe()
  }

  public checkTargetAudience(template: IAlertTemplate): Observable<any> {
    return this.http.post(ScampiiServiceService.baseURL + ScampiiServiceService.checkTargetAudience, template, ScampiiServiceService.httpOptions).pipe(
      tap((result) => {
      }),
      catchError(ScampiiServiceService.handleError<any>(this.router, 'checkTargetAudience'))
    );
  }

  public searchTemplates(searchTerm: string): Observable<Array<IListAlertTemplate>> {
    this.isError = false;
    return this.http.get(ScampiiServiceService.baseURL + ScampiiServiceService.templateSearchURL + '/' + searchTerm, ScampiiServiceService.httpOptions).pipe(
      tap((result: Array<IListAlertTemplate>) => {
        return console.log(`found products of templates size =${result.length}`);
      }),
      catchError(ScampiiServiceService.handleError<any>(this.router, 'searchTemplates'))
    );
  }

  public enableDisableTemplate(enable:IEnableDisable,id: string): Observable<any> {
    return this.http.post(ScampiiServiceService.baseURL + ScampiiServiceService.enableDisableTemplate + '/' + id,enable, ScampiiServiceService.httpOptions).pipe(
      tap(result => console.log(result), catchError(ScampiiServiceService.handleError<any>(this.router, 'enableDisableTemplate')))
    );
  }

  public setAuthenticated(email: string): Observable<IProduct> {
    return this.http.get(ScampiiServiceService.baseURL + ScampiiServiceService.oAuthURL + encodeURIComponent(email)).pipe(
      tap((user: any) => {
        if (this.isDev()) {
          localStorage.setItem(ScampiiServiceService.devAccessToken, user.authToken[0]);
        } else {
          localStorage.setItem(ScampiiServiceService.prodAccessToken, user.authToken[0]);
        }
        ScampiiServiceService.httpOptions = {
          headers: new HttpHeaders(
            {
              'Content-Type': 'application/json',
              Authorization: user.authToken[0]
            }
          )
        };
      }),
      catchError(ScampiiServiceService.handleError<any>(this.router, 'Error Logging in.'))
    );
  }

  getBackendURL() {
    return ScampiiServiceService.baseURL;
  }

  getProdURL() {
    return ScampiiServiceService.prodURL;
  }

  getDevURL() {
    return ScampiiServiceService.devURL;
  }

  isDev() {
    return localStorage.getItem('env') === 'dev';
  }

  isAuthenticated() {
    let token = '';
    if (this.isDev()) {
      token = localStorage.getItem(ScampiiServiceService.devAccessToken);
      ScampiiServiceService.baseURL = ScampiiServiceService.devURL;
    } else {
      token = localStorage.getItem(ScampiiServiceService.prodAccessToken);
      ScampiiServiceService.baseURL = ScampiiServiceService.prodURL;
    }
    // console.log(token);
    if (token && token.trim().length > 0) {
      ScampiiServiceService.httpOptions = {
        headers: new HttpHeaders(
          {
            'Content-Type': 'application/json',
            Authorization: token
          }
        )
      };
      return true;
    }
    return false;
  }
}
