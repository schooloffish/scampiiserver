import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { ManuComponent } from './manu/manu.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { HomeComponent } from './home/home.component';
import { MenuListComponent } from './menu-list/menu-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ScampiiServiceService } from './scampii-service.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { ListFilterPipe } from './list-filter.pipe';
import { ProductComponent } from './product/product.component';
import { ProductListComponent } from './product-list/product-list.component';
import { HeaderComponent } from './header/header.component';
import { ImageLoaderComponent } from './image-loader/image-loader.component';
import { WarrantCoverComponent } from './warrant-cover/warrant-cover.component';
import { TestComponent } from './test/test.component';
import { ServiceCenterComponent } from './service-center/service-center.component';
import { NAVIGATIONS } from './common/Iconstants';
import { WarrantyComponent } from './warranty/warranty.component';
import { CategoryComponent } from './category/category.component';
import { CategoryListComponent } from './category-list/category-list.component';
import { ProductTypeComponent } from './product-type/product-type.component';
import { WarrantyTypeComponent } from './warranty-type/warranty-type.component';
import { ProductDocumentComponent } from './product-document/product-document.component';
import { WarrantyTemplateComponent } from './warranty-template/warranty-template.component';
import { ConfigComponent } from './config/config.component';
import { ConfigItemComponent } from './config-item/config-item.component';
import { LoginComponent } from './login/login.component';
import { AlertTemplatesComponent } from './alert-templates/alert-templates.component';
import {
  SocialLoginModule,
  SocialAuthServiceConfig,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angularx-social-login';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AmazingTimePickerModule } from 'amazing-time-picker';
import {
  AuthGuardService,
  AuthGuardService as AuthGuard
} from './auth-guard.service';
import { AddEditAlertTemplateComponent } from './add-edit-alert-template/add-edit-alert-template.component';

const appRoutes: Routes = [
  { path: NAVIGATIONS.login.toString(), component: LoginComponent },
  { path: NAVIGATIONS.home.toString(), component: HomeComponent, canActivate: [AuthGuard] },
  { path: NAVIGATIONS.config.toString(), component: ConfigComponent, canActivate: [AuthGuard] },
  { path: NAVIGATIONS.manus.toString(), component: MenuListComponent, canActivate: [AuthGuard] },
  { path: NAVIGATIONS.manu.toString(), component: ManuComponent, canActivate: [AuthGuard] },
  { path: 'manu/:id', component: ManuComponent },
  { path: NAVIGATIONS.warrantyCovers.toString(), component: WarrantyTypeComponent },
  { path: NAVIGATIONS.products.toString(), component: ProductListComponent, canActivate: [AuthGuard] },
  { path: NAVIGATIONS.product.toString(), component: ProductComponent },
  { path: NAVIGATIONS.productCatList.toString(), component: ProductTypeComponent },
  { path: 'product/:id', component: ProductComponent },
  { path: NAVIGATIONS.alerts.toString(), component: AlertTemplatesComponent },
  { path: NAVIGATIONS.addTemplate.toString(), component: AddEditAlertTemplateComponent },
  { path: 'template/:id', component: AddEditAlertTemplateComponent },
  { path: NAVIGATIONS.serviceCenter.toString(), component: ServiceCenterComponent },
  { path: 'test', component: TestComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ManuComponent,
    NotFoundComponent,
    HomeComponent,
    MenuListComponent,
    ListFilterPipe,
    ProductComponent,
    ProductListComponent,
    HeaderComponent,
    ImageLoaderComponent,
    WarrantCoverComponent,
    TestComponent,
    ServiceCenterComponent,
    WarrantyComponent,
    CategoryComponent,
    CategoryListComponent,
    ProductTypeComponent,
    WarrantyTypeComponent,
    ProductDocumentComponent,
    WarrantyTemplateComponent,
    ConfigComponent,
    ConfigItemComponent,
    LoginComponent,
    AlertTemplatesComponent,
    AddEditAlertTemplateComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    NgbModule,
    HttpClientModule,
    SocialLoginModule,
    RouterModule.forRoot(
      appRoutes
    ),
    NgMultiSelectDropDownModule.forRoot(),
    AmazingTimePickerModule
  ],
  providers: [
    ScampiiServiceService,
    AuthGuardService,
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '699431023456-8sa2d99am0c3uv5e5vi21h1v7hkkus6r.apps.googleusercontent.com' // localhost
              // '699431023456-cqc7nv6o266b5nr910cc9haumikkufmu.apps.googleusercontent.com' // scampii url

            ),
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider('clientId'),
          },
        ],
      } as SocialAuthServiceConfig,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

