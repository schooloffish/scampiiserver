import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarrantyTemplateComponent } from './warranty-template.component';

describe('WarrantyTemplateComponent', () => {
  let component: WarrantyTemplateComponent;
  let fixture: ComponentFixture<WarrantyTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarrantyTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarrantyTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
