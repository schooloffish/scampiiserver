import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {ScampiiServiceService} from '../scampii-service.service';
import {IProduct} from '../common/IProduct';
import {ActivatedRoute, Router} from '@angular/router';
import {NAVIGATIONS} from '../common/Iconstants';
import {IWarrantyTemplate} from '../common/IWarranty';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {WarrantyComponent} from '../warranty/warranty.component';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})


export class ProductComponent implements OnInit, AfterViewInit {

  public manufacturer: string;
  duration: number;
  public serviceType: string;
  id: string;
  imagePath: string;
  disableSubmit = false;
  comp = this;
  public warTemplate;
  content: any;
  @ViewChild(WarrantyComponent) warComp: WarrantyComponent;
  isNameValid = false;
  templateName: string;
  templateNameStatus: string;
  public warranty;
  public productDocuments = new Array();
  public documentLabel: string;
  public documentURL: string;


  public prodForm = new FormGroup({
    productModel: new FormControl(''),
    modelDesc: new FormControl(''),
    productType: new FormControl(''),
    countryCode: new FormControl('')
  });

  public docForm = new FormGroup({
    documentLabel: new FormControl(''),
    documentURL: new FormControl(''),
  });


  constructor(public service: ScampiiServiceService,
              private route: Router,
              private activatedRoute: ActivatedRoute,
              private modalService: NgbModal) {
  }

  ngOnInit() {
    this.activatedRoute.url.subscribe((url) => {
      if (!url[1]) {
        this.service.loadCovers();
        return;
      }
      this.id = url[1].path;
      const prod = this.service.products.filter((prod1: IProduct) => prod1._id === this.id)[0];
      if (prod && prod.warranty) {
        this.service.warrantyCovers = prod.warranty.covers;
        this.prodForm.setValue({
          productModel: prod.productModel,
          modelDesc: prod.modelDesc,
          productType: prod.type,
          countryCode: prod.countryCode
        });
        this.manufacturer = prod.manufacturer;
        this.imagePath = prod.image;
        this.serviceType = prod.serviceType;
        this.warTemplate = prod.warranty.name;
        this.warranty = prod.warranty;
        this.productDocuments = prod.documents;
      }
    });
  }

  public imageLoaded(imgURL: string, comp: any) {
    comp.imagePath = imgURL;
    comp.disableSubmit = false;
  }

  public imageLoadingStarted(comp: any) {
    comp.disableSubmit = true;
  }


  public applyWarTemplate(content) {
    const template: IWarrantyTemplate = this.service.warrantyTemplates.filter((temp) => temp.name === this.warTemplate)[0];
    this.service.warrantyCovers = template.covers;
    this.content = content;
  }


  public onSubmit() {
    const warranty: IWarrantyTemplate = {
      covers: this.service.warrantyCovers,
      warrantyType: this.service.warrantyType,
      warrantyTypeDuration: this.service.warrantyTypeDuration

    };
    this.service.calculateWarrantyHash(warranty).subscribe((result: {hash: string; }) => {
      const templates: Array<IWarrantyTemplate> = this.service.warrantyTemplates.filter((war) => war.hash === result.hash);
      if (!templates || templates.length === 0) {
        this.openTemplateNamePopup(warranty);
      } else {
        this.saveProductDetals(warranty);
      }
    });
  }

  private saveTemplate(warranty: IWarrantyTemplate) {
    warranty.name = this.templateName;
    this.service.saveWarrantyTemplate(warranty);
  }

  private saveProductDetals(warranty: IWarrantyTemplate) {
    const prod: IProduct = {} as IProduct;
    prod.warranty = warranty;
    prod.warranty.name = this.warTemplate;
    prod.type = this.prodForm.value.productType;
    prod.countryCode = this.prodForm.value.countryCode;
    prod.modelDesc = this.prodForm.value.modelDesc;
    prod.productModel = this.prodForm.value.productModel;
    prod.serviceType = this.serviceType;
    prod.manufacturer = this.manufacturer;
    prod.image = this.imagePath;
    prod.documents = this.productDocuments;
    prod.name = `${this.getMenuName()} ${this.prodForm.value.modelDesc} ${this.prodForm.value.productType} ${this.prodForm.value.productModel}`;
    if (this.id) {
      this.service.updateProduct(prod, this.id).subscribe(result => {
        console.log(result);
        this.route.navigateByUrl(NAVIGATIONS.products.toString());
      });
    } else {
      this.service.saveProduct(prod, this.manufacturer).subscribe((result) => {
        console.log(result);
        this.route.navigateByUrl(NAVIGATIONS.products.toString());
      });
    }
  }


  public applyContentPopup(content: any) {
    this.content = content;
  }

  public getMenuName(): string {
    const iMenus = this.service.menus.filter(menu => menu._id === this.manufacturer);
    if (iMenus && iMenus.length === 1) {
      return iMenus[0].name;
    }
    return '';
  }

  public checkTemplateExists() {
    const warFilter = this.service.warrantyTemplates.filter(war => war.name === this.templateName);
    if (warFilter && warFilter.length > 0 ) {
      this.templateNameStatus = `Template with the name ${this.templateName} exists, provide a different name.`;
      this.isNameValid = false;
    } else {
      this.isNameValid = true;
    }
  }

  openDocumentPopup(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result
      .then((result) => {
        if (result === 'Save') {
          const result1 = this.productDocuments.filter(doc =>
            doc.documentURL === this.docForm.value.documentURL && doc.documentLabel === this.docForm.value.documentLabel);
          if (result1  && result1.length > 0) { return; }
          this.productDocuments.push({
            documentLabel: this.docForm.value.documentLabel,
            documentURL: this.docForm.value.documentURL
          });
        } else
        if (result === 'Skip') {
        }
      })
      .catch(error => console.log(error));
  }

  openTemplateNamePopup(warranty: IWarrantyTemplate) {
    this.modalService.open(this.content, {ariaLabelledBy: 'modal-basic-title'}).result
      .then((result) => {
      if (result === 'Save') {
        this.saveTemplate(warranty);
        this.saveProductDetals(warranty);
      } else
      if (result === 'Skip') {
        this.saveProductDetals(warranty);
      }
    })
      .catch(error => console.log(error));
  }

  removeProdDocument(document: any) {
      this.productDocuments = this.productDocuments.filter(doc =>
        doc.documentURL !== document.documentURL || doc.documentLabel !== document.documentLabel);
  }

  ngAfterViewInit(): void {
    this.service.loadWarrantyTemplates();
  }
}
