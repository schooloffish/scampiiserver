import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ScampiiServiceService } from "../scampii-service.service";
import { IEnableDisable } from "./IEnableDisable";

@Component({
  selector: "app-alert-templates",
  templateUrl: "./alert-templates.component.html",
  styleUrls: ["./alert-templates.component.css"],
})
export class AlertTemplatesComponent implements OnInit {
  public query = "";
  selectedFilter:string ='all'

  constructor(private route: Router, public service: ScampiiServiceService) {}

  ngOnInit(): void {
    this.service.listAlertTemplates();
  }

  public gotoAddEditTemplate() {
    this.route.navigateByUrl("addTemplate");
  }

  public toggleEnable = (enable:boolean, id: string) => {
    enable = ! enable
    const postData:IEnableDisable ={
      enable:enable.toString()
    }
    this.service
      .enableDisableTemplate(postData, id)
      .subscribe(() => this.service.listAlertTemplates());
  };

  initiateSearch(event: KeyboardEvent) {
    if (this.query.length < 3) {
      this.service.listAlertTemplates();
    }
    if (this.query.length > 3) {
      this.service.searchTemplates(this.query).subscribe((result) => {
        console.log(result);
        this.service.templates = result;
      });
    }
  }

  handleEnableDisable=(event)=>{
    this.selectedFilter = event.target.value;
    if(this.selectedFilter === 'enabled'){
      this.service.templates = this.service.allTemplates.filter((item)=>{
        return item.enable === true
      })
    }
    else if(this.selectedFilter === 'disabled'){
      this.service.templates = this.service.allTemplates.filter((item)=>{
        return item.enable === false
      })
    }
    else{
      this.service.listAlertTemplates();
    }

  }
}
