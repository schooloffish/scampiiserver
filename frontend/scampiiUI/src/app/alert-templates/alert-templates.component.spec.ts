import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertTemplatesComponent } from './alert-templates.component';

describe('AlertTemplatesComponent', () => {
  let component: AlertTemplatesComponent;
  let fixture: ComponentFixture<AlertTemplatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertTemplatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertTemplatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
