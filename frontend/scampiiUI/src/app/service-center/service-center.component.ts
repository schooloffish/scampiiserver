import { Component, OnInit } from '@angular/core';
import { ScampiiServiceService } from '../scampii-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { NAVIGATIONS } from '../common/Iconstants';

@Component({
  selector: 'app-service-center',
  templateUrl: './service-center.component.html',
  styleUrls: ['./service-center.component.css']
})
export class ServiceCenterComponent implements OnInit {

  _id: string;
  public centerForm = new FormGroup({
    countryCode: new FormControl(),
    custCareNum: new FormControl(),
    manufacturer: new FormControl(),
    website: new FormControl()
  });

  constructor(public service: ScampiiServiceService, private activatedRoute: ActivatedRoute, private router: Router) {

  }

  ngOnInit() {
  }

  public onSubmit() {
  }

  private gotoList(result: any) {
    console.log(result);
    this.router.navigateByUrl(NAVIGATIONS.serviceCenters.toString());
  }

}
