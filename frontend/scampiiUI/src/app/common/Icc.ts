/**
 * Created by rohit on 06/09/19.
 */

export interface Icc {
  country: string;
  code: string;
}
