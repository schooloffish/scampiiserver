
    export interface IListAlertTemplate {
        enable: boolean;
        country: string[];
        city: string[];
        searchString: string[];
        _id: string;
        template: string;
        alertType: string;
        itemHave: boolean;
        purchase_duration: number;
        product_categories: string[];
        manufacturer: string[];
        warranty: string;
        expired: boolean;
        expire_duration: string;
        activityExpiry:string;
        activityExpiryDuration:string;
        createdAt: Date;
        updatedAt: Date;
        __v: number;
        isWeekWarranty:boolean;
        isWeekServiceActivity:boolean;
        timeZone?:string;
        selectedTime?:string;
        selectedTimeUTC?:string;
    }

