/**
 * Created by rohit on 07/09/19.
 */

export enum NAVIGATIONS {
  home = '',
  login = 'login',
  manus = 'manus',
  manu = 'manu',
  products = 'products',
  product = 'product',
  serviceCenters = 'serviceCenters',
  serviceCenter = 'serviceCenter',
  productCatList = 'productCategories',
  productCat = 'productCategory',
  warrantyCovers = 'warrantyCovers',
  alerts = 'alerts',
  addTemplate = 'addTemplate',
  config = 'config'
}

