import { ICity } from "./Icity";
import { INotificationCountry } from "./INotificationCountry";


export interface IAlertTemplate {
    template: string;
    alertType: string;
    country: Array<INotificationCountry>;
    city?: any;
    itemHave: string;
    purchase_duration?: string;
    product_categories: string;
    manufacturer: string;
    expired?: string;
    expire_duration?: string;
    activityExpiry?:string;
    activityExpiryDuration?:string;
    searchString?: Array<string>;
    isWeekWarranty?:boolean;
    isWeekServiceActivity?:boolean;
    timeZone?:string;
    selectedTime?:string;
    selectedTimeUTC?:string;
  }