/**
 * Created by rohit on 02/09/19.
 */
import {IServiceCenter} from './IServiceCenter';

export interface IMenu {

  _id?: string;
  id?: number;
  name: string;
  logo: URL;
  serviceCenters?: Array<IServiceCenter>;
}
