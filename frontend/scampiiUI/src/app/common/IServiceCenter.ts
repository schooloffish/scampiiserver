/**
 * Created by rohit on 02/09/19.
 */

export interface IServiceCenter {

  _id?: string;
  address: string;
  countryCode: string;
  custCareNums: string;
  whatsappNumbers: string;
  manufacturer: string;
  manuName?: string;
  gpsLocation?: {lat: number, lng: number};
  website: string;
}
