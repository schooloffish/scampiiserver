export interface IWarrantycover {
  label: string;
  value?: string;
  duration?: number;
  type: string;
  warrantyCoverIcon?: string;
}

export interface IWarrantyTemplate {
  endDate?: Date;
  name?: string;
  startDate?: Date;
  daysLeft?: number;
  totalDays?: number;
  covers?: Array<IWarrantycover>;
  hash?: string;
  warrantyType: string;
  warrantyTypeDuration: number;

}

