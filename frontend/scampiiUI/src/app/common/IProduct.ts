import { IWarrantyTemplate } from './IWarranty';
/**
 * Created by rohit on 05/09/19.
 */
export interface IProduct {
  warrantyTypeDuration: number;
  _id?: string;
  countryCode: string;
  manufacturer?: string;
  manuName?: string;
  productModel: string;
  modelDesc: string;
  name: string;
  type: string;
  image: string;
  serviceType: string;
  warrantyType: string;
  warranty?: IWarrantyTemplate;
  documents?: Array<IProductDocument>;
}

export interface IProductDocument {
  documentLabel;
  documentURL;
}
