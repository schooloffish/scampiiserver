import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {ScampiiServiceService} from './scampii-service.service';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(public auth: ScampiiServiceService, public router: Router) {}
  canActivate(): boolean {
    if (!this.auth.isAuthenticated()) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
