import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditAlertTemplateComponent } from './add-edit-alert-template.component';

describe('AddEditAlertTemplateComponent', () => {
  let component: AddEditAlertTemplateComponent;
  let fixture: ComponentFixture<AddEditAlertTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditAlertTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditAlertTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
