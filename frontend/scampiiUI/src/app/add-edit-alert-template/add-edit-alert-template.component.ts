import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AmazingTimePickerService } from "amazing-time-picker";

import { IAlertTemplate } from "../common/IAlertTemplate";
import { NAVIGATIONS } from "../common/Iconstants";
import { IListAlertTemplate } from "../common/IListAlertTemplate";
import { ScampiiServiceService } from "../scampii-service.service";
import * as moment from "moment";
import "moment-timezone";

@Component({
  selector: "app-add-edit-alert-template",
  templateUrl: "./add-edit-alert-template.component.html",
  styleUrls: ["./add-edit-alert-template.component.css"],
})
export class AddEditAlertTemplateComponent implements OnInit {
  public selectedTime = "18:00";

  id: string;
  selectedExpiry: string = "";
  selectedActivityExpiry: string = "";
  selectedAlertType: string = "OneTime";
  isDateChanged: boolean = false;
  productDesc: string;
  searchStringArray: Array<string> = [];
  countryList = [];
  cityList = [];
  selectedCountriesList: any;
  selectedCitiesList: any;
  selectedManuList: any;
  selectedProductCat: any;
  dropdownSettings = {};
  timeZoneDropdownSettings = {};
  targetAudience = [];
  targetReceipients: number = 0;
  allTimezones: Array<string> = [];
  userTz: string;
  selectedUserTimezone: any;

  constructor(
    private fb: FormBuilder,
    private route: Router,
    public service: ScampiiServiceService,
    private activatedRoute: ActivatedRoute,
    private atp: AmazingTimePickerService
  ) {}

  get templateTitle() {
    return this.templateForm.get("templateTitle");
  }

  get timeZone() {
    return this.templateForm.get("timeZone");
  }

  get country() {
    return this.templateForm.get("country");
  }

  get productCategory() {
    return this.templateForm.get("productCategory");
  }

  get manufacturer() {
    return this.templateForm.get("manufacturer");
  }

  get purchaseDuration() {
    return this.templateForm.get("purchaseDuration");
  }

  get expiryDuration() {
    return this.templateForm.get("expiryDuration");
  }

  get activityExpiryDuration() {
    return this.templateForm.get("activityExpiryDuration");
  }

  templateForm = this.fb.group({
    templateTitle: ["", Validators.required],
    country: ["", Validators.required],
    city: [""],
    itemHave: ["", Validators.required],
    purchaseDuration: ["", Validators.pattern("^[0-9]{1,6}$")],
    productCategory: ["", Validators.required],
    manufacturer: ["", Validators.required],
    productDescription: [""],
    expiry: [""],
    expiryDuration: [""],
    activityExpiry: [""],
    activityExpiryDuration: [""],
    alertType: ["OneTime", Validators.required],
    timeZone: [""],
    isWeekWarranty: [true],
    isWeekServiceActivity: [true],
  });

  ngOnInit(): void {
    this.timeZoneDropdownSettings = {
      singleSelection: true,
      allowSearchFilter: true,
    };
    this.dropdownSettings = {
      singleSelection: false,
      idField: "item_id",
      textField: "item_text",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 4,
      allowSearchFilter: true,
    };
    this.activatedRoute.url.subscribe((url) => {
      if (url[1]) {
        this.id = url[1].path;
      }
      this.loadInitialData();
    });
  }

  loadInitialData = () => {
    this.allTimezones = moment.tz.names();
    this.userTz = moment.tz.guess(); // Get user timezone
    this.setSelectedTimeZone(this.userTz);
    this.service.loadProductTypes();
    this.service.loadManus();
    this.service.loadNotificationCountries().subscribe(() => {
      this.countryList = this.service.countryNames.map((item, index) => {
        return {
          item_id: index,
          item_text: item.countryName,
        };
      });
      this.countryList = this.countryList.filter(
        (country, index, countryList) => {
          return (
            country.item_text != undefined &&
            countryList.findIndex(
              (item) => item.item_text === country.item_text
            ) === index
          );
        }
      );
      if (this.id) {
        this.mapEditFormValues();
      }
    });
  };

  setSelectedTimeZone = (tz: string) => {
    this.selectedUserTimezone = this.allTimezones.filter((item, index) => {
      return item == tz;
    });
    this.templateForm.patchValue({
      timeZone: this.selectedUserTimezone,
    });
  };

  mapEditFormValues = () => {
    const temp = this.service.templates.filter(
      (item: IListAlertTemplate) => item._id === this.id
    )[0];

    this.selectedManuList = this.service.notificationManus.filter((manu) => {
      return temp.manufacturer.includes(manu.item_text);
    });

    this.selectedProductCat = this.service.notificationProductTypes.filter(
      (prod) => {
        return temp.product_categories.includes(prod.item_text);
      }
    );

    this.selectedCountriesList = this.countryList.filter((country) => {
      return temp.country.includes(country.item_text);
    });

    let selectedCountries =
      this.selectedCountriesList &&
      this.selectedCountriesList.map((item) => {
        return `${item.item_text}`;
      });

    this.loadCities(selectedCountries);

    this.templateForm.patchValue({
      templateTitle: temp.template,
      country: this.selectedCountriesList,
      itemHave: temp.itemHave,
      purchaseDuration: temp.purchase_duration,
      productCategory: this.selectedProductCat,
      manufacturer: this.selectedManuList,
      alertType: temp.alertType,
    });

    this.service.loadManus();
    this.service.loadProductTypes();

    if (temp.hasOwnProperty("timeZone")) {
      this.setSelectedTimeZone(temp.timeZone);
      this.selectedTime = temp.selectedTime;
    }
    if (temp.hasOwnProperty("expired"))
      this.templateForm.patchValue({ expiry: temp.expired });
    if (temp.hasOwnProperty("activityExpiry"))
      this.templateForm.patchValue({ activityExpiry: temp.activityExpiry });
    if (temp.hasOwnProperty("isWeekWarranty"))
      this.templateForm.patchValue({ isWeekWarranty: temp.isWeekWarranty });
    if (temp.hasOwnProperty("isWeekServiceActivity"))
      this.templateForm.patchValue({
        isWeekServiceActivity: temp.isWeekServiceActivity,
      });
    let divider;
    if (temp.isWeekWarranty == true || temp.isWeekServiceActivity == true)
      divider = 7;
    else divider = 30;
    if (temp.hasOwnProperty("expire_duration"))
      this.templateForm.patchValue({
        expiryDuration: (Number(temp.expire_duration) / divider).toFixed(0),
      });
    if (temp.hasOwnProperty("activityExpiryDuration"))
      this.templateForm.patchValue({
        activityExpiryDuration: (
          Number(temp.activityExpiryDuration) / divider
        ).toFixed(0),
      });
    if (temp.hasOwnProperty("expired"))
      this.selectedExpiry = temp.expired.toString();
    if (temp.hasOwnProperty("activityExpiry"))
      this.selectedActivityExpiry = temp.activityExpiry.toString();
    this.searchStringArray = temp.searchString;
    this.selectedAlertType = temp.alertType.toString();
  };

  onCountrySelect(item: any) {
    let selectedCountries = this.templateForm.value.country || item;
    selectedCountries =
      selectedCountries &&
      selectedCountries.map((item) => {
        return `${item.item_text}`;
      });
    this.loadCities(selectedCountries);
  }

  loadCities = (selectedCountries) => {
    selectedCountries = selectedCountries.join(",");
    this.service.loadCities(selectedCountries).subscribe(() => {
      this.cityList = this.service.cities.map((item, index) => {
        return {
          item_id: index,
          item_text: item.city,
        };
      });
      this.cityList = this.cityList.filter((city, index, cityList) => {
        return (
          city.item_text != undefined &&
          city.item_text !== "" &&
          cityList.findIndex((item) => item.item_text === city.item_text) ===
            index
        );
      });
      if (this.id) {
        const temp = this.service.templates.filter(
          (item: IListAlertTemplate) => item._id === this.id
        )[0];
        this.selectedCitiesList = this.cityList.filter((city) => {
          return temp.city.includes(city.item_text);
        });
        this.templateForm.patchValue({
          city: this.selectedCitiesList,
        });
      }
    });
  };

  openTimePicker = () => {
    const amazingTimePicker = this.atp.open({
      onlyHour: true,
      time: this.selectedTime,
      theme: "dark",
      arrowStyle: {
        background: "red",
        color: "white",
      },
    });
    amazingTimePicker.afterClose().subscribe((time) => {
      this.selectedTime = time;
    });
  };

  onSubmit = (type?: string) => {
    const country = this.templateForm.value.country.map((item) => {
      return `${item.item_text}`;
    });
    const city =
      this.templateForm.value.city &&
      this.templateForm.value.city.map((item) => {
        return `${item.item_text}`;
      });

    const manufacturer =
      this.templateForm.value.manufacturer &&
      this.templateForm.value.manufacturer.map((item) => {
        return `${item.item_text}`;
      });
    const productCategory =
      this.templateForm.value.productCategory &&
      this.templateForm.value.productCategory.map((item) => {
        return `${item.item_text}`;
      });

    const postData: IAlertTemplate = {
      template: this.templateForm.value.templateTitle,
      alertType: this.templateForm.value.alertType,
      country: country,
      itemHave: this.templateForm.value.itemHave,
      product_categories: productCategory,
      manufacturer: manufacturer,
      searchString: this.searchStringArray,
    };
    if (this.templateForm.value.purchaseDuration !== "") {
      postData.purchase_duration = this.templateForm.value.purchaseDuration;
    }
    if (city.length > 0) {
      postData.city = city;
    } else {
      postData.city = [];
    }
    if (this.templateForm.value.alertType != "OneTime") {
      postData.timeZone = this.templateForm.value.timeZone.join();
      postData.selectedTime = this.selectedTime;
      postData.selectedTimeUTC = moment
        .tz(this.selectedTime, "HH:mm", this.templateForm.value.timeZone.join())
        .utc()
        .format("YYYY-MM-DD[T]HH:mm:ss");
    }

    if (
      this.selectedAlertType === "scheduledWarranty" &&
      this.templateForm.value.expiryDuration !== "" &&
      this.templateForm.value.expiry === false
    ) {
      let multiplier: number;
      if (this.templateForm.value.isWeekWarranty == true || this.templateForm.value.isWeekWarranty == 'true') multiplier = 7;
      else multiplier = 30;
      postData.expire_duration = (
        this.templateForm.value.expiryDuration * multiplier
      ).toFixed(0);
      postData.isWeekWarranty = this.templateForm.value.isWeekWarranty;
    }
    if (
      this.selectedAlertType === "scheduledServiceActivity" &&
      this.templateForm.value.activityExpiryDuration !== "" &&
      this.templateForm.value.activityExpiry === false
    ) {
      let multiplier: number;
      if (this.templateForm.value.isWeekServiceActivity == true || this.templateForm.value.isWeekServiceActivity == 'true') multiplier = 7;
      else multiplier = 30;
      postData.activityExpiryDuration = (
        this.templateForm.value.activityExpiryDuration * multiplier
      ).toFixed(0);
      postData.isWeekServiceActivity =
        this.templateForm.value.isWeekServiceActivity;
    }
    if (
      this.selectedAlertType === "scheduledWarranty" &&
      this.templateForm.value.expiry !== ""
    ) {
      postData.expired = this.templateForm.value.expiry;
    }
    if (
      this.selectedAlertType === "scheduledServiceActivity" &&
      this.templateForm.value.activityExpiry !== ""
    ) {
      postData.activityExpiry = this.templateForm.value.activityExpiry;
    }

    if (type === "check") {
      this.service.checkTargetAudience(postData).subscribe((result) => {
        this.targetAudience = result;
        this.targetReceipients = this.calculateTotalReceipients();
      });
    } else if (this.id) {
      this.service.editAlertTemplate(postData, this.id).subscribe((result) => {
        this.route.navigateByUrl(NAVIGATIONS.alerts.toString());
      });
    } else {
      this.service.addAlertTemplate(postData).subscribe((result) => {
        this.route.navigateByUrl(NAVIGATIONS.alerts.toString());
      });
    }
  };

  calculateTotalReceipients = () => {
    let total = 0;
    this.targetAudience.forEach((audience) => {
      total += audience?.receipient?.length;
    });
    return total;
  };

  serviceRadioHandler = (type: string) => {
    this.selectedExpiry = type;
    this.updateValidation();
  };

  serviceActivityRadioHandler = (type: string) => {
    this.selectedActivityExpiry = type;
    this.updateValidation();
  };

  handleAlertType = (event) => {
    this.selectedAlertType = event.target.value;
    if (
      this.selectedAlertType === "scheduledWarranty" ||
      this.selectedAlertType === "scheduledServiceActivity"
    ) {
      this.updateValidation();
      this.templateForm.patchValue({
        itemHave: true,
      });
      return;
    }
    this.clearScheduledValidations();
  };

  handlePurchaseDurationValidation = (type: string) => {
    if (type === "false") {
      this.templateForm.get("purchaseDuration").clearValidators();
      this.templateForm.get("purchaseDuration").updateValueAndValidity();
      return;
    }
  };

  clearScheduledValidations = () => {
    this.templateForm.get("expiry").clearValidators();
    this.templateForm.get("expiryDuration").clearValidators();
    this.templateForm.get("activityExpiry").clearValidators();
    this.templateForm.get("activityExpiryDuration").clearValidators();

    this.templateForm.get("expiry").updateValueAndValidity();
    this.templateForm.get("expiryDuration").updateValueAndValidity();
    this.templateForm.get("activityExpiry").updateValueAndValidity();
    this.templateForm.get("activityExpiryDuration").updateValueAndValidity();

    this.selectedExpiry = "";
    this.selectedActivityExpiry = "";
    this.templateForm.patchValue({
      expiry: "",
      activityExpiry: "",
      expiryDuration: "",
      activityExpiryDuration: "",
    });
  };

  updateValidation = () => {
    this.templateForm.get("expiry").setValidators([Validators.required]);
    this.templateForm.get("timeZone").setValidators([Validators.required]);
    if (this.selectedExpiry == "false") {
      this.templateForm
        .get("expiryDuration")
        .setValidators([
          Validators.required,
          Validators.pattern("^[0-9]{1,6}$"),
        ]);
    } else {
      this.templateForm.controls["expiryDuration"].clearValidators();
      this.templateForm.controls["expiryDuration"].updateValueAndValidity();
    }
    this.templateForm
      .get("activityExpiry")
      .setValidators([Validators.required]);
    if (this.selectedActivityExpiry == "false") {
      this.templateForm
        .get("activityExpiryDuration")
        .setValidators([
          Validators.required,
          Validators.pattern("^[0-9]{1,6}$"),
        ]);
    } else {
      this.templateForm.controls["activityExpiryDuration"].clearValidators();
      this.templateForm.controls[
        "activityExpiryDuration"
      ].updateValueAndValidity();
    }

    if (this.selectedExpiry !== "") {
      this.templateForm.controls["activityExpiry"].clearValidators();
    } else if (this.selectedActivityExpiry !== "") {
      this.templateForm.controls["expiry"].clearValidators();
    }

    this.templateForm.controls["timeZone"].updateValueAndValidity();
    this.templateForm.controls["expiry"].updateValueAndValidity();
    if (this.templateForm.value.expiry === "false")
      this.templateForm.controls["expiryDuration"].updateValueAndValidity();
    this.templateForm.controls["activityExpiry"].updateValueAndValidity();
    if (this.templateForm.value.activityExpiry === "false")
      this.templateForm.controls[
        "activityExpiryDuration"
      ].updateValueAndValidity();
  };

  addSearchString = () => {
    this.searchStringArray = [...this.searchStringArray, this.productDesc];
    this.productDesc = "";
  };

  removeSearchString = (index) => {
    this.searchStringArray.splice(index, 1);
  };

  cancel = () => {
    this.route.navigateByUrl(NAVIGATIONS.alerts.toString());
  };
}
