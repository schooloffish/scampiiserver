import { TestBed, inject } from '@angular/core/testing';

import { ScampiiServiceService } from './scampii-service.service';

describe('ScampiiServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ScampiiServiceService]
    });
  });

  it('should be created', inject([ScampiiServiceService], (service: ScampiiServiceService) => {
    expect(service).toBeTruthy();
  }));
});
