#!/usr/bin/env bash
ng build
alias aws="aws --profile user1"
aws --profile user1 s3 rm s3://scampii.dev/ --recursive
aws --profile user1 s3 cp ../dist/scampiiUI s3://scampii.dev/ --recursive --acl public-read

